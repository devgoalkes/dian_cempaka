<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title') | Administrator {{ config('app.name') }}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Administrator {{ config('app.name') }}">
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('front/images/favicon.png') }}" />
  <link href="{{ asset('admin/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('admin/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('admin/css/pace.css') }}" rel="stylesheet">
  <link href="{{ asset('admin/css/morris.css') }}" rel="stylesheet"/>
  <link href="{{ asset('admin/css/app.min.css') }}" rel="stylesheet">
  <link href="{{ asset('admin/css/app-skin.css') }}" rel="stylesheet">
  @yield('style')

</head>
