<script src="{{ asset('admin/js/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('admin/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ asset('admin/js/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('admin/js/pace.min.js') }}"></script>
<script src="{{ asset('admin/js/jquery.popupoverlay.min.js') }}"></script>
<script src="{{ asset('admin/js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('admin/js/modernizr.min.js') }}"></script>
<script src="{{ asset('admin/js/jquery.cook.min.js') }}"></script>
<script src="{{ asset('admin/js/app/app_dashboard.js') }}"></script>
<script src="{{ asset('admin/js/app/app.js') }}"></script>
<script>
  $(function (){
    setInterval(function(){
      $.ajax({
        url: '{{ route('contact.count') }}',                  //the script to call to get data
        data: "",
        success: function(data)
        {
          $('.countMessage').html('<span class="badge badge-danger m-left-xs bounceIn">' + data + '</span>')
        }
      });
    }, 10000);
  });
</script>
