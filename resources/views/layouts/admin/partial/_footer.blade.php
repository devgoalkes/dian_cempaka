<footer>
  <div class="row">
    <div class="col-sm-6">
      <span class="footer-brand">
        <strong class="text-danger">{{ config('app.name') }}</strong> Admin
      </span>
      <p class="no-margin">
        &copy; <?php echo date('Y'); ?> <strong>{{ config('app.name') }}</strong>. ALL Rights Reserved.
      </p>
    </div><!-- /.col -->
  </div><!-- /.row-->
</footer>


<a href="#" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  {{ csrf_field() }}
</form>
<!-- Logout confirmation -->
<div class="custom-popup width-100" id="logoutConfirm">
<div class="padding-md">
  <h4 class="m-top-none"> Apakah anda ingin keluar dari halaman administrator?</h4>
</div>
<div class="text-center">
  <a class="btn btn-success m-right-sm" href="{{ route('logout') }}" onclick="event.preventDefault();
    document.getElementById('logout-form').submit();">Logout</a>
  <a class="btn btn-danger logoutConfirm_close">Cancel</a>
</div>
</div>
@include('layouts.admin.partial._script')
