<div id="top-nav" class="fixed default">
	<a href="{{ route('adm') }}" class="brand">
		<span>{{ config('app.name') }}</span>
		<span class="text-toggle">&nbsp;</span>
	</a><!-- /brand -->
	<button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	<button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
</div><!-- /top-nav-->
