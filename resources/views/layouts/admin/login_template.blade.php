<!DOCTYPE html>
<html lang="en">
@include('layouts.admin.partial._head')
  <body>
  	<div class="login-wrapper">
  		<div class="text-center">
  			<h2 class="fadeInUp animation-delay8" style="font-weight:bold">
  				<span class="text-success">Login</span> <span style="color:#ccc; text-shadow:0 1px #fff">Admin</span>
  			</h2>
  		</div>
  		<div class="login-widget animation-delay1">
  			<div class="panel panel-default">
  				@yield('content')
  			</div><!-- /panel -->
  		</div><!-- /login-widget -->
  	</div><!-- /login-wrapper -->
    @include('layouts.admin.partial._script')
  </body>
</html>
