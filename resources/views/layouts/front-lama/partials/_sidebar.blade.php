<!-- SIDE BAR START -->
<div class="col-md-3">

    <aside  class="side-bar">
      @if($latest->count() > 0)
        <!-- RECENT POST BLOCK START -->
        <div class="widget bg-white recent-posts-entry">

            <h4 class="widget-title">Recent Posts</h4>
            <div class="widget-post-bx">
              @foreach($latest as $news)
                <div class="widget-post clearfix">
                    <div class="wt-post-media">
                        <img src="{{ url('storage/uploads/news/'. $news->picture ) }}" width="200" height="143" alt="">
                    </div>
                    <div class="wt-post-info">
                        <div class="wt-post-header">
                            <h6 class="post-title"><a href="{{ route('artikel.detail', $news->slug) }}">{{ $news->title }}</a></h6>
                        </div>
                        <div class="wt-post-meta">
                            <ul>
                                <li class="post-author"><small>{{ $news->user->name }}</small></li>
                                <li class="post-comment"><i class="fa fa-comments"></i> <small>{{ $news->comments->count() }}</small></li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif

        <!-- OUR GALLERY  BLOCK START -->
        <div class="widget widget_categories">
            <h5 class="widget-title">Proyek</h5>
            <ul>
              @foreach($portfolio as $data)
              <li><a href="{{ route('portfolio') }}">{{ $data->title }}</a> {{ $data->galeries->count() }}</li>
              @endforeach
            </ul>
        </div>
        <!-- OUR GALLERY BLOCK END -->

    </aside>

</div>
<!-- SIDE BAR END -->
