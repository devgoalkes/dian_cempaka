
<footer class="site-footer footer-dark">
    @if(Route::current()->getName() != 'home')
    @include('layouts.front.partials._content-footer')
    @endif
    <!-- FOOTER COPYRIGHT -->
    <div class="footer-bottom overlay-wraper">
        <div class="overlay-main bg-black" style="opacity:0;"></div>
        <div class="constrot-strip"></div>
        <div class="container p-t30">
            <div class="row">
                <div class="wt-footer-bot-left">
                    <span class="copyrights-text">© <script>var currentYear = new Date().getFullYear();document.write(currentYear);</script> PT. Dian Cempaka. All Rights Reserved.</span>
                </div>
            </div>
        </div>
    </div>
</footer>

<a target="_blank" href="https://api.whatsapp.com/send?phone=6287889078228&text=Halo%20Marketing%20Support" class="button btn-fx6 hidden-lg hidden-md"><i class="fab fa-whatsapp fa-lg"></i></a>
<a target="_blank" href="https://web.whatsapp.com/send?phone=6287889078228&text=Halo%20Marketing%20Support" class="button btn-fx6 hidden-sm hidden-xs"><i class="fab fa-whatsapp fa-lg"></i></a>
