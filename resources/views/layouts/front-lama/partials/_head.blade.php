<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="robots" content="" />
    <meta name="description" content="" />

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('front/images/favicon.png') }}" />

    <title>@yield('front_title') - PT. Dian Cempaka</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="description" content="@yield('metadescription')">

    <meta property="og:title" content="@yield('metatitle') | PT. Dian Cempaka"/>
    <meta property="og:description" content="@yield('metadescription')"/>
    <meta property="og:site_name" content="Dian Cempaka"/>
    <meta property="og:url" content="{{ request()->fullUrl() }}"/>
    <meta property="og:image" content="@yield('image', asset('front/images/favicon.png'))"/>

    <meta name="keywords" content="{{ $identities->meta_keyword }}">
    <meta name="author" content="{{ $identities->website_name }}">
    <meta http-equiv="imagetoolbar" content="no">
    <meta name="language" content="Indonesia">
    <meta name="revisit-after" content="7">
    <meta name="webcrawlers" content="all">
    <meta name="rating" content="general">
    <meta name="spiders" content="all">

    <link rel="canonical" href="{{ request()->fullUrl() }}">
    <!-- [if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
	<![endif] -->
    <link rel="stylesheet" type="text/css" href="{{ asset('front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/css/fontawesome/css/fontawesome-all.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('front/css/flaticon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ mix('front/css/style.min.css') }}">
    @yield('styles')
</head>
