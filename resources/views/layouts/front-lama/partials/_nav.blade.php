<div class="header-nav navbar-collapse collapse ">
      <ul class="nav navbar-nav">
        <?php
        $menu_0 = \App\MenuModel::where('id_parent',0)->where('active', 'Y')->where('admin', 'N')->get();
        foreach ($menu_0 as $key) {
          get_menu_child($key->id);
        }
        function get_menu_child($parent=0){
          $menu = \App\MenuModel::where('id_parent',$parent)->where('active', 'Y')->get();
          $parent = \App\MenuModel::where('id',$parent)->where('active', 'Y')->first();
        ?>
        @if(sizeof($menu) > 0)
        <li>
          <a href="{{url($parent->url)}}">{{ $parent->name }} <i class="fa fa-chevron-down"></i></a>
        @else
        <li><a href="{{url($parent->url)}}">{{ $parent->name }}</a>
        @endif
        @if(sizeof($menu)>0)
        <ul class="sub-menu">
          <?php
          foreach ($menu as $key) {
            get_menu_child($key->id);
          }
          ?>
        </ul>
        @endif
        </li>
      <?php } ?>
      <li class="submenu-direction"></li>
    </ul>

</div>
