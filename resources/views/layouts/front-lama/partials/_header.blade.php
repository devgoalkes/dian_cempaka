<!-- HEADER START -->
<header class="site-header header-style-1 ">

    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="wt-topbar-right clearfix">
                  <ul class="social-bx list-inline pull-right">
                    @foreach($social as $soc)
                      @if($soc->driver == 'facebook')
                      <li><a href="http://www.facebook.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                      @endif
                      @if($soc->driver == 'twitter')
                      <li><a href="http://twitter.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                      @endif
                      @if($soc->driver == 'pinterest')
                      <li><a href="http://www.pinterest.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                      @endif
                      @if($soc->driver == 'google')
                      <li><a href="http://profiles.google.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                      @endif
                      @if($soc->driver == 'youtube')
                      <li><a href="http://youtube.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                      @endif
                      @if($soc->driver == 'instagram')
                      <li><a href="http://instagram.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                      @endif
                    @endforeach
                    </ul>
                    <ul class="list-unstyled e-p-bx pull-right">
                        <li><i class="fa fa-envelope"></i>{{ $identities->mail_address }}</li>
                        <li><i class="fa fa-phone"></i>{{ $identities->phone }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="sticky-header main-bar-wraper">
        <div class="main-bar bg-primary">
            <div class="container">
                <div class="logo-header">
                    <a href="/">
                        <img src="{{ asset('front/images/logo.png') }}" width="230" height="67" alt="" />
                    </a>
                </div>
                <!-- NAV Toggle Button -->
                <button data-target=".header-nav" data-toggle="collapse" type="button" class="navbar-toggle collapsed">
                    <span class="sr-only">Buka Menu</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                @include('layouts.front.partials._nav')
            </div>
        </div>
    </div>

</header>
<!-- HEADER END -->
