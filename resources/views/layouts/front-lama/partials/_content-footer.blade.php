<!-- FOOTER BLOCKES START -->
<div class="footer-top overlay-wraper">
    <div class="overlay-main bg-black" style="opacity:0;"></div>
    <div class="container">
        <div class="row">
            <!-- ABOUT COMPANY -->
            <div class="col-md-3 col-sm-6">
                <div class="widget widget_about">
                    <h4 class="widget-title">Tentang Kami</h4>
                    <div class="logo-footer clearfix p-b15">
                        <a href="/"><img src="{{ asset('front/images/logo.png')}}" width="230" height="67" alt=""/></a>
                    </div>
                    <p>Percayakan kebutuhan pipa anda pada kami. Distributor pipa Kembla (Australia) terpercaya dan terbaik di Indonesia.</p>
                </div>
            </div>
            <!-- RESENT POST -->
            <div class="col-md-3 col-sm-6">
                <div class="widget recent-posts-entry-date">
                    <h4 class="widget-title">Proyek Terakhir</h4>
                    <div class="widget-post-bx">
                      @foreach($latest_portfolio as $latest)
                        <div class="bdr-light-blue widget-post clearfix  bdr-b-1 m-b10 p-b10">
                            <div class="wt-post-date">
                                <img src="{{ url('storage/uploads/galery/'. $latest->picture) }}">
                            </div>
                            <div class="wt-post-info">
                                <div class="wt-post-header">
                                    <h6 class="post-title"><a href="{{ route('portfolio') }}">{{ $latest->galery_title }}</a></h6>
                                </div>
                                <div class="wt-post-meta">
                                    <ul>
                                      <li class="post-comment"><i class="fa fa-calendar"></i> {{ $latest->created_at }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
            <!-- USEFUL LINKS -->
            <div class="col-md-3 col-sm-6">
                <div class="widget widget_services">
                    <h4 class="widget-title">Link Cepat</h4>
                    <ul>
                      @php $menu_0 = \App\MenuModel::where('id_parent',0)->where('active', 'Y')->where('admin', 'N')->get(); @endphp
                      @foreach($menu_0 as $menu)
                        <li><a href="{{ url($menu->url) }}">{{ $menu->name }}</a></li>
                      @endforeach
                    </ul>
                </div>
            </div>
            <!-- NEWSLETTER -->
            <div class="col-md-3 col-sm-6">
                <!-- SOCIAL LINKS -->
                <div class="widget widget_social_inks">
                    <h4 class="widget-title">Sosial Media</h4>
                    <ul class="social-icons social-square social-darkest">
                      @foreach($social as $soc)
                        @if($soc->driver == 'facebook')
                        <li><a href="http://www.facebook.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                        @endif
                        @if($soc->driver == 'twitter')
                        <li><a href="http://twitter.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                        @endif
                        @if($soc->driver == 'pinterest')
                        <li><a href="http://www.pinterest.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                        @endif
                        @if($soc->driver == 'google')
                        <li><a href="http://profiles.google.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                        @endif
                        @if($soc->driver == 'youtube')
                        <li><a href="http://youtube.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                        @endif
                        @if($soc->driver == 'instagram')
                        <li><a href="http://instagram.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                        @endif
                      @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">

           <div class="col-md-4 col-sm-4 p-tb20">
               <div class="wt-icon-box-wraper left p-tb15 p-lr10 clearfix">
                    <div class="icon-md text-primary">
                        <span class="flaticon-placeholder"></span>
                    </div>
                    <div class="icon-content">
                        <h5 class="wt-tilte text-uppercase m-b0">Alamat</h5>
                        <p>{{ $identities->address }}</p>
                    </div>
               </div>
            </div>
           <div class="col-md-4 col-sm-4  p-tb20 ">
               <div class="wt-icon-box-wraper left p-tb15 p-lr10 clearfix ">
                    <div class="icon-md text-primary">
                        <span class="flaticon-smartphone"></span>
                    </div>
                    <div class="icon-content">
                        <h5 class="wt-tilte text-uppercase m-b0">Telepon</h5>
                        <p class="m-b0">+{{ $identities->phone }}</p>
                    </div>
               </div>
           </div>
           <div class="col-md-4 col-sm-4 p-tb20">
               <div class="wt-icon-box-wraper left p-tb15 p-lr10 clearfix">
                    <div class="icon-md text-primary">
                        <span class="flaticon-email"></span>
                    </div>
                    <div class="icon-content">
                        <h5 class="wt-tilte text-uppercase m-b0">Email</h5>
                        <p class="m-b0">{{ $identities->mail_address }}</p>
                    </div>
                </div>
            </div>

      </div>
    </div>
</div>
