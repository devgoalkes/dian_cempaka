<!DOCTYPE html>
<html lang="en">
@include('layouts.front.partials._head')

<body id="bg">

	<div class="page-wraper">

        @include('layouts.front.partials._header')

        <!-- CONTENT START -->
        <div class="page-content">

            @yield('content')
        </div>
        <!-- CONTENT END -->

        @include('layouts.front.partials._footer')
        <!-- SCROLL TOP BUTTON -->
        <button class="scroltop" ><i class=" fa fa-arrow-up"></i></button>

    </div>


@include('layouts.front.partials._script')
</body>
</html>
