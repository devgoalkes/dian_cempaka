<!DOCTYPE html>
<html>

@include('layouts.front.partials._head')

<body>

<div class="page-wrapper">

    <!-- <div class="preloader"></div> -->

    <header class="main-header">

        @include('layouts.front.partials._header')

        @include('layouts.front.partials._sticky_nav')

    </header>

    @yield('content')

    @include('layouts.front.partials._footer')

</div>
<div class="hidden-lg hidden-md whatsapp-chat"><a target="_blank" href="https://api.whatsapp.com/send?phone=6287889078228&text=Halo%20Marketing%20Support"><span class="icon fa fa-whatsapp"></span></a></div>
<div class="hidden-sm hidden-xs whatsapp-chat"><a target="_blank" href="https://web.whatsapp.com/send?phone=6287889078228&text=Halo%20Marketing%20Support"><span class="icon fa fa-whatsapp"></span></a></div>

<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-double-up"></span></div>

@include('layouts.front.partials._script')

</body>
</html>
