<div class="header-top">
    <div class="auto-container">
        <div class="clearfix">
            <div class="top-left pull-left">
              <ul class="clearfix">
                  <li>Jadikan kami mitra terpercaya anda</li>
                    <li><span class="fa fa-phone"></span>{{ $identities->phone }}</li>
                </ul>
            </div>

            <div class="top-right pull-right">
              <ul class="social-nav">
                @foreach($socials as $soc)
                    @if($soc->driver == 'facebook')
                    <li><a href="http://www.facebook.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                    @endif
                    @if($soc->driver == 'twitter')
                    <li><a href="http://twitter.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                    @endif
                    @if($soc->driver == 'pinterest')
                    <li><a href="http://www.pinterest.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                    @endif
                    @if($soc->driver == 'google')
                    <li><a href="http://profiles.google.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                    @endif
                    @if($soc->driver == 'youtube')
                    <li><a href="http://youtube.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                    @endif
                    @if($soc->driver == 'instagram')
                    <li><a href="http://instagram.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                    @endif
                @endforeach
                </ul>

                <div class="search-box">
                  <form action="/search" method="POST" role="search">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <input type="search" name="search-field" value="" placeholder="Search" required>
                            <button type="submit"><span class="icon fa fa-search"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="header-upper">
  <div class="auto-container">
      <div class="clearfix">

          <div class="pull-left logo-box">
              <div class="logo"><a href="{{ route('home') }}"><img src="front/images/logo.png" alt="" title=""></a></div>
            </div>

            <div class="pull-right upper-right clearfix">

                <!--Info Box-->
                <div class="upper-column info-box">
                  <div class="icon-box"><span class="flaticon-telephone-handle-silhouette"></span></div>
                    <ul>
                      <li><strong>Phone</strong></li>
                        <li>{{ $identities->phone }}</li>
                    </ul>
                </div>

                <div class="upper-column info-box">
                  <div class="icon-box"><span class="flaticon-envelope"></span></div>
                    <ul>
                      <li><strong>Mail</strong></li>
                        <li>{{ $identities->mail_address }}</li>
                    </ul>
                </div>

                <!--Info Box-->
                <div class="upper-column info-box">
                  <div class="icon-box"><span class="flaticon-map-marker"></span></div>
                    <ul>
                      <li><strong>Alamat</strong></li>
                        <li>{{ $identities->address }}</li>
                    </ul>
                </div>

            </div>

        </div>
    </div>
</div>

<div class="header-lower">

  <div class="auto-container">
      <div class="nav-outer clearfix">
            <!-- Main Menu -->
            <nav class="main-menu">
                <div class="navbar-header">
                    <!-- Toggle Button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="navbar-collapse collapse clearfix">
                    <ul class="navigation clearfix">
                      <?php
                      $menu_0 = \App\MenuModel::where('id_parent',0)->where('active', 'Y')->where('admin', 'N')->get();
                      foreach ($menu_0 as $key) {
                        get_menu_child($key->id);
                      }
                      function get_menu_child($parent=0){
                        $menu = \App\MenuModel::where('id_parent',$parent)->where('active', 'Y')->get();
                        $parent = \App\MenuModel::where('id',$parent)->where('active', 'Y')->first();
                      ?>
                      @if(sizeof($menu) > 0)
                      <li class="dropdown">
                        <a href="{{url($parent->url)}}">{{ $parent->name }}</a>
                      @else
                      <li><a href="{{url($parent->url)}}">{{ $parent->name }}</a>
                      @endif
                      @if(sizeof($menu)>0)
                      <ul>
                        <?php
                        foreach ($menu as $key) {
                          get_menu_child($key->id);
                        }
                        ?>
                      </ul>
                      @endif
                      </li>
                    <?php } ?>

                    </ul>
                </div>
            </nav>

        </div>
    </div>
</div>
