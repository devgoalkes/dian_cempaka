<footer class="main-footer" style="background-image:url(front/images/background/5.jpg)">
  <div class="auto-container">

      <!--Widgets Section-->
        <div class="widgets-section">
          <div class="row clearfix">

                <!--big column-->
                <div class="big-column col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">

                        <!--Footer Column-->
                        <div class="footer-column col-md-7 col-sm-6 col-xs-12">
                            <div class="footer-widget logo-widget">
                                <div class="logo">
                                  <a href="index-2.html"><img src="front/images/footer-logo.png" alt="" /></a>
                                </div>
                                <div class="text">Lebih dari 30 tahun pengalaman dan pengetahuan standar internasional, kami berdedikasi untuk memberikan solusi terbaik kepada pelanggan kami.</div>
                            </div>
                        </div>

                        <!--Footer Column-->
                        <div class="footer-column col-md-5 col-sm-6 col-xs-12">
                            <div class="footer-widget links-widget">
                              <div class="footer-title">
                                <h2>Services</h2>
                                </div>
                                <ul class="footer-lists">
                                    <li><a href="#">Agricultural Processing</a></li>
                                    <li><a href="#">Chemical Research</a></li>
                                    <li><a href="#">Material Engineering</a></li>
                                    <li><a href="#">Mechanical Engineering</a></li>
                                    <li><a href="#">Petroleum and Gas</a></li>
                                    <li><a href="#">Power and Energy</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

                <!--big column-->
                <div class="big-column col-md-6 col-sm-12 col-xs-12">
                    <div class="row clearfix">

                      <!--Footer Column-->
                      <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                          <div class="footer-widget news-widget">
                              <div class="footer-title">
                                <h2>Artikel Terakhir</h2>
                                </div>
                                <div class="widget-content">
                                    @foreach($latest as $news)
                                    <article class="post">
                                        <figure class="post-thumb"><a href="news-detail.html"><img src="{{ url('/storage/uploads/news/'.$news->picture) }}" alt=""></a></figure>
                                        <div class="text"><a href="">{{ $news->title }}</a></div>
                                        <ul class="post-info">
                                          <li><span class="icon fa fa-calendar"></span>{{ tglIndo($news->created_at, false) }}</li>
                                            <li><span class="icon fa fa-comment-o"></span>{{ $news->comments->count() }}</li>
                                        </ul>
                                    </article>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <!--Footer Column-->
                      <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                          <div class="footer-widget subscribe-widget">
                              <div class="footer-title">
                                <h2>Subscribe Us</h2>
                                </div>
                                <div class="widget-content">
                                    <ul class="social-icon-two">
                                      <li class="follow">Ikuti Kami :</li>
                                      @foreach($identities->identity_socials as $soc)
                                          @if($soc->driver == 'facebook')
                                          <li><a href="http://www.facebook.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                                          @endif
                                          @if($soc->driver == 'twitter')
                                          <li><a href="http://twitter.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                                          @endif
                                          @if($soc->driver == 'pinterest')
                                          <li><a href="http://www.pinterest.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                                          @endif
                                          @if($soc->driver == 'google')
                                          <li><a href="http://profiles.google.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                                          @endif
                                          @if($soc->driver == 'youtube')
                                          <li><a href="http://youtube.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                                          @endif
                                          @if($soc->driver == 'instagram')
                                          <li><a href="http://instagram.com/{{ $soc->username }}" target="_blank" class="fa fa-{{ $soc->driver }}"></a></li>
                                          @endif
                                      @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="footer-bottom">
      <div class="auto-container">
          <div class="copyright">&copy; Copyright {{ date("Y") }}. All Rights Reserved | {{ config('app.name') }}</div>
        </div>
    </div>
</footer>
