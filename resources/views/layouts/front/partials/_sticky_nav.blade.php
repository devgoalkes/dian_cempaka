<div class="sticky-header">
  <div class="auto-container clearfix">
      <!--Logo-->
      <div class="logo pull-left">
          <a href="{{  route('home') }}" class="img-responsive"><img src="front/images/logo-small.png" alt="" title=""></a>
        </div>

        <!--Right Col-->
        <div class="right-col pull-right">
          <!-- Main Menu -->
            <nav class="main-menu">
                <div class="navbar-header">
                    <!-- Toggle Button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="navbar-collapse collapse clearfix">
                    <ul class="navigation clearfix">
                      <?php
                      $menu_1 = \App\MenuModel::where('id_parent',0)->where('active', 'Y')->where('admin', 'N')->get();
                      foreach ($menu_1 as $key) {
                        get_sub_menu($key->id);
                      }
                      function get_sub_menu($parent=0){
                        $menu = \App\MenuModel::where('id_parent',$parent)->where('active', 'Y')->get();
                        $parent = \App\MenuModel::where('id',$parent)->where('active', 'Y')->first();
                      ?>
                      @if(sizeof($menu) > 0)
                      <li class="dropdown">
                        <a href="{{url($parent->url)}}">{{ $parent->name }}</a>
                      @else
                      <li><a href="{{url($parent->url)}}">{{ $parent->name }}</a>
                      @endif
                      @if(sizeof($menu)>0)
                      <ul>
                        <?php
                        foreach ($menu as $key) {
                          get_sub_menu($key->id);
                        }
                        ?>
                      </ul>
                      @endif
                      </li>
                    <?php } ?>
                    </ul>
                </div>
            </nav><!-- Main Menu End-->
        </div>

    </div>
</div>
