<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<meta property="og:title" content="@yield('metatitle') | PT. Griya Arcadia Indah"/>
<meta property="og:description" content="@yield('metadescription')"/>
<meta property="og:site_name" content="PT. Griya Arcadia Indah"/>
<meta property="og:url" content="{{ request()->fullUrl() }}"/>
<meta property="og:image" content="@yield('image', asset('front/images/favicon.png'))"/>

<meta name="keywords" content="{{ $identities->meta_keyword }}">
<meta name="author" content="{{ $identities->website_name }}">
<meta http-equiv="imagetoolbar" content="no">
<meta name="language" content="Indonesia">
<meta name="revisit-after" content="7">
<meta name="webcrawlers" content="all">
<meta name="rating" content="general">
<meta name="spiders" content="all">

<title>@yield('front_title') - PT. Griya Arcadia Indah</title>
<link rel="stylesheet" type="text/css" href="{{ mix('front/css/style.min.css') }}">
@yield('styles')
<link rel="shortcut icon" href="{{ asset('front/images/favicon.png') }}" type="image/x-icon">

<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>
