<div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
  <aside class="sidebar">

    <div class="sidebar-widget search-box">
    	<form method="post" action="http://expert-themes.com/html/global-industry/contact.html">
        <div class="form-group">
          <input type="search" name="search-field" value="" placeholder="Search ..." required>
          <button type="submit"><span class="icon fa fa-search"></span></button>
        </div>
      </form>
    </div>

    <div class="sidebar-widget sidebar-blog-category">
      <div class="sidebar-title">
        <h2>Kategori</h2>
        <div class="separater"></div>
      </div>
      <ul class="cat-list">
        @foreach($categories as $category)
        <li class="clearfix"><a href="{{ route('kategori.list', $category->slug) }}">{{ $category->category_name }} <span>{{ $category->news->count() }}</span></a></li>
        @endforeach
      </ul>
    </div>

    @if($latest->count() > 0)
    <div class="sidebar-widget popular-posts">
      <div class="sidebar-title">
        <h2>Artikel Terkini</h2>
        <div class="separater"></div>
      </div>
      @foreach($latest as $news)
      <article class="post">
      	<figure class="post-thumb"><img src="{{ url('storage/uploads/news/'. $news->picture ) }}" alt=""><a href="{{ route('artikel.detail', $news->slug) }}" class="overlay-box"><span class="icon fa fa-link"></span></a></figure>
        <div class="text"><a href="{{ route('artikel.detail', $news->slug) }}">{{ $news->title }}</a></div>
        <div class="post-info">{{ $news->comments->count() }}</div>
      </article>
      @endforeach
    </div>
    @endif

    <div class="sidebar-widget popular-tags">
      <div class="sidebar-title">
        <h2>Tags</h2>
        <div class="separater"></div>
      </div>
      @foreach($tags as $tag)
      <a href="{{ route('tag.list', $tag->tag_slug) }}">{{ $tag->tag_name }}</a>
      @endforeach
    </div>

  </aside>
</div>
