@extends('layouts.front.template')
@section('content')
@section('metatitle', "Kontak Kami")
@section('metadescription', "Kontak Kami")
@section('front_title', "Kontak Kami")
<div class="wt-bnr-inr overlay-wraper" style="background-image:url({{ asset('front/images/banner/blog-banner.jpg') }});">
  <div class="overlay-main bg-black" style="opacity:0.5;"></div>
    <div class="container">
        <div class="wt-bnr-inr-entry">
        </div>
    </div>
</div>

<!-- SECTION CONTENTG START -->
<div class="section-full p-t80 p-b50">
    <div class="container">

        <div class="section-content m-b50">
            <div class="row">

            	<!-- LOCATION BLOCK-->
                <div class="wt-box col-md-6">
                    <h4 class="text-uppercase">Lokasi</h4>
                    <div class="wt-separator-outer m-b30">
                       <div class="wt-separator style-square">
                           <span class="separator-left bg-primary"></span>
                           <span class="separator-right bg-primary"></span>
                       </div>
                   </div>
                    <div class="gmap-outline m-b30">
                        <div id="gmap_canvas" class="google-map"></div>
                    </div>
                </div>

                <!-- CONTACT FORM-->
                <div class="wt-box col-md-6">
                    <h4 class="text-uppercase">Kontak Form</h4>
                    <div class="wt-separator-outer m-b30">
                        <div class="wt-separator style-square">
                           <span class="separator-left bg-primary"></span>
                           <span class="separator-right bg-primary"></span>
                        </div>
                    </div>

                    <div id="msgSubmit"></div>
                    <div id="loading"></div>
                    <form id="contactform" data-toggle="validator" class="shake scroll-reveal">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input id="name" name="name" type="text" required class="form-control" placeholder="Nama">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                        <input id="email" name="email" type="text" class="form-control" required placeholder="Email">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon v-align-m"><i class="fa fa-pencil"></i></span>
                                        <textarea id="message" name="message" rows="4" class="form-control " required placeholder="Pesan"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <button name="submit" type="submit" value="Submit" class="site-button skew-icon-btn m-r15">Kirim <i class="fa fa-angle-double-right"></i></button>
                                <button name="Resat" type="reset" value="Reset"  class="site-button skew-icon-btn" >Reset <i class="fa fa-angle-double-right"></i></button>
                            </div>
                        </div>
            		    </form>

                </div>

            </div>
        </div>

        <!-- CONTACT DETAIL BLOCK -->
        <div class="section-content ">
            <div class="row">
                <div class="wt-box col-md-12">
                    <h4 class="text-uppercase">Detail Kontak</h4>
                    <div class="wt-separator-outer m-b30">
                        <div class="wt-separator style-square">
                           <span class="separator-left bg-primary"></span>
                           <span class="separator-right bg-primary"></span>
                       </div>
                    </div>
                    <div class="row">

                        <div class="col-md-4 col-sm-12 m-b30">
                            <div class="wt-icon-box-wraper center p-a30 bdr-2 bdr-gray-light">
                                <div class="wt-icon-box-sm bg-primary m-b20 corner"><span class="icon-cell"><i class="fa fa-phone"></i></span></div>
                                <div class="icon-content">
                                    <h5>Telepon</h5>
                                    <p>{{ $identities->phone }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 m-b30">
                            <div class="wt-icon-box-wraper center p-a30 bdr-2 bdr-gray-light">
                                <div class="wt-icon-box-sm bg-primary m-b20 corner"><span class="icon-cell"><i class="fa fa-envelope"></i></span></div>
                                <div class="icon-content">
                                    <h6>Email</h6>
                                    <p>{{ $identities->mail_address }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 m-b30">
                            <div class="wt-icon-box-wraper center p-a30 bdr-2 bdr-gray-light">
                                <div class="wt-icon-box-sm bg-primary m-b20 corner"><span class="icon-cell"><i class="fa fa-map-marker"></i></span></div>
                                <div class="icon-content">
                                    <h5>Alamat</h5>
                                    <p>{{ $identities->address }}</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- SECTION CONTENT END -->

@endsection

@push('js')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6J-39nNLYLNSDbTuZIrdJgRzd9ZKpClE&callback=init"></script>
<script src="{{ asset('front/contact-form/js/validator.min.js') }}"></script>
<script src="{{ asset('js/share.js') }}"></script>
<script>
  if(jQuery('#gmap_canvas').length){

    function init_map1() {

      var myOptions = {
        zoom: 17,
        center: new google.maps.LatLng(-6.1648079, 106.8734557),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        // This is where you would paste any style found on Snazzy Maps.
        styles: [ { "featureType": "poi.business", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.park", "elementType": "labels.text", "stylers": [ { "visibility": "off" } ] } ]
      };
      // Let's also add a marker while we're at it
      map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
      marker = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(-6.1648079, 106.8734557),
        optimized: false,
        icon: new google.maps.MarkerImage('front/images/marker.png')
      });

      marker.setDraggable(false);
      @php $identity = \App\IdentityModel::first() @endphp
      infowindow = new google.maps.InfoWindow({
        content: '<strong>{{ $identity->website_name }}</strong><br>{{ $identity->address }}<br>'
      });
      google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map, marker);
      });
    }
    google.maps.event.addDomListener(window, 'load', init_map1);

  }


	jQuery('.df-share, .df-tweet, .df-pin, .df-pluss, .df-link').click(function(event) {
		var width  = 575,
			height = 400,
			left   = (jQuery(window).width()  - width)  / 2,
			top    = (jQuery(window).height() - height) / 2,
			url    = this.href,
			opts   = 'status=1' +
					 ',width='  + width  +
					 ',height=' + height +
					 ',top='    + top    +
					 ',left='   + left;

		window.open(url, 'twitter', opts);

		return false;
	});

	var TWEET_URL = "https://twitter.com/intent/tweet";

	jQuery(".df-tweet").each(function() {
		var elem = jQuery(this),
		// Use current page URL as default link
		url = encodeURIComponent(elem.attr("data-url") || document.location.href),
		// Use page title as default tweet message
		text = elem.attr("data-text") || document.title,
		via = elem.attr("data-via") || "",
		related = encodeURIComponent(elem.attr("data-related")) || "",
		hashtags = encodeURIComponent(elem.attr("data-hashtags")) || "";

		// Set href to tweet page
		elem.attr({
			href: TWEET_URL + "?original_referer=" +
					encodeURIComponent(document.location.href) +
					"&source=tweetbutton&text=" + text + "&url=" + url + "&via=" + via + "&hashtags=" + hashtags,
			target: "_blank"
		});

	});
</script>
<script>
$("#contactform").validator().on("submit", function (event) {
    $('#loading').html('<h5 data-text="Loading...">Loading...</h5>');
    if (event.isDefaultPrevented()) {
        formError();
        submitMSG(false, "Silahkan isi form dengan benar.");
    } else {
        event.preventDefault();
        submitForm();
    }
});

function submitForm(){
    var name = $("#name").val();
    var email = $("#email").val();
    var subject = "Pesan dari website";
    var message = $("#message").val();

    $.ajax({
        type: "POST",
        url: "{{ route('home.contact') }}",
        data: "contact_name=" + name + "&contact_email=" + email + "&contact_subject=" + subject + "&contact_message=" + message + "&_token={{ csrf_token() }}",
        success : function(text){
            if (text == "success"){
                formSuccess();
            } else {
                formError();
                submitMSG(false,text);
            }
        }
    });
}

function formSuccess(){
    $("#contactform")[0].reset();
    submitMSG(true, "Pesan anda telah terkirim !");
}

function formError(){
    $("#contactform").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
}

function submitMSG(valid, msg){
    if(valid){
        var msgClasses = "h5 alert alert-success";
    }else {
        var msgClasses = "h5 alert alert-danger";
    }
    $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
    $("#loading").removeClass().addClass('hidden');
}
</script>
@endpush
