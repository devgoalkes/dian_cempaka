@extends('layouts.front.template')
@section('metatitle', "$static->title")
@section('metadescription', "$static->title")
@section('front_title', $static->title)

@section('content')

<!-- SECTION CONTENT START -->
<div class="section-full p-t80 p-b50">
    <div class="container">
        <div class="row">


        	<!-- RIGHT PART START -->
            <div class="col-md-12">

                <div class="blog-post date-style-1 blog-detail">

                    <!-- <div class="wt-post-media wt-img-effect zoom-slow">
                        <a href="javascript:void(0);"><img src="{{ url('storage/uploads/static_pages/'. $static->picture) }}" alt=""></a>
                    </div> -->
                    <div class="wt-post-text">
                        {!! $static->content !!}
                    </div>

                </div>
            </div>
            <!-- RIGHT PART END -->

        </div>
    </div>
</div>
<!-- SECTION CONTENT END -->

</div>
<!-- CONTENT END -->

@endsection
