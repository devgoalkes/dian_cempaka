@extends('layouts.front.template')
@section('content')
@section('metatitle', "Tentang Kami")
@section('metadescription', "Tentang Kami")
@section('front_title', "Tentang Kami")
<div class="wt-bnr-inr overlay-wraper" style="background-image:url({{ asset('front/images/banner/blog-banner.jpg') }});">
  <div class="overlay-main bg-black" style="opacity:0.5;"></div>
    <div class="container">
        <div class="wt-bnr-inr-entry">
        </div>
    </div>
</div>

<!-- ABOUT COMPANY SECTION START -->
<div class="section-full p-tb20" style="background-image:url(images/background/pic-c-1bg.png); background-position:left bottom; background-repeat:no-repeat;">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-5">
                <div class="about-com-pic">
                    <img src="front/images/dian_cempaka1.jpg" alt="" class="img-responsive"/><br>
                    <img src="front/images/dian_cempaka2.jpg" alt="" class="img-responsive hidden-xs"/><br>
                    <img src="front/images/dian_cempaka3.jpg" alt="" class="img-responsive hidden-xs"/><br>
                </div>
            </div>
            <div class="col-md-7 col-sm-7">
                <div class="section-head text-left">
                    <h2 class="text-uppercase">Tentang Kami</h2>
                    <div class="wt-separator-outer">
                        <div class="wt-separator style-square">
                            <span class="separator-left bg-primary"></span>
                            <span class="separator-right bg-primary"></span>
                        </div>
                    </div>
                    <p>PT Dian Cempaka adalah pelopor dalam Industri Pipa Tembaga dan telah menjadi agen tunggalpipa tembaga merek Kembla (Australia) di Indonesia selama lebih dari 25 tahun.</p>
                    <p>PT Dian Cempaka telah beroperasi di Jakarta sejak 1984 (beroperasi dengan nama PT. Prima Bintang Indorejeki sampai 1994). Selama dua dekade terakhir kami telah mengumpulkan kepercayaan pasar yang kuat dan sehat melalui prinsip sederhana yang kami ikuti: Dapatkan Kepercayaan dengan kualitas produk yang terpercaya.</p>
                    <p>Kami telah membangun reputasi yang kuat untuk menyediakan produk di banyak proyek mulai dari apartemen, rumah sakit, hotel, dan banyak proyek lainnya dengan kualitas dan harga yang kompetitif.</p>
                    <p>Mengetahui banyak vendor memberi Anda banyak pilihan, tetapi menyia-nyiakan lebih banyak waktu dalam membuat perbandingan. PT. Dian Cempaka akan menjadi pilihan terbaik untuk perusahaan Anda, karena karena kami adalah distributor tunggal pipa Kembla di Indonesia, kami selalu menjamin ketersediaan stock dan kualitas, dengan harga yang kompetitif.</p>
                    <p>Perekonomian Indonesia berkembang pesat dan untuk mengatasi permintaan pasar yang sangat besar, kami juga melakukan yang terbaik untuk menyediakan layanan "satu atap" bagi pelanggan kami, di mana kami menyelesaikan berbagai produk kami, seperti:</p>
                      <ul style="padding-left:15px;">
                        <li>Pair Coil Tubes</li>
                        <li>Copper fittings (Elbow, Tee, Knee)</li>
                        <li>Refrigerant</li>
                        <li>And other mechanical equipment</li>
                      </ul>
                    <p>Akibatnya, Anda tidak perlu mencari vendor lain ketika membutuhkan tabung tembaga untuk kebutuhan Anda, karena kami berkomitmen untuk menyediakan produk kami di waktu yang tepat dan di tempat yang tepat dengan toleransi nol.</p>

                    <p>Tujuan kami adalah untuk menjadi salah satu pemegang pangsa pasar terkemuka di salah satu pasar paling kompetitif di dunia di mana kepuasan Pelanggan adalah tujuan akhir dari perusahaan kami.</p>

                    <p>Untuk informasi lebih rinci tentang kantor kami dan produk kami, silakan hubungi kami pada halaman kontak yang tersedia.</p>
                </div>

            </div>
        </div>

    </div>

</div>
<!-- ABOUT COMPANY SECTION END -->

<!-- ABOUT COMPANY SECTION START -->
<div class="section-full p-tb20" style="background-image:url(); background-position:left bottom; background-repeat:no-repeat;">
    <div class="container">
        <div class="row">

            <div class="col-md-9 col-sm-9">
                <div class="section-head text-left">
                    <h2 class="text-uppercase">Tentang Kembla</h2>
                    <div class="wt-separator-outer">
                        <div class="wt-separator style-square">
                            <span class="separator-left bg-primary"></span>
                            <span class="separator-right bg-primary"></span>
                        </div>
                    </div>
                    <p>
                      MM Kembla, sebuah divisi dari Metal Manufactures Limited, didirikan pada tahun 1916 di Port Kembla, NSW, dan dari daerah inilah nama merek terkenal "Kembla" berasal.
                    </p>
                    <p>
                      MM Kembla menyediakan berbagai macam tabung tembaga berkualitas tinggi, tabung stainless steel dan perlengkapan pelengkap dan produk aksesori yang melayani konstruksi bangunan, HVAC, pendinginan, industri energi dan transportasi, produsen peralatan asli dan pasar industri. MM Kembla melayani beragam pelanggan di seluruh Australia, Selandia Baru, Asia, dan Timur Tengah.
                    </p>
                    <p>
                      Produk MM Kembla diproduksi untuk standar Australia dan internasional yang ketat. Semua produk MM Kembla didukung oleh reputasi MM Kembla untuk kualitas, layanan, dan layanan pelanggan.
                    </p>
                </div>

            </div>

            <div class="col-md-3 col-sm-3">
                <div class="about-com-pic p-t80">
                    <img src="{{ asset('front/images/logo-kembla.gif') }}" alt="" class="img-responsive"/>
                </div>
                <div class="about-com-pic p-t10">
                    <img src="{{ asset('front/images/kembla.jpg') }}" alt="" class="img-responsive"/>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- ABOUT COMPANY SECTION END -->

<div class="section-full p-tb10" style="background-image:url(); background-position:left bottom; background-repeat:no-repeat;">
    <div class="container">
        <div class="row">

            <div class="col-md-7 col-sm-7">
                <div class="section-head text-left">
                    <h2 class="text-uppercase">Sertifikat</h2>
                    <div class="wt-separator-outer">
                        <div class="wt-separator style-square">
                            <span class="separator-left bg-primary"></span>
                            <span class="separator-right bg-primary"></span>
                        </div>
                    </div>

                    <h3>Copper Tube</h3>

                    <ul style="padding-left:15px;">
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-ISO/Certificate-QEC000201-20161025.pdf" target="_blank" title="KemPress Design &amp; Installation Guide">ISO9001 Quality Management System Certificate of Registration</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Copper/AS-NZS-4020-Testing-for-products-in-contact-with-drinking-water-1703.pdf" target="_blank" title="AS/NZS 4020 Testing for products in contact with drinking water">AS/NZS 4020 Testing for products&nbsp;in contact&nbsp;with drinking water</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Copper/Watermark-Certificate-of-Confirmity-AS-14322004-Copper-Tube-2015.pdf" target="_blank" title="AS1432 Copper Tube Watermark Certificate of Conformity">AS1432&nbsp;Copper Tube Watermark&nbsp;Certificate of Conformity</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Copper/ASNZS-1571-Standards-Mark-Licence-Exp-2020.pdf" target="_blank" title="AS/NZS 1571 Copper Tube Standards Mark Licence">AS/NZS 1571 Copper Tube Standards Mark Licence</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Export/Standards-Mark-Licence-BS-EN-1057-Copper-Tube-180301.pdf" target="_blank">EN 1057 Copper Tube Standards Mark Licence</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Export/BS-EN-1057-Certification-BSI-2015.pdf" target="_blank">EN 1057 Copper Tube BSI Benchmark Licence </a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Export/Certificate-of-Conformity-PSB-Singapore-till-2018.pdf" target="_blank" title="EN 1057 Certificate of Conformity from PSB Singapore ">EN 1057 Certificate of Conformity&nbsp;from&nbsp;PSB Singapore</a>&nbsp;</li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Export/SIRIM-Copper-Tube-for-Water-Gas2014.pdf" target="_blank" title="EN 1057 Product Certification Licence from SIRIM Malaysia">EN 1057 Product Certification Licence from SIRIM Malaysia</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Export/SPAN-Copper-TubeSAI-GLOBAL16-03-16.pdf" target="_blank" title="EN 1057 Product Certification Licence from SPAN Malaysia">EN 1057 Product Certification Licence from SPAN Malaysia</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Export/Kahramaa-Approval-valid-290718.pdf" target="_blank" title="EN 1057 Product Approval Certificate from KAHRAMAA Qatar ">EN 1057 Product Approval Certificate from KAHRAMAA Qatar</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Export/Standards-Mark-Licence-BS-EN-13348-Copper-Tube-Certification-2018.pdf" target="_blank" title="MM Kembla EN 13348 Copper Tube Standards Mark Licence">EN 13348 Copper Tube Standards Mark Licence&nbsp;</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Export/SIRIM-Copper-Tube-for-Medical-Gas2014.pdf" target="_blank" title="EN 13348 Product Certification Licence from SIRIM Malaysia ">EN 13348 Product Certification Licence from SIRIM&nbsp;Malaysia </a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Export/Standards-Mark-Licence-ASTM-B88-09-SMKP21866.pdf" target="_blank" title="ASTM BB88-09 Copper Tube Standards Mark Licence">ASTM B88-09 Copper Tube Standards Mark Licence</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Export/Standards-Mark-Licence-ASTM-B819-Certification-2013.pdf" target="_blank" title="ASTM B819 Copper Tube Standards Mark Licence">ASTM B819 Copper Tube Standards Mark Licence</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Export/Standards-Mark-Licence-ASTM-B280-13.pdf" target="_blank" title="ASTM B280 Copper Tube Standards Mark Licence">ASTM B280 Copper Tube Standards Mark Licence</a></li>
                    </ul>

                    <h3>KemPress&reg; Copper, Copper Fittings &amp; Assemblies</h3>

                    <ul style="padding-left:15px;">
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-KNZ-and-Fittings/Watermark-Certificate-of-Conformity-AS-3688-Fittings-WMKA1287-160704.pdf" target="_blank" title="AS 3688 Kembla Plumbing Fittings and DN65-100 Kempress Fittings Watermark Certificate of Conformity ">AS 3688&nbsp;Kembla Plumbing Fittings and DN65-100 Kempress Fittings Watermark Certificate of Conformity&nbsp;</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-KPF-and-Assemblies/Watermark-Certificate-of-Conformity-ASNZS-3718-Tapware-2015.pdf" target="_blank">AS/NZS 3718 Tapware Watermark Certificate of Conformity</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/KemPress/KemPress-Watermark-Certificate-of-Conformity-23087-1592017.pdf" target="_blank" title="AS 3688 KemPress® Fittings (Copper, Copper Alloy &amp; Stainless) Watermark Certificate of Conformity">AS 3688 KemPress&reg; Fittings&nbsp;(Copper, Copper Alloy&nbsp;&amp; Stainless)&nbsp;Watermark Certificate of Conformity</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Export/EN1254-KemPress-Fittings-WRAS-UK-Approval-160118.pdf" target="_blank" title="EN1254 KemPress® Fittings WRAS UK Approval">EN1254 KemPress&reg; Fittings WRAS UK Approval</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-Export/EN1254-KemPress-Fittings-HDB-Singapore-Certification.pdf" target="_blank" title="EN1254 KemPress® Fittings HDB Singapore Certification">EN1254 KemPress&reg; Fittings HDB Singapore Certification</a></li>
                    </ul>

                    <h3>KemPress&reg; Stainless &amp; KemDrain&trade; Stainless</h3>

                    <ul style="padding-left:15px;">
                    	<li><a href="http://www.kembla.com/assets/Uploads/KemPress/23151MMK-CertificateofConformity-172013.pdf" target="_blank" title="AS5200 Stainless Tube Watermark Certificate of Conformity">AS5200 Stainless Tube Watermark Certificate of Conformity</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/KemPress/MMK-Certificate-of-Conformity-23087-1572013.pdf" target="_blank">AS 3688 KemPress&reg; Fittings&nbsp;(Copper, Copper Alloy&nbsp;&amp; Stainless)&nbsp;Watermark Certificate of Conformity</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/KemPress-Stainless/KemPress-Stainless-FM-Certificate.pdf" target="_blank" title="KemPress Stainless FM Certification">KemPress Stainless FM Certification</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/KemPress-Stainless/Certificate-of-Confirmity-AS-NZS-4020-161222.pdf" target="_blank">AS/NZS 4020 Certificate of Conformity - KemPress&reg; Stainless Tube</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-KemDrain-Stainless/SA-WaterKemDrain-ApprovalMay-2016.pdf" target="_blank" title="SA Water Product Approval - KemDrain™">SA Water Product Approval - KemDrain&trade;</a></li>
                    	<li><a href="http://www.kembla.com/assets/Uploads/Certifications-KemDrain-Stainless/Water-Corporation-Approval-for-KemDrainJuly-2016.pdf" target="_blank" title="Water Corporation Approval - KemDrain™">Water Corporation Approval - KemDrain&trade;</a></li>
                    </ul>

                </div>

            </div>
            <div class="col-md-5 col-sm-5 p-tb80">
                <div class="about-com-pic">
                    <img src="{{ asset('front/images/sertifikat.jpg') }}" alt="" class="img-responsive"/>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- ABOUT COMPANY SECTION END -->


<!-- OUR BEST PROJECT SCETION START -->
<div class="section-full p-t20 p-b20">
    <div class="container">

    	<!-- TITLE START -->
        <div class="section-head text-center">
            <h2 class="text-uppercase">Pengalaman Proyek yang Relevan</h2>
            <span class="title-small">
              Bagian ini akan mencantumkan beberapa proyek yang disediakan oleh kami menggunakan tabung Tembaga Kembla untuk pipa pendingin, medis dan pipa saluran air.
            </span>
            <div class="wt-separator bg-primary"></div>
        </div>
        <!-- TITLE END -->

         <!-- OUR BEST PROJECT BLOCK START -->
         <div class="section-content">
            <div class="row">
                <!-- COLUMNS 1 -->
                <div class="col-md-3 col-sm-3">
                    <div class="wt-box m-b30">
                        <div class="wt-media">
                            <a href="javascript:void(0);"><img src="front/images/our-work/pic2.jpg" alt=""></a>
                      </div>
                        <div class="wt-info">
                            <h4 class="wt-title m-t20"><a href="javascript:void(0);">Kedutaan Besar</a></h4>

                        </div>
                    </div>
                </div>
                <!-- COLUMNS 2 -->
                <div class="col-md-3 col-sm-3">
                    <div class="wt-box m-b30">
                        <div class="wt-media">
                            <a href="javascript:void(0);"><img src="front/images/our-work/pic4.jpg" alt=""></a>
                      </div>
                        <div class="wt-info">
                            <h4 class="wt-title m-t20"><a href="javascript:void(0);">Kantor / Pabrik</a></h4>

                        </div>
                    </div>
                </div>
                <!-- COLUMNS 3 -->
                <div class="col-md-3 col-sm-3">
                    <div class="wt-box m-b30">
                        <div class="wt-media">
                            <a href="javascript:void(0);"><img src="front/images/our-work/pic3.jpg" alt=""></a>
                      </div>
                        <div class="wt-info">
                            <h4 class="wt-title m-t20"><a href="javascript:void(0);">Hotel / Apartemen</a></h4>

                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="wt-box m-b30">
                        <div class="wt-media">
                            <a href="javascript:void(0);"><img src="front/images/our-work/pic1.jpg" alt=""></a>
                      </div>
                        <div class="wt-info">
                            <h4 class="wt-title m-t20"><a href="javascript:void(0);">Rumah Sakit</a></h4>

                        </div>
                    </div>
                </div>
            </div>
        </div>
         <!-- OUR BEST PROJECT BLOCK END -->


         <div class="section-content">
            <div class="row">
              <div class="col-md-3 col-sm-3">

              </div>
              <div class="col-md-3 col-sm-3">
                  <div class="wt-box m-b30">
                      <div class="wt-media">
                          <a href="javascript:void(0);"><img src="front/images/our-work/pic5.jpg" alt=""></a>
                    </div>
                      <div class="wt-info">
                          <h4 class="wt-title m-t20"><a href="javascript:void(0);">Sekolah</a></h4>

                      </div>
                  </div>
              </div>
                <!-- COLUMNS 1 -->
                <div class="col-md-3 col-sm-3">
                    <div class="wt-box m-b30">
                        <div class="wt-media">
                            <a href="javascript:void(0);"><img src="front/images/our-work/pic6.jpg" alt=""></a>
                      </div>
                        <div class="wt-info">
                            <h4 class="wt-title m-t20"><a href="javascript:void(0);">Rumah Ibadah</a></h4>

                        </div>
                    </div>
                </div>
                <!-- COLUMNS 2 -->
                <div class="col-md-3 col-sm-3">

                </div>

            </div>
        </div>

    </div>
</div>
<!-- OUR BEST PROJECT SCETION END -->


<!-- OUR TEAM MEMBER SECTION START -->
<div class="section-full wt-our-team bg-white p-t20 p-b80">
    <div class="container">

    	<!-- TITTLE START -->
        <div class="section-head text-center">
            <h2 class="text-uppercase">Team Profesional</h2>
            <div class="wt-separator-outer">
                <div class="wt-separator style-square">
                    <span class="separator-left bg-primary"></span>
                    <span class="separator-right bg-primary"></span>
                </div>
            </div>
            <p>Kami selalu didukung oleh tim yang solid dan professional demi menjaga kepercayaan klien kami.</p>
        </div>
        <!-- TITLE END -->

        <div class="section-content">
            <div class="row">

                <!-- COLUMNS 1 -->
                <div class="col-md-3 col-sm-6 col-xs-6 col-xs-100pc m-t30">
                    <div class="wt-team-four">
                        <div class="wt-team-media">
                            <a href="javascript:void(0);"><img src="front/images/our-team/marketing.jpg" alt="" ></a>
                        </div>
                        <div class="wt-team-info">
                            <div class="wt-team-skew-block p-t10">
                                <div class="social-icons-outer bg-primary  p-a25">
                                    Marketing Division
                                </div>
                             </div>
                        </div>
                    </div>
                </div>
                <!-- COLUMNS 2 -->
                <div class="col-md-3 col-sm-6 col-xs-6 col-xs-100pc m-t30">
                    <div class="wt-team-four">
                        <div class="wt-team-media">
                            <a href="javascript:void(0);"><img src="front/images/our-team/collector.jpg" alt="" ></a>
                        </div>
                        <div class="wt-team-info">
                            <div class="wt-team-skew-block p-t10">
                                <div class="social-icons-outer bg-primary  p-a25">
                                  Collector Division
                                </div>
                          </div>
                      </div>
                    </div>
                </div>
                <!-- COLUMNS 3 -->
                <div class="col-md-3 col-sm-6 col-xs-6 col-xs-100pc m-t30">
                    <div class="wt-team-four">
                        <div class="wt-team-media">
                            <a href="javascript:void(0);"><img src="front/images/our-team/acounting.jpg" alt="" ></a>
                        </div>
                        <div class="wt-team-info">
                            <div class="wt-team-skew-block p-t10">
                                <div class="social-icons-outer bg-primary  p-a25">
                                  Accounting Division
                                </div>
                          </div>
                      </div>
                    </div>
                </div>
                <!-- COLUMNS 4 -->
                <div class="col-md-3 col-sm-6 col-xs-6 col-xs-100pc m-t30">
                    <div class="wt-team-four">
                        <div class="wt-team-media">
                            <a href="javascript:void(0);"><img src="front/images/our-team/team.jpg" alt="" ></a>
                        </div>
                        <div class="wt-team-info">
                            <div class="wt-team-skew-block p-t10">
                                <div class="social-icons-outer bg-primary  p-a25">
                                  Dian Cempaka Team
                                </div>
                          </div>
                      </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
<!-- OUR TEAM MEMBER SECTION End -->
@endsection

@push('js')
<script src="{{ asset('js/share.js') }}"></script>
<script>
	jQuery('.df-share, .df-tweet, .df-pin, .df-pluss, .df-link').click(function(event) {
		var width  = 575,
			height = 400,
			left   = (jQuery(window).width()  - width)  / 2,
			top    = (jQuery(window).height() - height) / 2,
			url    = this.href,
			opts   = 'status=1' +
					 ',width='  + width  +
					 ',height=' + height +
					 ',top='    + top    +
					 ',left='   + left;

		window.open(url, 'twitter', opts);

		return false;
	});

	var TWEET_URL = "https://twitter.com/intent/tweet";

	jQuery(".df-tweet").each(function() {
		var elem = jQuery(this),
		// Use current page URL as default link
		url = encodeURIComponent(elem.attr("data-url") || document.location.href),
		// Use page title as default tweet message
		text = elem.attr("data-text") || document.title,
		via = elem.attr("data-via") || "",
		related = encodeURIComponent(elem.attr("data-related")) || "",
		hashtags = encodeURIComponent(elem.attr("data-hashtags")) || "";

		// Set href to tweet page
		elem.attr({
			href: TWEET_URL + "?original_referer=" +
					encodeURIComponent(document.location.href) +
					"&source=tweetbutton&text=" + text + "&url=" + url + "&via=" + via + "&hashtags=" + hashtags,
			target: "_blank"
		});

	});
</script>
@endpush
