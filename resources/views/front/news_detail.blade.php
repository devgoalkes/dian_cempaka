@extends('layouts.front.template')
@section('content')
@section('metatitle', "$query->title")
@section('metadescription', "$query->title")
@section('front_title', $query->title)

<div class="wt-bnr-inr overlay-wraper" style="background-image:url({{ asset('front/images/banner/blog-banner.jpg') }});">
  <div class="overlay-main bg-black" style="opacity:0.5;"></div>
    <div class="container">
        <div class="wt-bnr-inr-entry">
        </div>
    </div>
</div>

<!-- SECTION CONTENT START -->
<div class="section-full p-t80 p-b50">
    <div class="container">
        <div class="row">

        	<!-- RIGHT PART START -->
            <div class="col-md-9">

                <div class="blog-post date-style-1 blog-detail">

                    <div class="wt-post-media wt-img-effect zoom-slow">
                        <a href="javascript:void(0);"><img src="{{ url('storage/uploads/static_pages/'. $query->picture) }}" alt=""></a>
                    </div>
                    <div class="wt-post-title ">
                        <h3 class="post-title"><a href="javascript:void(0);">{{ $query->title }}</a></h3>
                    </div>
                    <div class="wt-post-meta ">
                      <ul>
                        <li class="post-date"> <i class="fa fa-calendar"></i><strong>{{ $tglIndo($query->news_created_at) }}</strong> </li>
                        <li class="post-comment"><i class="fa fa-comments"></i> <a href="javascript:void(0);">0</a> </li>
                      </ul>
                    </div>
                    <div class="wt-post-text">
                        {!! $query->content !!}
                    </div>

                </div>
                @if (session('success'))
                  <div class="alert alert-success" id="success-alert">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      {{ session('success') }}
                  </div>
                @endif
                <div class="clear" id="comment-list">
                    <div class="comments-area" id="comments">
                        <h2 class="comments-title">{{ $comment->count() }} Komentar</h2>
                        <div class="">

                            <!-- COMMENT LIST -->
                            <ol class="comment-list">
                              @foreach($comment as $c)
                                <li  class="comment">

                                    <!-- COMMENT LIST -->
                                    <div class="comment-body">
                                        <div class="comment-meta">
                                            <a href="javascript:void(0);">{{ $c->created_at }}</a>
                                        </div>

                                        <p>{{ $c->content }}</p>
                                    </div>

                                </li>
                                @endforeach
                            </ol>
                            <!-- COMMENT LIST END -->

                            <div class="comments-pager">{{ $comment->links() }}</div>

                            <!-- LEAVE A  REPLY START -->
                            <div class="comment-respond" id="respond">

                                <h3 class="comment-reply-title" id="reply-title">Berikan Komentar</h3>

                                <form class="comment-form" id="commentform" method="post" action="{{ route('comment.post') }}">
                                    {{ csrf_field() }}
                                    <input type='hidden' name='news_id' value='{{ $query->news_id }}'/>
                                    <p class="comment-form-author">
                                        <label for="author">Nama <span class="required">*</span></label>
                                        <input type="text" value="" name="comment_name"  placeholder="Nama">
                                    </p>

                                    <p class="comment-form-url">
                                        <label for="url">Website</label>
                                        <input type="text"  value=""  placeholder="Website"  name="url">
                                    </p>

                                    <p class="comment-form-comment">
                                        <label for="comment">Komentar</label>
                                        <textarea rows="4" name="content" placeholder="Komentar"></textarea>
                                    </p>

                                    <p class="form-submit">
                                        <button class="site-button skew-icon-btn m-r15" type="submit">Kirim<i class="fa fa-angle-double-right"></i></button>
                                    </p>

                                </form>

                            </div>
                            <!-- LEAVE A  REPLY END -->

                        </div>
                    </div>
                </div>

            </div>
            <!-- RIGHT PART END -->

            @include('layouts.front.partials._sidebar')

        </div>
    </div>
</div>
<!-- SECTION CONTENT END -->

@endsection

@push('js')
<script src="{{ asset('front/js/comment.js') }}"></script>
<script src="{{ asset('js/share.js') }}"></script>
<script>
	jQuery('.df-share, .df-tweet, .df-pin, .df-pluss, .df-link').click(function(event) {
		var width  = 575,
			height = 400,
			left   = (jQuery(window).width()  - width)  / 2,
			top    = (jQuery(window).height() - height) / 2,
			url    = this.href,
			opts   = 'status=1' +
					 ',width='  + width  +
					 ',height=' + height +
					 ',top='    + top    +
					 ',left='   + left;

		window.open(url, 'twitter', opts);

		return false;
	});

	var TWEET_URL = "https://twitter.com/intent/tweet";

	jQuery(".df-tweet").each(function() {
		var elem = jQuery(this),
		// Use current page URL as default link
		url = encodeURIComponent(elem.attr("data-url") || document.location.href),
		// Use page title as default tweet message
		text = elem.attr("data-text") || document.title,
		via = elem.attr("data-via") || "",
		related = encodeURIComponent(elem.attr("data-related")) || "",
		hashtags = encodeURIComponent(elem.attr("data-hashtags")) || "";

		// Set href to tweet page
		elem.attr({
			href: TWEET_URL + "?original_referer=" +
					encodeURIComponent(document.location.href) +
					"&source=tweetbutton&text=" + text + "&url=" + url + "&via=" + via + "&hashtags=" + hashtags,
			target: "_blank"
		});

	});
</script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });
});
</script>
@endpush
