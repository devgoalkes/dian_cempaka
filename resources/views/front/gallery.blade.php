@extends('layouts.front.template')
@section('front_title', 'Home')
@section('metatitle', "Home")
@section('metadescription', "FRES Group, alat kesehatan, gas medis, gas medis indonesia")
@section('content')

<div class="wt-bnr-inr overlay-wraper" style="background-image:url({{ asset('front/images/banner/blog-banner.jpg') }});">
  <div class="overlay-main bg-black" style="opacity:0.5;"></div>
    <div class="container">
        <div class="wt-bnr-inr-entry">
            <h1 class="text-white">Pengalaman Kami</h1>
        </div>
    </div>
</div>

<!-- SECTION CONTENT START -->
<div class="section-full p-t80 p-b50">
    <!-- PAGINATION START -->
    <div class="filter-wrap p-a15">
        <ul class="masonry-filter link-style  text-uppercase">
            <li class="active"><a data-filter="*" href="#">All</a></li>
            @foreach($albums as $album)
            <li><a data-filter=".{{ $album->slug }}" href="#">{{ $album->title }}</a></li>
            @endforeach
        </ul>
    </div>
    <!-- PAGINATION END -->

    <!-- GELLERY CONTENT -->
    <div class="container-fluid">
        <div class="row">
            <div class="portfolio-wrap mfp-gallery no-col-gap">
                @foreach($galeries as $galery)

                <div class="masonry-item {{ $galery->album->slug }} col-lg-4 col-md-6 col-sm-6">
                    <div class="wt-box p-a15">
                        <div class="wt-thum-bx wt-img-effect zoom">
                            <img src="{{ asset('storage/uploads/galery/'. $galery->picture ) }}" alt="">
                            <div class="wt-info-has p-a20 bg-white ">
                                <div class="wt-info p-b10">
                                    <h4 class="m-a0">{{ $galery->galery_title }}</h4>
                                </div>
                                <div class="wt-info-has-text">{{ $galery->description }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
    <!-- GELLERY END -->

</div>
<!-- SECTION CONTENT END  -->

@endsection
