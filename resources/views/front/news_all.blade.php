@extends('layouts.front.template')
@section('content')
@section('metatitle', "Semua Artikel")
@section('metadescription', "Semua Artikel")
@section('front_title', 'Semua Artikel')

<div class="wt-bnr-inr overlay-wraper" style="background-image:url({{ asset('front/images/banner/blog-banner.jpg') }});">
  <div class="overlay-main bg-black" style="opacity:0.5;"></div>
    <div class="container">
        <div class="wt-bnr-inr-entry">
        </div>
    </div>
</div>

<!-- SECTION CONTENT START -->
<div class="section-full p-t80 p-b50">
    <div class="container">
        <div class="row">

            <!-- LEFT PART START -->
            <div class="col-md-9">
              @foreach ($news as $r)
            	<!-- COLUMNS 1 -->
                <div class="blog-post blog-md date-style-1 clearfix">

                    <div class="wt-post-media wt-img-effect zoom-slow">
                      @if($r->picture == '')
                            <a href="javascript:void(0);"><img src="{{ url('storage/uploads/news/no-image.jpg') }}" alt="{!! $r->title !!}"></a>
                      @else
                            <a href="javascript:void(0);"><img src="{{ url('storage/uploads/news', $r->picture) }}" alt="{!! $r->title !!}"></a>
                      @endif
                    </div>
                    <div class="wt-post-info">

                        <div class="wt-post-title ">
                            <h3 class="post-title"><a href="{{ route('artikel.detail', $r->news_slug) }}">{{ $r->title }}</a></h3>
                        </div>
                        <div class="wt-post-meta ">
                          <ul>
                            <li class="post-date"> <i class="fa fa-calendar"></i><strong>{{ $tglJamIndo($r->news_created_at) }}</strong></li>
                            <li class="post-author"><i class="fa fa-user"></i><a href="javascript:void(0);">By <span>demongo</span></a> </li>
                            <li class="post-comment"><i class="fa fa-comments"></i> <a href="javascript:void(0);">0</a> </li>
                          </ul>
                        </div>
                        <div class="wt-post-text">
                            {!! str_limit(strip_tags($r->content), 200) !!} [...]
                        </div>
                        <div class="wt-post-readmore">
                             <a href="{{ route('artikel.detail', $r->news_slug) }}" title="READ MORE" rel="bookmark" class="site-button-link">Selengkapnya <i class="fa fa-angle-double-right"></i></a>
                        </div>

                        <div class="wt-post-tags">
                            <div class="post-tags">
                              @foreach(explode(',', $r->tag) as $tag)
                                <a href="{{ route('tag.list', $tag) }}">{{ $tag }} </a>
                              @endforeach
                            </div>
                        </div>

                    </div>

                </div>
                @endforeach

                <!-- PAGINATION START -->
                <div class="pagination-bx clearfix ">
                  {{ $news->links() }}
                </div>
                <!-- PAGINATION END -->

            </div>
            <!-- LEFT PART END -->

            @include('layouts.front.partials._sidebar')

        </div>
    </div>
</div>
<!-- SECTION CONTENT END -->

@endsection

@push('js')
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@endpush
