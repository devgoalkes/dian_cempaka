<div id="mailsub" class="notification" align="center">

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">

<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
    <tr><td style="height:80px;">&nbsp;</td></tr>
	<!--header -->
	<tr><td align="left" bgcolor="#ffffff">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr><td style="font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #57697e; font-weight:bold;">&nbsp;
      </td>
			</tr>
			<tr><td style="font-family: Arial, Helvetica, sans-serif; height:40px; font-size: 10px; color: #57697e; font-weight:bold;">
        {{ $tglJamIndo(now()) }}
      </td>
			</tr>
		</table>
    <hr />
	</td></tr>
	<!--header END-->

	<!--content 1 -->
	<tr><td align="center" bgcolor="#fbfcfd">
		<table width="90%" border="0" cellspacing="0" cellpadding="0">
			<tr><td align="center">
				<div>
  					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 30px; color: #57697e;">PT. Dian Cempaka</span>
				</div>
			</td></tr>
			<tr><td align="center">
				<div style="line-height: 24px;">
					<font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 15px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">
						Ini adalah email dari Customer yang diteruskan melalui sistem PT. Dian Cempaka. Mohon untuk segera ditindaklanjuti.
					</span></font>
				</div>
			</td></tr>
		</table>
    <hr />
	</td></tr>
	<!--content 1 END-->
  <!--content 2 -->
	<tr><td align="center" bgcolor="#fbfcfd">
		<table style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #57697e;" width="90%" border="0px" cellspacing="0" cellpadding="0">
			<tr><td align="center" colspan="3">&nbsp;</td></tr>
      <div style="line-height: 24px;">
      <tr><td width="100px" valign="top">Nama</td></t><td>:</td> <td> {{ $sender_name }}</td></tr>
      <tr><td valign="top">Email</td></t><td>:</td><td> {{ $sender_email }}</td></tr>
      <tr><td valign="top">Subjek</td></t><td>:</td><td> {{ $sender_subject }}</td></tr>
      <tr><td valign="top">Isi Pesan</td></t><td valign="top">:</td><td>{{ $sender_message }}</td></tr>
      </div>
		</table>
	</td></tr>
	<!--content 2 END-->
	<!--footer -->
	<tr><td class="iage_footer" align="center" bgcolor="#ffffff">
		<!-- padding -->
    <div style="height: 80px; line-height: 80px; font-size: 10px;"></div>

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td align="center">
				<font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
				<span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
					2018 © PT. Dian Cempaka. ALL Rights Reserved.
				</span></font>
			</td></tr>
		</table>
	</td></tr>
	<!--footer END-->
</table>
<!--[if gte mso 10]>
</td></tr>
</table>
<![endif]-->

</td></tr>
</table>

</div>
