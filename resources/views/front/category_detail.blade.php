@extends('layouts.front.template')
@section('front_title', $cat_name->category_name)
@section('content')
<section id="category" class="about-area section-one">
	<div class="container">
		<div class="panel_title">
			<div>
				<h4><a href="#">{!! $cat_name->category_name !!}</a></h4>
			</div>
		</div>

		<div class="row">
			@foreach ($read as $r)
			<div class="col col_12_of_12">
				<div class="layout_post_2 clearfix">
					<div class="item_thumb">
						<div class="thumb_icon">
							<a href="{{ url('read', $r->news_slug) }}" style="background-color: #446CB3"><i class="fa fa-copy"></i></a>
						</div>
						<div class="thumb_hover">
							<a href="{{ url('read', $r->news_slug) }}"><img src="{{ url('storage/upload/news', $r->picture) }}" alt="{!! $r->title !!}"></a>
						</div>
						<div class="thumb_meta">
							<span class="category" style="background-color: #446CB3"><a href="#">{!! $r->category_name !!}</a></span>
						</div>
					</div>
					<div class="item_content">
						<h4><a href="{{ url('read', $r->news_slug) }}">{!! $r->title !!}</a></h4>
						{!! str_limit($r->content, 300) !!} [...]
						<div class="item_meta clearfix">
							<span class="meta_date">{{ $tglJamIndo($r->news_created_at) }} </span>
							<span class="meta_likes"><a href="#">{!! $r->news_read !!}</a></span>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>
@endsection
