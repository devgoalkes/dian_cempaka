@extends('layouts.front.template')
@section('front_title', 'Home')
@section('metatitle', "Home")
@section('metadescription', "The pioneer in the Copper Tube Industry.")
@section('content')
@if($slides->count() > 0)
  <div class="main-slider style-two default-banner">
 		<div class="tp-banner-container">
        <div class="tp-banner" >
          <section class="main-slideshow flexslider mb30">
            <ul id="slider" class="slides">
              @foreach($slides as $key => $slide)
              <li class="item"> <a href="#"><img src="{{ asset('storage/uploads/banner/'. $slide->picture) }}" data-url="#" class="slide-img" alt="Lorum Ispum" draggable="false" /></a>
              </li>
              @endforeach
            </ul>
          </section>
        </div>
  	</div>
  </div>
  @endif

  <div class="section-full p-tb50">
      <div class="container">
          <div class="row">
              <div class="col-md-5 col-sm-5">
                  <div class="about-com-pic">
                      <img src="{{ asset('front/images/dian_cempaka1.jpg') }}" alt="" class="img-responsive"/>
                      <br>
                      <iframe src="https://www.youtube.com/embed/jQtN9miqO9A?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                  </div>
              </div>
              <div class="col-md-7 col-sm-7">
                  <div class="section-head text-left">
                      <h2 class="text-uppercase">Latar Belakang Perusahaan</h2>
                      <div class="wt-separator-outer">
                          <div class="wt-separator style-square">
                              <span class="separator-left bg-primary"></span>
                              <span class="separator-right bg-primary"></span>
                          </div>
                      </div>
                      <p>PT Dian Cempaka adalah pelopor dalam Industri Pipa Tembaga dan telah menjadi agen tunggalpipa tembaga merek Kembla (Australia) di Indonesia selama lebih dari 25 tahun.</p>
                      <p>PT Dian Cempaka telah beroperasi di Jakarta sejak 1984 (beroperasi dengan nama PT. Prima Bintang Indorejeki sampai 1994). Selama dua dekade terakhir kami telah mengumpulkan kepercayaan pasar yang kuat dan sehat melalui prinsip sederhana yang kami ikuti: Dapatkan Kepercayaan dengan kualitas produk yang terpercaya.</p>
                      <p>Kami telah membangun reputasi yang kuat untuk menyediakan produk di banyak proyek mulai dari apartemen, rumah sakit, hotel, dan banyak proyek lainnya dengan kualitas dan harga yang kompetitif.</p>
                      <p>Mengetahui banyak vendor memberi Anda banyak pilihan, tetapi menyia-nyiakan lebih banyak waktu dalam membuat perbandingan. PT. Dian Cempaka akan menjadi pilihan terbaik untuk perusahaan Anda, karena karena kami adalah distributor tunggal pipa Kembla di Indonesia, kami selalu menjamin ketersediaan stock dan kualitas, dengan harga yang kompetitif.</p>
                  </div>
              </div>
          </div>
          <div class="about-types row p-t40">
              <div class="col-md-3 col-sm-6 p-tb20">
                  <div class="wt-icon-box-wraper left p-l20  bdr-1 bdr-gray-light">
                      <div class="icon-sm text-primary">
                          <a href="#" class="icon-cell p-t5 center-block"><i class="fa fa-building" aria-hidden="true"></i></a>
                      </div>
                      <div class="icon-content">
                          <h5 class="wt-tilte text-uppercase m-b0">QUALITY ASSURANCE</h5>
                          <p>Pengendalian mutu sangat ketat dan harus memenuhi standar yang ditetapkan baik nasional maupun internasional membuat PT. Dian Cempaka menjadi perusahaan pipa berkualitas utama.</p>
                      </div>
                  </div>
              </div>
              <div class="col-md-3 col-sm-6 p-tb20 ">
                  <div class="wt-icon-box-wraper left  p-l20 bdr-1 bdr-gray-light">
                      <div class="icon-sm text-primary">
                          <a href="#" class="icon-cell p-t5 center-block"><i class="fa fa-paint-brush" aria-hidden="true"></i></a>
                      </div>
                      <div class="icon-content">
                          <h5 class="wt-tilte text-uppercase m-b0">KEPUASAN PELANGGAN</h5>
                          <p>Kualitas produk yang dapat diandalkan, printing di badan pipa yang menyatakan ketebalan ketepatan ukuran dan ketebalan pipa tersebut yang memudahkan proses instalasi maupun penyambungan serta berbagai nilai tambah lain yang langsung bisa dirasakan oleh pelanggan kami selama ini.</p>
                      </div>
                  </div>
              </div>
              <div class="col-md-3 col-sm-6 p-tb20 ">
                  <div class="wt-icon-box-wraper left  p-l20 bdr-1 bdr-gray-light">
                      <div class="icon-sm text-primary">
                          <a href="#" class="icon-cell p-t5 center-block"><i class="fa fa-gavel" aria-hidden="true"></i></a>
                      </div>
                      <div class="icon-content">
                          <h5 class="wt-tilte text-uppercase m-b0 ">JARINGAN DISTRIBUSI</h5>
                          <p>Dukungan tenaga marketing yang handal dan keagenan tunggal yang membuat harga dan pemasarannya menjadi jelas dan pertanggungan jawabnya apabila ada kebutuhan hingga kepelosok Indonesia .</p>
                      </div>
                  </div>
              </div>
              <div class="col-md-3 col-sm-6 p-tb20 ">
                  <div class="wt-icon-box-wraper left p-l20 bdr-1 bdr-gray-light">
                      <div class="icon-sm text-primary">
                          <a href="#" class="icon-cell p-t5 center-block"><i class="fa fa-chart-line" aria-hidden="true"></i></a>
                      </div>
                      <div class="icon-content">
                          <h5 class="wt-tilte text-uppercase m-b0">MITRA</h5>
                          <p>Kami selalu menjaga hubungan  kemitraan yang telah lama dirintis dan memiliki hubungan yang baik di lokal maupun internasional</p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <!-- WHY CHOOSE US SECTION START  -->
  <div class="section-full bg-gray p-t50 p-b20" style="background-image:url(images/background/why-choose-pic.png); background-repeat:no-repeat; background-position:right bottom;">
  	<div class="container">
          <!-- TITLE START-->
          <div class="section-head text-center">
              <h2 class="text-uppercase">Kenapa Memilih Kami</h2>
              <div class="wt-separator-outer">
                  <div class="wt-separator style-square">
                      <span class="separator-left bg-primary"></span>
                      <span class="separator-right bg-primary"></span>
                  </div>
              </div>
              <p>PT Dian Cempaka adalah pelopor dalam Industri Pipa Tembaga dan telah menjadi agen tunggal tembaga Kembla (Australia) di Indonesia.</p>
          </div>
          <!-- TITLE END-->
          <div class="section-content">
            <div class="row">

                <!-- COLUMNS 1 -->
                <div class="col-md-4 col-sm-6 p-t15">
                    <div class="wt-icon-box-wraper  p-a20 left bg-white">
                        <div class="icon-sm text-primary m-b20">
                            <a href="#" class="icon-cell"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a>
                        </div>
                        <div class="icon-content">
                            <h5 class="wt-tilte text-uppercase">Kualitas Terpercaya</h5>
                            <div class="wt-separator bg-primary"></div>
                            <p>Kami memiliki kualitas pipa yang berstandar international dan memiliki semua sertifikasi standart-standart yang dibutuhkan.</p>
                        </div>
                    </div>
                </div>
                <!-- COLUMNS 2 -->
                <div class="col-md-4 col-sm-6 p-tb15">
                    <div class="wt-icon-box-wraper  p-a20 left bg-white">
                        <div class="icon-sm text-primary m-b20">
                            <a href="#" class="icon-cell"><i class="fa fa-dollar-sign" aria-hidden="true"></i></a>
                        </div>
                        <div class="icon-content ">
                            <h5 class="wt-tilte text-uppercase">Harga Kompetitif</h5>
                            <div class="wt-separator bg-primary"></div>
                            <p>Kami memberikan Harga terbaik untuk Produk Pipa berkualitas yang berstandar sesuai dengan Kebutuhan pada proyek pemipaan di seluruh Indonesia.</p>
                        </div>
                    </div>
                </div>
                <!-- COLUMNS 3 -->
                <div class="col-md-4 col-sm-6 p-tb15">
                    <div class="wt-icon-box-wraper  p-a20 left bg-white">
                        <div class="icon-sm text-primary m-b20">
                            <a href="#" class="icon-cell"><i class="fa fa-users" aria-hidden="true"></i></a>
                        </div>
                        <div class="icon-content">
                            <h5 class="wt-tilte text-uppercase">Support</h5>
                            <div class="wt-separator bg-primary"></div>
                            <p>Kami memilki support team yang berpengalaman diperusahaan kami yang dapat membantu customer kami dalam setiap kebutuhan dan permasalahan yang dihadapi.</p>
                        </div>
                    </div>
                </div>
                <!-- COLUMNS 4 -->
                <div class="col-md-4 col-sm-6 p-tb15">
                    <div class="wt-icon-box-wraper  p-a20 left bg-white">
                        <div class="icon-sm text-primary m-b20">
                            <a href="#" class="icon-cell"><i class="fa fa-map-marker-alt" aria-hidden="true"></i></a>
                        </div>
                        <div class="icon-content">
                            <h5 class="wt-tilte text-uppercase">Lokasi</h5>
                            <div class="wt-separator bg-primary"></div>
                            <p>Kami berlokasikan dijakarta pusat yang sudah berdiri dan berkecimpung diperusahaan pipa sejak 1984 sehingga memudahkan kami untuk melakukan pendistribusian produk kami keseluruh Indonesia.</p>
                        </div>
                    </div>
                </div>
                <!-- COLUMNS 5 -->
                <div class="col-md-4 col-sm-6 p-tb15">
                  <div class="wt-icon-box-wraper  p-a20 left bg-white">
                      <div class="icon-sm text-primary m-b20">
                          <a href="#" class="icon-cell"><i class="fa fa-check-square" aria-hidden="true"></i></a>
                      </div>
                      <div class="icon-content">
                          <h5 class="wt-tilte text-uppercase">Terlengkap dan Terpercaya</h5>
                          <div class="wt-separator bg-primary"></div>
                          <p>Kami menyediakan beragam kebutuhan material pipa dan fitting sesuai kebutuhan dengan dukungan resmi dari pabrik berstandar internasional.</p>
                      </div>
                  </div>
                </div>


                <!-- COLUMNS 6 -->
                <div class="col-md-4 col-sm-6 p-tb15">
                    <div class="wt-icon-box-wraper  p-a20 left bg-white">
                        <div class="icon-sm text-primary m-b20">
                            <a href="#" class="icon-cell"><i class="fa fa-chart-line" aria-hidden="true"></i></a>
                        </div>
                        <div class="icon-content">
                            <h5 class="wt-tilte text-uppercase">Skala Besar</h5>
                            <div class="wt-separator bg-primary"></div>
                            <p>Kami melayani setiap kebutuhan material multi sekala, bahkan dalam jumlah besar untuk proyek di seluruh Indonesia.</p>
                        </div>
                    </div>
                </div>

            </div>
          </div>

      </div>
  </div>
  <!-- WHY CHOOSE US SECTION END -->


  @if($clients->count() > 0)
  <!-- OUR CLIENT SLIDER START -->
    <div class="section-full p-t20 p-b20">
        <div class="container">

        	<!-- TITLE START -->
            <div class="section-head text-center">
                <h2 class="text-uppercase">Klien Kami</h2>
                <div class="wt-separator-outer">
                    <div class="wt-separator style-square">
                        <span class="separator-left bg-primary"></span>
                        <span class="separator-right bg-primary"></span>
                    </div>
                </div>
            </div>
            <!-- TITLE END -->

            <!-- IMAGE CAROUSEL START -->
            <div class="section-content">
                <div class="owl-carousel client-logo-carousel">

                	@foreach($clients as $client)
                    <div class="item">
                        <div class="ow-client-logo">
                            <div class="client-logo wt-img-effect on-color">
                                <a target="_blank" href="{{ $client->url }}"><img src="{{ asset('storage/uploads/client/'. $client->picture) }}" alt="{{ $client->name }}"></a>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
            <!-- IMAGE CAROUSEL START -->
        </div>

    </div>
    <!-- OUR CLIENT SLIDER END -->
    @endif

  <div class="call-to-action-wrap bg-primary" style="background-image:url(images/background/bg-4.png); background-repeat:repeat;">
      <div class="container">
          <div class="row">
              <div class="col-md-8 col-sm-8">
                  <div class="call-to-action-left p-tb20 p-r50">
                      <h4 class="text-uppercase m-b10">Kami siap menjadi mitra bisnis anda.</h4>
                      <p>Kami menyediakan kebutuhan anda akan pipa tembaga terbaik dan sesuai standar proyek anda.</p>
                  </div>
              </div>

              <div class="col-md-3">
                  <div class="call-to-action-right p-tb30">
                      <a href="{{ route('kontak_kami') }}" class="site-button-secondry text-uppercase"  style="font-weight:600;">
                          Hubungi Kami <i class="fa fa-angle-double-right"></i>
                      </a>
                  </div>
              </div>
          </div>
       </div>
  </div>
@endsection
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('front/css/flexslider.css') }}" />
@endsection
@push('js')
<script src="{{ asset('front/js/flexslider.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.flexslider').flexslider({
    animation:  "fade",
    slideshowSpeed: 4000,
    slideshow: true,
    animationSpeed :500,
    touch: true,
    pauseOnHover: false,
    controlNav:true
    });
  });
</script>

@endpush
