<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Error 404 | {{ config('app.name', 'Blog GoAlkes') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('admin/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

	<!-- Font Awesome -->
	<link href="{{ asset('admin/css/font-awesome.min.css') }}" rel="stylesheet">

	<!-- Jaiswal -->
	<link href="{{ asset('admin/css/app.min.css') }}" rel="stylesheet">
	<link href="{{ asset('admin/css/app-skin.css') }}" rel="stylesheet">

  </head>

  <body>
	<div id="wrapper">
		<div class="padding-md" style="margin-top:50px;">
			<div class="row">
				<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center">
					<div class="h5">Oops, This Page Could Not Be Found!</div>
					<h1 class="m-top-none error-heading">404</h1>

					<h4>Search Our Website</h4>
					<div>Can't find what you need?</div>
					<div class="m-bottom-md">Try searching for the page here</div>
					<div class="input-group m-bottom-md">
						<input type="text" class="form-control input-sm" placeholder="search here...">
						<span class="input-group-btn">
							<button class="btn btn-default btn-sm" type="button"><i class="fa fa-search"></i></button>
						</span>
					</div><!-- /input-group -->
					<a class="btn btn-success m-bottom-sm" href="{{ url('/') }}"><i class="fa fa-home"></i> Back to Home</a>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.padding-md -->
	</div><!-- /wrapper -->

	<!-- Jquery -->
	<script src="{{ asset('admin/js/jquery-1.10.2.min.js') }}"></script>

	<!-- Bootstrap -->
    <script src="{{ asset('admin/bootstrap/js/bootstrap.min.js') }}"></script>

	<!-- Modernizr -->
	<script src="{{ asset('admin/js/modernizr.min.js') }}"></script>

	<!-- Pace -->
	<script src="{{ asset('admin/js/pace.min.js') }}"></script>

	<!-- Popup Overlay -->
	<script src="{{ asset('admin/js/jquery.popupoverlay.min.js') }}"></script>

	<!-- Slimscroll -->
	<script src="{{ asset('admin/js/jquery.slimscroll.min.js') }}"></script>

	<!-- Cookie -->
	<script src="{{ asset('admin/js/jquery.cook.min.js') }}"></script>

	<!-- Jaiswal -->
	<script src="{{ asset('admin/js/app/app.js') }}"></script>

  </body>
</html>
