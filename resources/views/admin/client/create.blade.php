@extends('layouts.admin.template')
@section('title', 'Add Client')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li><a onclick="location.href='{{ route('adm.client.index') }}'">Client</a></li>
		 <li class="active">Add</li>
	</ul>
</div><!-- /breadcrumb-->
@endsection
@section('content')
<div class="panel panel-primary">
	<div class="panel-heading">Add Client</div>
	<div class="panel-body">
		{!! Form::open(['route' => 'adm.client.store', 'methode' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}

			<div class="form-group">
				<label for="name" class="col-sm-2 control-label">Client Name</label>
				<div class="col-sm-10">
					<input type="text" name="name" class="form-control" id="name" placeholder="Client Name" value="{{ old('name') }}">
				</div>
			</div>
			<div class="form-group">
				<label for="url" class="col-sm-2 control-label">Client URL</label>
				<div class="col-sm-10">
					<input type="text" name="url" class="form-control" id="url" placeholder="Client URL" value="{{  old('url') }}">
				</div>
			</div>
			<div class="form-group">
				<label for="picture" class="col-sm-2 control-label">Picture</label>
				<div class="col-sm-5">
					<div class="upload-file">
						<input type="file" name="picture" id="picture" class="picture">
						<label data-title="Select file" for="picture">
							<span data-title="No file selected..."></span>
						</label>
					</div>
					<br>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
					<a onclick="location.href='{{ route('adm.client.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
				</div>
			</div>

		{!! Form::close() !!}
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.gritter.min.js') }}"></script>
<script>
	$('.picture').change(function()	{
		var filename = $(this).val().split('\\').pop();
		$(this).parent().find('span').attr('data-title',filename);
		$(this).parent().find('label').attr('data-title','Change file');
		$(this).parent().find('label').addClass('selected');
	});
</script>

@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <script>
			$.gritter.add({
				title: '<i class="fa fa-warning"></i> Error',
				text: '{{ $error }}',
				sticky: false,
				time: '5000',
				class_name: 'gritter-danger'
			});
        </script>
    @endforeach
@endif

@endsection
