@extends('layouts.admin.template')
@section('title', 'Client')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li class="active">Client</li>
	</ul>
</div>
@endsection
@section('content')
<div class="panel panel-primary table-responsive">
	<div class="panel-heading">
		&nbsp;
	</div>
	<div class="padding-md clearfix">
		@section('button')
		<a onclick="location.href='{{ route('adm.client.create') }}'" class="btn btn-primary btn-small"><i class="fa fa-plus"></i> Add Client</a>
		@endsection
		<table class="table table-condensed table-bordered" id="dataTable">
			<thead>
				<tr>
					<th width="20px">No</th>
					<th>URL</th>
					<th>Name</th>
					<th>Logo</th>
					<th>Created At</th>
					<th width="100px">Action</th>
				</tr>
			</thead>
			<tbody>
				@php $no=0; @endphp
				@forelse($clients as $r)
				@php $no++; @endphp
				<tr>
					<td>{{ $no }}</td>
					<td>{{ $r->name }}</td>
					<td>{{ $r->url }}</td>
					<td>{{ $r->picture }}</td>
					<td>{{ $r->created_at }}</td>
					<td>
						@if(!empty($r->picture))
						<a class="gallery-zoom btn btn-xs btn-info" href="{{ url('storage/uploads/client/'.$r->picture) }}"><i class="fa fa-eye"></i></a>
						@endif
						<a href="{{ route('adm.client.edit', $r->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
						{{ Form::open(['route' => ['adm.client.destroy', $r->id], 'method' => 'DELETE', 'class' => 'delete_form', 'style' => 'display:inline']) }}
						<button class="btn btn-danger btn-xs delete_button"><i class="fa fa-trash"></i></button>
						{{ Form::close() }}
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="6">Belum ada data !</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/jquery.dataTables_themeroller.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/sweetalert.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/colorbox/colorbox.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('admin/js/jquery.colorbox.min.js') }}"></script>

<script>
		$('#dataTable').dataTable( {
			"bJQueryUI": true,
			"bLengthChange": false,
			"sPaginationType": "full_numbers"
		});
		$('.gallery-zoom').colorbox({
			rel:'gallery',
			maxWidth:'90%',
			width:'800px'
		});
    $('button.delete_button').on('click', function(e){
    	e.preventDefault();
    	var self = $(this);
    	swal({
        title: "Alert",
        text: "Apakah anda yakin ingin menghapus data ini ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#cc3f44",
        confirmButtonText: "Hapus",
        cancelButtonText : "Batal",
        closeOnConfirm: false,
        closeOnCancel: false
    	},
    	function(isConfirm) {
	        if(isConfirm){
	        	self.parents(".delete_form").submit();
	        }else{
	        	swal('Cancelled', 'Data anda gagal dihapus !', 'error');
	        }
	    });
    });
</script>
@endsection
