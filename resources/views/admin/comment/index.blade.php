@extends('layouts.admin.template')
@section('title', 'Comment')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li class="active">Comment</li>
	</ul>
</div>
@endsection
@section('content')
<div class="panel panel-primary table-responsive">
	<div class="panel-heading">
		&nbsp;
	</div>
	<div class="padding-md clearfix">
		@section('button')

		@endsection
		<table class="table table-condensed table-bordered" id="dataTable">
			<thead>
				<tr>
					<th>No</th>
					<th>News Title</th>
					<th>Comment Name</th>
					<th>URL</th>
					<th>Created At</th>
					<th>Active</th>
					<th width="80px">Action</th>
				</tr>
			</thead>
			<tbody>
				@php $no=0; @endphp
				@foreach($comment as $r)
				@php $no++; @endphp
				<tr>
					<td>{{ $no }}</td>
					<td>{{ $r->news_title }}</td>
					<td>{{ $r->comment_name }}</td>
					<td>{{ $r->url }}</td>
					<td>{{ $r->created_at }}</td>
          <td>{!! $r->active == 'Y' ? '<i class=\'fa fa-check\'></i>' : '<i class=\'fa fa-times\'></i>' !!}</td>
					<td>
						<button type="button" class="btn btn-info btn-xs btn-detail" data-toggle="modal" data-target=".bs-example-modal-md" id="{{ route('adm.comment.show', $r->id) }}"><i class="fa fa-eye"></i></button>
						{!! Form::open(['route' => ['adm.comment.destroy', $r->id], 'method' => 'DELETE', 'class' => 'delete_form', 'style' => 'display:inline']) !!}
						<button class="btn btn-danger btn-xs delete_button"><i class="fa fa-trash"></i></button>
						{!! Form::close() !!}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-body">
        <div id="modalcontent"></div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/jquery.dataTables_themeroller.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>

<script>
	$(function(){
			$('.btn-detail').on('click', function(){
					var id = $(this).attr('id');
					$('#modalcontent').load(id);
			});
      $('button.delete_button').on('click', function(e){
        e.preventDefault();
        var self = $(this);
        swal({
          title: "Alert",
          text: "Apakah anda yakin ingin menghapus data ini ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#cc3f44",
          confirmButtonText: "Hapus",
          cancelButtonText : "Batal",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm) {
            if(isConfirm){
              self.parents(".delete_form").submit();
              swal('Sukses', 'Data anda berhasil dihapus !', 'success');
            }else{
              swal('Cancelled', 'Data anda gagal dihapus !', 'error');
            }
        });
      });
			$('#dataTable').dataTable( {
				"bJQueryUI": true,
				"sPaginationType": "full_numbers"
			});
	});
</script>
@endsection
