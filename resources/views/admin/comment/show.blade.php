<div class="panel blog-container">
	<div class="panel-body">
		<h4>{{ $comment->title }}</h4>
		<small class="text-muted">Oleh <a href="#"><strong> {{ $comment->comment_name }}</strong></a> | Tanggal {{ tglIndo($comment->created_at) }} | <a href="{{ $comment->comment_url }}" target="_blank">{{ $comment->url }}</a></small>
		<div class="seperator"></div>
		<div class="seperator"></div>
		{!! $comment->content !!}
	</div>
</div><!-- /panel -->
