@extends('layouts.admin.template')
@section('title', 'Edit Profil')

@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
     <li><a onclick="location.href='{{ route('adm.user.index') }}'"> User</a></li>
     <li><a onclick="location.href='{{ url('adm.profil') }}'"> Profil</a></li>
		 <li class="active">Edit</li>
	</ul>
</div>
@endsection

@section('content')

<ul class="tab-bar grey-tab">
	<li>
		<a href="{{ route('adm.profil') }}">
			<span class="block text-center"><i class="fa fa-home fa-2x"></i></span>
			Overview
		</a>
	</li>
	<li class="active">
		<a href="">
			<span class="block text-center"><i class="fa fa-edit fa-2x"></i></span>
			Edit Profil
		</a>
	</li>
</ul>

<div class="padding-md">
	<div class="row">
		<div class="col-md-3 col-sm-3">
			<div class="row">
				<div class="col-xs-6 col-sm-12 col-md-6 text-center">
					<a href="#">
						@if(empty($cek->picture))
						<img src="{{ asset('admin/img/unknown.png') }}" alt="User Avatar" class="img-thumbnail">
						@else
						<img src="{{ asset('admin/img/'.$cek->picture) }}" alt="User Avatar" class="img-thumbnail">
						@endif
					</a>
					<div class="seperator"></div>
					<div class="seperator"></div>
				</div>
				<div class="col-xs-6 col-sm-12 col-md-6">
					<strong class="font-14">{{ $users->name }}</strong>
					<small class="block text-muted">{{ $users->email }}</small>
					<div class="seperator"></div>
					<div class="seperator"></div>
					<a href="#" class="social-connect tooltip-test facebook-hover pull-left m-right-xs" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
					<a href="#" class="social-connect tooltip-test twitter-hover pull-left m-right-xs" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
					<a href="#" class="social-connect tooltip-test google-plus-hover pull-left" data-toggle="tooltip" data-original-title="Google Plus"><i class="fa fa-google-plus"></i></a>
					<div class="seperator"></div>
					<div class="seperator"></div>
				</div>
			</div>
		</div>
    <div class="col-md-9 col-sm-9">
			<div class="tab-content">
				<div class="tab-pane fade in active" id="overview">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default fadeInDown animation-delay2">
								<div class="panel-heading">
									Tentang Saya
								</div>
								<div class="panel-body">
									{!! Form::open(array('url' => 'adm/profil/update/'.$cek->id, 'method' => 'PUT', 'class' => 'form-horizontal form-border')) !!}
										<div class="panel-body">
											<div class="form-group">
												<label class="control-label col-md-2">Nama Lengkap</label>
												<div class="col-md-10">
													<input type="text" class="form-control input-sm" name="full_name" autocomplete="off" value="{{ $cek->full_name }}">
                          <input type="hidden" name="user_id" value="{{ $cek->user_id }}">
												</div>
											</div>
									    <div class="form-group">
												<label class="control-label col-md-2">Jenis Kelamin</label>
												<div class="col-md-10">
                          @if($cek->sex == 'L')
													<label class="label-radio inline">
														<input type="radio" name="sex" value="L" checked>
														<span class="custom-radio"></span>
														Male
													</label>
													<label class="label-radio inline">
														<input type="radio" name="sex" value="P">
														<span class="custom-radio"></span>
														Female
													</label>
                          @else
                          <label class="label-radio inline">
														<input type="radio" name="sex" value="L">
														<span class="custom-radio"></span>
														Male
													</label>
													<label class="label-radio inline">
														<input type="radio" name="sex" value="P" checked>
														<span class="custom-radio"></span>
														Female
													</label>
                          @endif
												</div>
											</div>
									    <div class="form-group">
									      <label class="control-label col-md-2">Alamat</label>
									      <div class="col-md-10">
									        <textarea class="form-control" rows="3" name="address">{{ $cek->address }}</textarea>
									      </div>
									    </div>
											<div class="form-group">
												<label class="control-label col-md-2">Telepon</label>
												<div class="col-md-10">
													<input type="text" class="form-control input-sm" name="phone_number" autocomplete="off" value="{{ $cek->phone_number }}">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-2">Twitter</label>
												<div class="col-md-10">
													<input type="text" class="form-control input-sm" name="twitter" value="{{ $cek->twitter }}">
												</div>
											</div>
									    <div class="form-group">
												<label class="control-label col-md-2">Google</label>
												<div class="col-md-10">
													<input type="text" class="form-control input-sm" name="google" value="{{ $cek->google }}">
												</div>
											</div>
									    <div class="form-group">
									      <label class="control-label col-md-2">Youtube</label>
									      <div class="col-md-10">
									        <input type="text" class="form-control input-sm" name="youtube" value="{{ $cek->youtube }}">
									      </div>
									    </div>
									    <div class="form-group">
									      <label class="control-label col-md-2">Path</label>
									      <div class="col-md-10">
									        <input type="text" class="form-control input-sm" name="path" value="{{ $cek->path }}">
									      </div>
									    </div>
									    <div class="form-group">
									      <label class="control-label col-md-2">Deskripsi</label>
									      <div class="col-md-10">
									        <textarea class="form-control" rows="3" name="description">{{ $cek->description }}</textarea>
									      </div>
									    </div>
										</div>
										<div class="panel-footer">
											<div class="text-right">
												<button class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button>
												<button class="btn btn-sm btn-danger" type="reset" onclick="location.href='{{ url('adm/profil') }}'"><i class="fa fa-times"></i> Tutup</button>
											</div>
										</div>
									{!! Form::close() !!}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  </div>
</div>
@endsection

@section('js')
<script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>
@endsection
