@extends('layouts.admin.template')
@section('title', 'Edit News')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li><a onclick="location.href='{{ url('adm.news.index') }}'">News</a></li>
		 <li class="active">Edit</li>
	</ul>
</div><!-- /breadcrumb-->
@endsection
@section('content')

<section class="panel panel-primary">
	<header class="panel-heading">Edit News Data</header>
	<div class="panel-body">

	{!! Form::open(['route' => ['adm.news.update', $news->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}
		<div class="form-group">
			<label for="title" class="col-sm-2 control-label">News Title</label>
			<div class="col-sm-10">
				<input type="text" name="title" class="form-control" id="title" placeholder="News Title" value="{{ $news->title }}">
				<input type="hidden" name="user_id" value="{{ $news->user_id }}">
				@if ($errors->has('title')) <span class="help-block alert alert-danger">{{ $errors->first('title') }}</span> @endif
			</div>
		</div>

		<div class="form-group">
			<label for="content" class="col-sm-2 control-label">Content</label>
			<div class="col-sm-10">
				<textarea id="content" name="content" placeholder="Enter your text ..." class="form-control" rows="6">
					{{ $news->content }}
				</textarea>
				@if ($errors->has('content')) <span class="help-block alert alert-danger">{{ $errors->first('content') }}</span> @endif
			</div>
		</div>
		<div class="form-group">
			<label for="category_name" class="col-sm-2 control-label">Category Name</label>
			<div class="col-sm-10">
				<select class="form-control" name="category_name">
					<option value="0" selected>No Category</option>
					@if(!$categorys->isEmpty())

						@foreach($categorys as $r)
							@if($news->category_id == $r->id)
								<option value="{{ $r->id }}" selected>{{ $r->category_name }}</option>
							@else
								<option value="{{ $r->id }}">{{ $r->category_name }}</option>
							@endif
						@endforeach
					@else
						<option value="">No Category</option>
					@endif
				</select>
				@if ($errors->has('category_name')) <span class="help-block alert alert-danger">{{ $errors->first('category_name') }}</span> @endif
			</div>
		</div>
		<div class="form-group">
			<label for="picture" class="col-sm-2 control-label">Picture</label>
			<div class="col-sm-5">
				<div class="upload-file">
					<input type="file" name="picture" id="picture" class="picture">
					<label data-title="Select file" for="picture">
						<span data-title="No file selected..."></span>
					</label>
				</div>
				<input type="hidden" name="picture_old" value="{{ $news->picture }}">
				<br>
				@if ($errors->has('picture')) <span class="help-block alert alert-danger">{{ $errors->first('picture') }}</span> @endif
			</div>
		</div>
		<div class="form-group">
			<label for="active" class="col-sm-2 control-label">Status</label>
			<div class="col-lg-10">
				@if ($news->active == 'Y')
					<label class="label-radio inline">
						<input type="radio" name="active" value="Y" checked>
						<span class="custom-radio"></span>
						Aktive
					</label>
					<label class="label-radio inline">
						<input type="radio" name="active" value="N">
						<span class="custom-radio"></span>
						Non Active
					</label>
		        @else
					<label class="label-radio inline">
						<input type="radio" name="active" value="Y">
						<span class="custom-radio"></span>
						Aktive
					</label>
					<label class="label-radio inline">
						<input type="radio" name="active" value="N" checked>
						<span class="custom-radio"></span>
						Non Active
					</label>
		        @endif

			</div><!-- /.col -->
		</div>
		<div class="form-group">
				<label class="col-lg-2 control-label">Tag</label>
				<div class="col-lg-10">
					<select multiple class="form-control chzn-select" name="tag[]">

						@foreach(explode(',', $news->tag) as $info)
						@foreach($tags as $r)
								@if($r->tag_slug = $info)
									<option value="{{ $r->tag_slug }}" selected>{{ $r->tag_slug }}</option>
								@else
									<option value="{{ $r->tag_slug }}">{{ $r->tag_slug }}</option>
								@endif
						@endforeach
						@endforeach
					</select>
				</div><!-- /.col -->
			</div><!-- /form-group -->
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a onclick="location.href='{{ route('adm.news.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</section>
@endsection


@section('style')
<link href="{{ asset('admin/css/chosen/chosen.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/chosen.jquery.min.js') }}"></script>
<script src="{{ asset('ckeditor/ckeditor/ckeditor.js') }}"></script>
<!-- Gritter -->
<script src="{{ asset('admin/js/jquery.gritter.min.js') }}"></script>
<script>
	$(".chzn-select").chosen();
	CKEDITOR.replace( 'content' );
	@if(count($errors) > 0)
	@foreach($errors->all() as $error)
	$.gritter.add({
		title: '<i class="fa fa-warning"></i> Error',
		text: '{{ $error }}',
		sticky: false,
		time: '5000',
		class_name: 'gritter-danger'
	});
	@endforeach
	@endif
</script>
@endsection
