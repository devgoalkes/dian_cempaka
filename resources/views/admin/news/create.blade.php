@extends('layouts.admin.template')
@section('title', 'Add News')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li><a onclick="location.href='{{ route('adm.news.index') }}'">News</a></li>
		 <li class="active">Add</li>
	</ul>
</div><!-- /breadcrumb-->
@endsection
@section('content')

<div class="panel panel-primary">
	<div class="panel-heading">Add News</div>
	<div class="panel-body">
		{!! Form::open(['route' => 'adm.news.store', 'methode' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}

			<div class="form-group">
				<label for="title" class="col-sm-2 control-label">News Title</label>
				<div class="col-sm-10">
					<input type="text" name="title" class="form-control" id="title" placeholder="News Title" autocomplete="off">
				</div>
			</div>
			<div class="form-group">
				<label for="content" class="col-sm-2 control-label">Content</label>
				<div class="col-sm-10">
					<textarea id="content" name="content" placeholder="Enter your text ..." class="form-control" rows="6"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label for="category_name" class="col-sm-2 control-label">Category Name</label>
				<div class="col-sm-10">
					<select class="form-control" name="category_name">
						@if(!$categorys->isEmpty())
							<option value="">No Category</option>
							@foreach($categorys as $r)
								<option value="{!! $r->id !!}">{!! $r->category_name !!}</option>
							@endforeach
						@else
							<option value="">No Category</option>
						@endif
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="picture" class="col-sm-2 control-label">Picture</label>
				<div class="col-sm-5">
					<div class="upload-file">
						<input type="file" name="picture" id="picture" class="picture">
						<label data-title="Select file" for="picture">
							<span data-title="No file selected..."></span>
						</label>
					</div>
					<br>
				</div>
			</div>
			<div class="form-group">
				<label for="active" class="col-sm-2 control-label">Status</label>
				<div class="col-lg-10">
					<label class="label-radio inline">
						<input type="radio" name="active" value="Y">
						<span class="custom-radio"></span>
						Active
					</label>
					<label class="label-radio inline">
						<input type="radio" name="active" value="N">
						<span class="custom-radio"></span>
						Non Active
					</label>
				</div>
			</div>
			<div class="form-group">
					<label class="col-lg-2 control-label">Tag</label>
					<div class="col-lg-10">
						<select multiple class="form-control chzn-select" name="tag[]">
							@if(!$tags->isEmpty())
								@foreach($tags as $r)
								<option value="{{ $r->tag_slug }}">{{ $r->tag_name }}</option>
								@endforeach
							@endif
						</select>
					</div><!-- /.col -->
				</div><!-- /form-group -->
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
					<a onclick="location.href='{{ route('adm.news.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
				</div>
			</div>

		{!! Form::close() !!}
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/chosen/chosen.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/chosen.jquery.min.js') }}"></script>
<script src="{{ asset('ckeditor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('admin/js/jquery.gritter.min.js') }}"></script>
<script>
	$(".chzn-select").chosen();
	CKEDITOR.replace( 'content');
	$('.picture').change(function()	{
			var filename = $(this).val().split('\\').pop();
			$(this).parent().find('span').attr('data-title',filename);
			$(this).parent().find('label').attr('data-title','Change file');
			$(this).parent().find('label').addClass('selected');
	});
	@if(count($errors) > 0)
	@foreach($errors->all() as $error)
	$.gritter.add({
		title: '<i class="fa fa-warning"></i> Error',
		text: '{{ $error }}',
		sticky: false,
		time: '5000',
		class_name: 'gritter-danger'
	});
	@endforeach
	@endif
  </script>
@endsection
