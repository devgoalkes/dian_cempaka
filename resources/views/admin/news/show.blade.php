<div class="panel blog-container">
	<div class="panel-body">
		<h4>{{ $news->title }}</h4>
		<small class="text-muted">Oleh : <strong> {{ $news->user->name }}</strong> | Tanggal : {{ tglIndo($news->created_at) }}  | {{ $news->comments->count() }} Komentar</small>
		<div class="seperator"></div>
		<div class="seperator"></div>

		<img width="200px" class="img-thumbnail" style="float:left; margin-right:10px" src="{{ url('/storage/uploads/news/'.$news->picture) }}" alt="{{ $news->title }}"/>
		{!! $news->content !!}
	</div>
</div><!-- /panel -->
