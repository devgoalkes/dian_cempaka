@extends('layouts.admin.template')
@section('title', 'Schedule')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li class="active">Schedule</li>
	</ul>
</div><!-- /breadcrumb-->
@endsection
@section('content')
<div class="panel panel-primary table-responsive">
	<div class="panel-heading">
		&nbsp;
	</div>
	<div class="padding-md clearfix">
		@section('button')
		<a onclick="location.href='{{route('agenda.create')}}'" class="btn btn-primary btn-small"><i class="fa fa-plus"></i> Add Agenda</a>
		@endsection
		<table class="table table-condensed table-bordered" id="dataTable">
			<thead>
				<tr>
					<th width="20px">No</th>
					<th>Title</th>
					<th>Sender</th>
					<th>Date Start</th>
					<th>Date End</th>
					<th>Time</th>
					<th>User ID</th>
					<th width="100px">Action</th>
				</tr>
			</thead>
			<tbody>
				@if(!$agendas->isEmpty())
				@php $no=0; @endphp
				@foreach($agendas as $r)
				@php $no++; @endphp
				<tr>
					<td>{!! $no !!}</td>
					<td>{!! $r->title !!}</td>
					<td>{!! $r->sender !!}</td>
					<td>{!! $r->date_start !!}</td>
					<td>{!! $r->date_end !!}</td>
					<td>{!! $r->time !!}</td>
					<td>{!! $r->name !!}</td>
					<td>
						<button type="button" class="btn btn-info btn-xs btn-detail" data-toggle="modal" data-target=".bs-example-modal-md" id="{{ url('adm/agenda/show/'.$r->id) }}"><i class="fa fa-eye"></i></button>
						<a href="agenda/edit/{{ $r->id }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
						{!! Form::open(['route' => ['agenda.destroy', $r->id], 'method' => 'DELETE', 'class' => 'delete_form', 'style' => 'display:inline']) !!}
						<button class="btn btn-danger btn-xs delete_button"><i class="fa fa-trash"></i></button>
						{!! Form::close() !!}
					</td>
				</tr>
				@endforeach
				@endif
			</tbody>
		</table>
	</div><!-- /.padding-md -->
</div>
<div class="modal fade bs-example-modal-md" id="simple-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
			<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <div id="modalcontent"></div>
      </div>
			<div class="modal-footer">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/jquery.dataTables_themeroller.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>

<script>
		$('#dataTable').dataTable( {
			"bJQueryUI": true,
			"bLengthChange": false,
			"sPaginationType": "full_numbers"
		});
		$('.btn-detail').on('click', function(){
				var id = $(this).attr('id');
				$('#modalcontent').load(id);
		});
    $('button.delete_button').on('click', function(e){
    	e.preventDefault();
    	var self = $(this);
    	swal({
        title: "Alert",
        text: "Apakah anda yakin ingin menghapus data ini ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#cc3f44",
        confirmButtonText: "Hapus",
        cancelButtonText : "Batal",
        closeOnConfirm: false,
        closeOnCancel: false
    	},
    	function(isConfirm) {
	        if(isConfirm){
	        	self.parents(".delete_form").submit();
	        	// swal('Sukses', 'Data anda berhasil dihapus !', 'success');
	        }else{
	        	swal('Batal', 'Data anda gagal dihapus !', 'error');
	        }
	    });
    });
</script>
@endsection
