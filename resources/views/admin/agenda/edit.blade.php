@extends('layouts.admin.template')
@section('title', 'Edit Schedule')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li><a onclick="location.href='{{ url('category') }}'">Schedule</a></li>
		 <li class="active">Edit</li>
	</ul>
</div><!-- /breadcrumb-->
@endsection
@section('content')

<section class="panel panel-primary">
	<header class="panel-heading">Edit Agenda Data</header>
	<div class="panel-body">
	{!! Form::open(array('url' => 'adm/agenda/update/'.$agendas->id, 'method' => 'PUT', 'class' => 'form-horizontal')) !!}
		<div class="form-group">
			<label for="title" class="col-sm-2 control-label">Title</label>
			<div class="col-sm-10">
				<input type="text" name="title" class="form-control" id="title" placeholder="Agenda Title" value="{{ $agendas->title }}">
				<input type="hidden" name="user_id" value="{{ $agendas->user_id }}">
			</div>
		</div>
		<div class="form-group">
			<label for="content" class="col-sm-2 control-label">Content</label>
			<div class="col-sm-10">
				<textarea id="content" name="content" placeholder="Enter your text ..." class="form-control" rows="6">{{ $agendas->title }}</textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="place" class="col-sm-2 control-label">Place</label>
			<div class="col-sm-10">
				<input type="text" name="place" class="form-control" id="place" placeholder="Place" value="{{ $agendas->place }}">
			</div>
		</div>
		<div class="form-group">
			<label for="sender" class="col-sm-2 control-label">Sender</label>
			<div class="col-sm-10">
				<input type="text" name="sender" class="form-control" id="sender" placeholder="Sender" value="{{ $agendas->sender }}">
			</div>
		</div>
		<div class="form-group">
			<label for="date_start" class="col-sm-2 control-label">Date Start</label>
			<div class="col-lg-2">
				<input type="text" name="date_start" class="datepicker form-control" id="date_start" placeholder="Date Start" value="{{ $agendas->date_start }}">
			</div>
		</div>
		<div class="form-group">
			<label for="date_end" class="col-sm-2 control-label">Date End</label>
			<div class="col-lg-2">
				<input type="text" name="date_end" class="datepicker form-control" id="date_end" placeholder="Date End" value="{{ $agendas->date_end }}">
			</div>
		</div>
		<div class="form-group">
			<label for="time" class="col-sm-2 control-label">Time</label>
			<div class="col-sm-2">
				<input type="text" name="time" class="timepicker form-control" id="timepicker" placeholder="Time" value="{{ $agendas->time }}">
			</div>
		</div>
		<div class="form-group">
			<label for="status" class="col-sm-2 control-label">Status</label>
			<div class="col-lg-10">
				@if($agendas->active == 'Y')
				<label class="label-radio inline">
					<input type="radio" name="active" value="Y" checked>
					<span class="custom-radio"></span>
					Aktive
				</label>
				<label class="label-radio inline">
					<input type="radio" name="active" value="N">
					<span class="custom-radio"></span>
					Non Active
				</label>
				@else
				<label class="label-radio inline">
					<input type="radio" name="active" value="Y">
					<span class="custom-radio"></span>
					Aktive
				</label>
				<label class="label-radio inline">
					<input type="radio" name="active" value="N" checked>
					<span class="custom-radio"></span>
					Non Active
				</label>
				@endif
			</div><!-- /.col -->
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a onclick="location.href='{{ route('agenda.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
			</div>
		</div>
	{!! Form::close() !!}

	</div>
</section>
@endsection


@section('style')
<link href="{{ asset('admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/datepicker.css') }}" rel="stylesheet"/>
<link href="{{ asset('admin/css/bootstrap-timepicker.css') }}" rel="stylesheet"/>
@endsection

@section('js')
<script src="{{ asset('ckeditor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('admin/js/jquery.gritter.min.js') }}"></script>
<script src="{{ asset('admin/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('admin/js/bootstrap-timepicker.min.js') }}"></script>
<script>
// Wysihtml5
	CKEDITOR.replace( 'content' );
	$('.datepicker').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true
  });
	$('#timepicker').timepicker({ 'scrollDefault': 'now' });
</script>
@if(count($errors) > 0)
	@foreach($errors->all() as $error)
		<script>
			$.gritter.add({
			title: '<i class="fa fa-warning"></i> Error',
			text: '{{ $error }}',
			sticky: false,
			time: '5000',
			class_name: 'gritter-danger'
			});
		</script>
	@endforeach
@endif
@endsection
