<div class="panel blog-container">
	<div class="panel-body">
    <table class="table table-striped">
      <tr><td colspan="3"><h4 class="text-center"><b>{{ $agendas->title }}</b></h4></td></tr>
      <tr><td width="150px">Place</td>  <td width="10px">: </td><td> {{ $agendas->place }}</td></tr>
      <tr><td>Date Start</td>           <td>: </td><td> {{ $agendas->date_start }}</td></tr>
      <tr><td>Date End</td>             <td>: </td><td> {{ $agendas->date_end }}</td></tr>
      <tr><td>Time</td>                 <td>: </td><td> {{ $agendas->time }}</td></tr>
      <tr><td>Sender</td>               <td>: </td><td> {{ $agendas->sender }}</td></tr>
      <tr><td>Content</td>              <td>: </td><td> {!! $agendas->content !!}</td></tr>
    </table>
	</div>
</div><!-- /panel -->
