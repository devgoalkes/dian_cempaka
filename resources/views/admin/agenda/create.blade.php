@extends('layouts.admin.template')
@section('title', 'Add Schedule')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li><a onclick="location.href='{{ url('agenda') }}'">Agenda</a></li>
		 <li class="active">Add</li>
	</ul>
</div>
@endsection
@section('content')
<div class="panel panel-primary">
	<div class="panel-heading">Add New Agenda</div>
	<div class="panel-body">
		@if ($messages = Session::get('messages'))
			{!! $messages !!}
		@endif
		{!! Form::open(array('route' => 'agenda.store', 'methode' => 'POST', 'class' => 'form-horizontal')) !!}
		<div class="form-group">
			<label for="title" class="col-sm-2 control-label">Title</label>
			<div class="col-sm-10">
				<input type="text" name="title" class="form-control" id="title" placeholder="Agenda Title" autocomplete="off">
				<input type="hidden" name="user_id" value="{{ Auth::user()->name }}">
			</div>
		</div>
		<div class="form-group">
			<label for="content" class="col-sm-2 control-label">Content</label>
			<div class="col-sm-10">
				<textarea id="content" name="content" placeholder="Enter your text ..." class="form-control"></textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="place" class="col-sm-2 control-label">Place</label>
			<div class="col-sm-10">
				<input type="text" name="place" class="form-control" id="place" placeholder="Place" autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label for="sender" class="col-sm-2 control-label">Sender</label>
			<div class="col-sm-10">
				<input type="text" name="sender" class="form-control" id="sender" placeholder="Sender" autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label for="date_start" class="col-sm-2 control-label">Date Start</label>
			<div class="col-lg-2">
				<input type="text" name="date_start" class="tanggal form-control" id="date_start" placeholder="Date Start" autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label for="date_end" class="col-sm-2 control-label">Date End</label>
			<div class="col-lg-2">
				<input type="text" name="date_end" class="tanggal form-control" id="date_end" placeholder="Date End" autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label for="time" class="col-sm-2 control-label">Time</label>
			<div class="col-lg-2">
				<input type="text" name="time" class="timepicker form-control" id="time" placeholder="Time" autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label for="active" class="col-sm-2 control-label">Status</label>
			<div class="col-lg-10">
				<label class="label-radio inline">
					<input type="radio" name="active" value="Y">
					<span class="custom-radio"></span>
					Aktive
				</label>
				<label class="label-radio inline">
					<input type="radio" name="active" value="N">
					<span class="custom-radio"></span>
					Non Active
				</label>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a onclick="location.href='{{ route('agenda.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/datepicker.css') }}" rel="stylesheet"/>
<link href="{{ asset('admin/css/bootstrap-timepicker.css') }}" rel="stylesheet"/>
@endsection

@section('js')
<script src="{{ asset('ckeditor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('admin/js/jquery.gritter.min.js') }}"></script>
<script src="{{ asset('admin/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('admin/js/bootstrap-timepicker.min.js') }}"></script>
<script>
	CKEDITOR.replace( 'content' );
	$('#date_start').datepicker({
		format: "yyyy-mm-dd",
		autoclose: true
	});
	$('#date_end').datepicker({
		format: "yyyy-mm-dd",
		autoclose: true
    });
	$('#time').timepicker({ 'scrollDefault': 'now' });
</script>

@if(count($errors) > 0)
@foreach($errors->all() as $error)
<script>
$.gritter.add({
title: '<i class="fa fa-warning"></i> Error',
text: '{{ $error }}',
sticky: false,
time: '5000',
class_name: 'gritter-danger'
});
</script>
@endforeach
@endif
@endsection
