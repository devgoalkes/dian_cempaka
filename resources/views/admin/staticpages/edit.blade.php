@extends('layouts.admin.template')
@section('title', 'Edit Halaman Statis')
@section('content')

<section class="panel panel-primary">
	<header class="panel-heading">Edit Halaman Status</header>
	<div class="panel-body">

	{!! Form::open(['route' => ['adm.staticpages.update', $static->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}

		<div class="form-group">
			<label for="title" class="col-sm-2 control-label">Judul</label>
			<div class="col-sm-10">
				<input type="text" name="title" class="form-control" id="title" placeholder="Judul" value="{{ $static->title }}">
			</div>
		</div>

		<div class="form-group">
			<label for="content" class="col-sm-2 control-label">Konten</label>
			<div class="col-sm-10">
				<textarea id="content" name="content" placeholder="Masukan konten halaman statis anda disini..." class="form-control" rows="6">
					{{ $static->content }}
				</textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="picture" class="col-sm-2 control-label">Gambar</label>
			<div class="col-sm-5">
				<div class="upload-file">
					<input type="file" name="picture" id="picture" class="picture">
					<label data-title="Select file" for="picture">
						<span data-title="No file selected..."></span>
					</label>
				</div>
				<br>
			</div>
		</div>
		<div class="form-group">
			<label for="active" class="col-sm-2 control-label">Status</label>
			<div class="col-lg-10">
				@if ($static->active == 'Y')
					<label class="label-radio inline">
						<input type="radio" name="active" value="Y" checked>
						<span class="custom-radio"></span>
						Aktif
					</label>
					<label class="label-radio inline">
						<input type="radio" name="active" value="N">
						<span class="custom-radio"></span>
						Non Aktif
					</label>
		        @else
					<label class="label-radio inline">
						<input type="radio" name="active" value="Y">
						<span class="custom-radio"></span>
						Aktif
					</label>
					<label class="label-radio inline">
						<input type="radio" name="active" value="N" checked>
						<span class="custom-radio"></span>
						Non Aktif
					</label>
		        @endif

			</div><!-- /.col -->
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a onclick="location.href='{{ route('adm.staticpages.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</section>
@endsection


@section('style')
<link href="{{ asset('ckeditor/content.css') }}" rel="stylesheet"/>
<link href="{{ asset('admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('ckeditor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('admin/js/jquery.gritter.min.js') }}"></script>
<script>
CKEDITOR.replace( 'content' );
</script>
@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <script>
			$.gritter.add({
				title: '<i class="fa fa-warning"></i> Error',
				text: '{{ $error }}',
				sticky: false,
				time: '5000',
				class_name: 'gritter-danger'
			});
        </script>
    @endforeach
@endif
@endsection
