<div class="panel blog-container">
	<div class="panel-body">
		<h4>{{ $static->title }}</h4>
		<small class="text-muted">Oleh <a href="#"></a> | Tanggal {{ tglIndo($static->created_at) }} </small>
		<div class="seperator"></div>
    <div class="input-group">
				<input id="slug" type="text" value="{{ 'pages/'.$static->slug }}" class="form-control">
				<span class="input-group-btn">
					<button class="btn btn-success copy-button" data-clipboard-action="copy" data-clipboard-target="#slug">Copy</button>
				</span>
			</div>
		<div class="seperator"></div>
		@if( $static->picture != '' )
		<img width="200px" class="img-thumbnail" style="float:left; margin-right:10px" src="{{ url('storage/uploads/static_pages/'.$static->picture) }}" alt="{{ $static->title }}">
		@endif
		{!! $static->content !!}

	</div>
</div>
