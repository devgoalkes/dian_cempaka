@extends('layouts.admin.template')
@section('title', 'Website Identity')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li class="active">Identity</li>
	</ul>
</div>
@endsection
@section('content')
<div class="panel panel-primary table-responsive">
	<div class="panel-heading">&nbsp;</div>
	<div class="padding-md clearfix">
		@section('button')
		<a onclick="location.href='{{ route('adm.identity.edit', $r->id) }}'" class="btn btn-primary btn-small"><i class="fa fa-plus"></i> Edit Identity</a>
		@endsection
    <table class="table table-striped">
      <tr><td width="175px">Website Name</td> <td>{{ $r->website_name }}</td></tr>
      <tr><td>Website Address</td>            <td>{{ $r->website_address }}</td></tr>
      <tr><td>Meta Description</td>           <td>{{ $r->meta_description }}</td></tr>
      <tr><td>Meta Keyord</td>                <td>{{ $r->meta_keyword }}</td></tr>
      <tr><td>Favicon</td>                    <td>{{ $r->favicon }}</td></tr>
      <tr><td>Mail Address</td>               <td>{{ $r->mail_address }}</td></tr>
      <tr><td>Address</td>                    <td>{{ $r->address }}</td></tr>
      <tr><td>Phone</td>                      <td>{{ $r->phone }}</td></tr>
    </table>
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/jquery.dataTables_themeroller.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>
@endsection
