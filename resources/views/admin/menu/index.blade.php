@extends('layouts.admin.template')
@section('title', 'Menu')

@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li class="active">Menu</li>
	</ul>
</div>
@endsection

@section('content')
<div class="panel panel-primary table-responsive">
	<div class="panel-heading">
		&nbsp;
	</div>
	<div class="padding-md clearfix">
		@section('button')
		<a onclick="location.href='{{ route('adm.menu.create') }}'" class="btn btn-primary btn-small"><i class="fa fa-plus"></i> Add Menu</a>
		@endsection
		<div class="panel-tab clearfix">
			<ul class="tab-bar">
				<li class="active"><a href="#admin-menu" data-toggle="tab"><i class="fa fa-user"></i> Admin Menu</a></li>
				<li><a href="#front-menu" data-toggle="tab"><i class="fa fa-dashboard"></i> Front Menu</a></li>
			</ul>
		</div>
		<div class="panel-body">
			<div class="tab-content">
				<div class="tab-pane fade in active" id="admin-menu">
					<h4>Menu Admin</h4>
					<table class="table table-condensed table-bordered" id="table-admin-menu">
						<thead>
							<tr>
								<th width="20px">No</th>
								<th>Parent Menu</th>
								<th>Name</th>
								<th>URL</th>
								<th>Active</th>
								<th width="80px">Action</th>
							</tr>
						</thead>
						<tbody>
							@php $no = 0; @endphp
							@foreach($menus as $r)
							@if($r->admin == 'Y')
							@php $no++; @endphp
							<tr>
								<td>{{ $no }}</td>
								@php
								$menu = \App\MenuModel::where('id', $r->id_parent)->first();
								@endphp
								<td>{{ $r->id_parent == 0 ? '-' : $menu->name }}</td>
								<td>{{ $r->name }}</td>
								<td>{{ $r->url }}</td>
								<td>{!! $r->active == 'Y' ? '<i class=\'fa fa-check\'></i>' : '<i class=\'fa fa-times\'></i>' !!}</td>
								<td>
									<a href="{{ route('adm.menu.edit', $r->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
									{!! Form::open(['route' => ['adm.menu.destroy', $r->id], 'method' => 'DELETE', 'class' => 'delete_form', 'style' => 'display:inline']) !!}
									<button class="btn btn-danger btn-xs delete_button"><i class="fa fa-trash"></i></button>
									{!! Form::close() !!}
								</td>
							</tr>
							@endif
							@endforeach
						</tbody>
					</table>
				</div>

				<div class="tab-pane fade in" id="front-menu">
					<table class="table table-condensed table-bordered" id="table-front-menu">
						<h4>Menu Front</h4>
						<thead>
							<tr>
								<th width="20px">No</th>
								<th>Parent Menu</th>
								<th>Name</th>
								<th>URL</th>
								<th>Active</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@php $no = 0; @endphp
							@forelse($menus as $r)
							@if($r->admin == 'N')
							@php $no++; @endphp
							<tr>
								@php $mn = \App\MenuModel::where('id', $r->id_parent)->first(); @endphp
								<td>{{ $no }}</td>
								<td>{{ $r->id_parent == 0 ? '-' : $mn->name }}</td>
								<td>{{ $r->name }}</td>
								<td>{{ $r->url }}</td>
								<td>{!! $r->active == 'Y' ? '<i class=\'fa fa-check\'></i>' : '<i class=\'fa fa-times\'></i>' !!}</td>
								<td>
									<a href="{{ route('adm.menu.edit', $r->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
									{!! Form::open(['route' => ['adm.menu.destroy', $r->id], 'method' => 'DELETE', 'class' => 'delete_form', 'style' => 'display:inline']) !!}
									<button class="btn btn-danger btn-xs delete_button"><i class="fa fa-trash"></i></button>
									{!! Form::close() !!}
								</td>
							</tr>
							@endif
							@empty
							<tr>
								<td colspan="6">Belum ada data !</td>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/jquery.dataTables_themeroller.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>

<script>
$(function	()	{
		$('#table-admin-menu').dataTable( {
			"bJQueryUI": true,
			"bLengthChange": false,
			"sPaginationType": "full_numbers"
		});
		$('#table-front-menu').dataTable( {
			"bJQueryUI": true,
			"bLengthChange": false,
			"sPaginationType": "full_numbers"
		});
    $('button.delete_button').on('click', function(e){
    	e.preventDefault();
    	var self = $(this);
    	swal({
        title: "Alert",
        text: "Apakah anda yakin ingin menghapus data ini ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#cc3f44",
        confirmButtonText: "Hapus",
        cancelButtonText : "Batal",
        closeOnConfirm: false,
        closeOnCancel: false
    	},
    	function(isConfirm) {
	        if(isConfirm){
	        	self.parents(".delete_form").submit();
	        }else{
	        	swal('Cancelled', 'Data anda gagal dihapus !', 'error');
	        }
	    });
    });
	});
</script>
@endsection
