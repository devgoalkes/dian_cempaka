@extends('layouts.admin.template')
@section('title', 'Add Menu')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li><a onclick="location.href='{{ route('adm.menu.index') }}'">Static Pages</a></li>
		 <li class="active">Add</li>
	</ul>
</div><!-- /breadcrumb-->
@endsection
@section('content')
<div class="panel panel-primary">
	<div class="panel-heading">Add New Menu</div>
	<div class="panel-body">
		{!! Form::open(['route' => 'adm.menu.store', 'methode' => 'POST', 'class' => 'form-horizontal']) !!}
		<div class="form-group">
			<label for="name" class="col-sm-2 control-label">Name</label>
			<div class="col-sm-5">
				<input type="text" name="name" class="form-control" id="name" placeholder="Name">
			</div>
		</div>
		<div class="form-group">
			<label for="id_parent" class="col-sm-2 control-label">Parent ID</label>
			<div class="col-sm-5">
				<select class="form-control" name="id_parent">
						<option value="0">No Parent</option>
					@if(!$parent_menus->isEmpty())
						@foreach($parent_menus as $r)
							<option value="{!! $r->id !!}">{!! $r->name !!}</option>
						@endforeach
					@else
						<option value="0">No Parent</option>
					@endif
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="url" class="col-sm-2 control-label">URL</label>
			<div class="col-sm-5">
				<input type="text" name="url" class="form-control" id="url" placeholder="URL">
			</div>
		</div>
		<div class="form-group">
			<label for="icon" class="col-sm-2 control-label">Icon</label>
			<div class="col-sm-5">
					<input type="text" name="icon" class="form-control icon_menu" id="icon_menu" placeholder="Icon">
			</div>
		</div>
		<div class="form-group">
			<label for="admin" class="col-sm-2 control-label">Admin</label>
			<div class="col-lg-10">
				<label class="label-radio inline">
					<input type="radio" name="admin" value="Y">
					<span class="custom-radio"></span>
					Yes
				</label>
				<label class="label-radio inline">
					<input type="radio" name="admin" value="N">
					<span class="custom-radio"></span>
					No
				</label>
			</div>
		</div>
		<div class="form-group">
			<label for="active" class="col-sm-2 control-label">Status</label>
			<div class="col-lg-10">
				<label class="label-radio inline">
					<input type="radio" name="active" value="Y">
					<span class="custom-radio"></span>
					Aktive
				</label>
				<label class="label-radio inline">
					<input type="radio" name="active" value="N">
					<span class="custom-radio"></span>
					Non Active
				</label>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a onclick="location.href='{{ route('adm.menu.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/fontawesome-iconpicker.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.gritter.min.js') }}"></script>
<script src="{{ asset('admin/js/fontawesome-iconpicker.js') }}"></script>
<script>
	$('.icon_menu').iconpicker();
</script>

@if(count($errors) > 0)
@foreach($errors->all() as $error)
<script>
$.gritter.add({
	title: '<i class="fa fa-warning"></i> Error',
	text: '{{ $error }}',
	sticky: false,
	time: '5000',
	class_name: 'gritter-danger'
});
</script>
@endforeach
@endif

@endsection
