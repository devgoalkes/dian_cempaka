@extends('layouts.admin.template')
@section('title', 'Dashboard')

@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li class="active"><i class="fa fa-home"></i> Home</li>
	</ul>
</div>
@endsection

@section('content')
<div class="panel panel-primary table-responsive">
  <div class="panel-heading">&nbsp;</div>
	<div class="well alert-danger">Selamat datang di halaman Administrator Website {{ config('app.name') }}</div>
	<div class="grey-container shortcut-wrapper">
			<a href="{{ url('adm/contact') }}" class="shortcut-link">
				<span class="shortcut-icon">
					<i class="fa fa-envelope-o"></i>
					<span class="shortcut-alert">{{ $jum_contacts }}</span>
				</span>
				<span class="text">Messages</span>
			</a>
			<a href="{{ url('adm/user') }}" class="shortcut-link">
				<span class="shortcut-icon">
					<i class="fa fa-user"></i>
					<span class="shortcut-alert">{{ $jum_user }}</span>
				</span>
				<span class="text">Users</span>
			</a>
			<a href="{{ url('adm/news') }}" class="shortcut-link">
				<span class="shortcut-icon">
					<i class="fa fa-newspaper-o"></i>
					<span class="shortcut-alert">{{ $jum_news }}</span>
				</span>
				<span class="text">News</span>
			</a>
			<a href="#" class="shortcut-link">
				<span class="shortcut-icon">
					<i class="fa fa-list"></i>
				</span>
				<span class="text">Activity</span>
			</a>
			<a href="{{ url('adm/identity') }}" class="shortcut-link">
				<span class="shortcut-icon">
					<i class="fa fa-cog"></i></span>
				<span class="text">Setting</span>
			</a>
		</div><!-- /grey-container -->
  <div class="panel-body">

      @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
      @endif

			<div class="row">
					<div class="col-sm-6 col-md-3">
						<div class="panel-stat3 bg-danger">
							<h2 class="m-top-none">{{ $jum_user }}</h2>
							<h5>Registered users</h5>
							<div class="stat-icon">
								<i class="fa fa-user fa-3x"></i>
							</div>
							<div class="loading-overlay">
								<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
							</div>
						</div>
					</div><!-- /.col -->
					<div class="col-sm-6 col-md-3">
						<div class="panel-stat3 bg-info">
							<h2 class="m-top-none"><span id="serverloadCount">15</span>%</h2>
							<h5>Server Load</h5>
							<div class="stat-icon">
								<i class="fa fa-hdd-o fa-3x"></i>
							</div>
							<div class="refresh-button">
								<i class="fa fa-refresh"></i>
							</div>
							<div class="loading-overlay">
								<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
							</div>
						</div>
					</div><!-- /.col -->
					<div class="col-sm-6 col-md-3">
						<div class="panel-stat3 bg-warning">
							<h2 class="m-top-none">593</h2>
							<h5>New Orders</h5>
							<div class="stat-icon">
								<i class="fa fa-shopping-cart fa-3x"></i>
							</div>
							<div class="refresh-button">
								<i class="fa fa-refresh"></i>
							</div>
							<div class="loading-overlay">
								<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
							</div>
						</div>
					</div><!-- /.col -->
					<div class="col-sm-6 col-md-3">
						<div class="panel-stat3 bg-success">
							<h2 class="m-top-none" id="visitorCount">7214</h2>
							<h5>Total Visitors</h5>
							<div class="stat-icon">
								<i class="fa fa-bar-chart-o fa-3x"></i>
							</div>
							<div class="refresh-button">
								<i class="fa fa-refresh"></i>
							</div>
							<div class="loading-overlay">
								<i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
							</div>
						</div>
					</div><!-- /.col -->
				</div>
  </div>
</div>
@endsection
