@extends('layouts.admin.template')
@section('title', 'Edit Users')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a href="" onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li><a href="" onclick="location.href='{{ route('adm.user.index') }}'">User</a></li>
		 <li class="active">Edit</li>
	</ul>
</div>
@endsection

@section('content')

<section class="panel panel-primary">
	<header class="panel-heading">Edit Data User</header>
	<div class="panel-body">

	{!! Form::open(['route' => ['adm.user.update', $users->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}

		<div class="form-group">
			<label for="username" class="col-sm-2 control-label">Username</label>
			<div class="col-sm-10">
				<input type="text" name="name" class="form-control" id="name" placeholder="Username" value="{{ $users->name }}" autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label for="email" class="col-sm-2 control-label">Email</label>
			<div class="col-sm-10">
				<input type="text" name="email" class="form-control" id="email" placeholder="Username" value="{{ $users->email }}" autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label for="password" class="col-sm-2 control-label">Password</label>
			<div class="col-sm-10">
				<input type="password" name="password" class="form-control" id="password" placeholder="Password" autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a href="{{ route('adm.user.index') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
			</div>
		</div>
	{!! Form::close() !!}

	</div>
</section>
@endsection
