@extends('layouts.admin.template')
@section('title', 'Profil Users')

@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li class="active">User</li>
	</ul>
</div>
@endsection

@section('content')
<ul class="tab-bar grey-tab">
	<li class="active">
		<a href="#overview">
			<span class="block text-center"><i class="fa fa-home fa-2x"></i></span>
			Overview
		</a>
	</li>
	<li>
		<a href="{{ route('adm.profil.edit', $id) }}">
			<span class="block text-center"><i class="fa fa-edit fa-2x"></i></span>
			Edit Profile
		</a>
	</li>
</ul>

<div class="padding-md">
	<div class="row">
		<div class="col-md-3 col-sm-3">
			<div class="row">
				<div class="col-xs-6 col-sm-12 col-md-6 text-center">
					<a href="#">
						@if(empty($cek->picture))
						<img src="{{ asset('admin/img/unknown.png') }}" alt="User Avatar" class="img-thumbnail">
						@else
						<img src="{{ asset('admin/img/'.$cek->picture) }}" alt="User Avatar" class="img-thumbnail">
						@endif
					</a>
					<div class="seperator"></div>
					<div class="seperator"></div>

				</div><!-- /.col -->
				<div class="col-xs-6 col-sm-12 col-md-6">
					<strong class="font-14">{{ $users->name }}</strong>
					<small class="block text-muted">{{ $users->email }}</small>
					<div class="seperator"></div>
					<a href="#" class="social-connect tooltip-test facebook-hover pull-left m-right-xs" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
					<a href="#" class="social-connect tooltip-test twitter-hover pull-left m-right-xs" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
					<a href="#" class="social-connect tooltip-test google-plus-hover pull-left" data-toggle="tooltip" data-original-title="Google Plus"><i class="fa fa-google-plus"></i></a>
					<div class="seperator"></div>
					<div class="seperator"></div>
					<br>
					<button type="button" class="btn btn-info btn-xs btn-detail"  onclick="location.href='{{ route('profil.change_password', $users->id) }}'">Ubah Password</button>
				</div><!-- /.col -->
			</div><!-- /.row -->
			@if(count($cek) < 1)
			<div class="alert alert-danger">Anda belum melengkapi profil anda ! Klik <a href="{{ route('adm.profil.edit', $users->id) }}">disini</a> untuk melengkapi profil anda.</div>
			@endif
		</div><!-- /.col -->
		<div class="col-md-9 col-sm-9">
			<div class="tab-content">
				<div class="tab-pane fade in active" id="overview">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default fadeInDown animation-delay2">
								<div class="panel-heading">
									<h4><b>My Profile</b></h4>
								</div>
								<div class="panel-body">
									<p>
										<table class="table table-striped">
											<tr><td width="175px">Full Name</td> <td>{{ empty($profil->full_name) ? '' : $profil->full_name }}</td></tr>
											<tr><td>Gender</td>            <td>{{ empty($profil->sex) ? '' : $profil->sex }}</td></tr>
											<tr><td>Phone Number</td>      <td>{{ empty($profil->phone_number) ? '' : $profil->phone_number }}</td></tr>
											<tr><td>Address</td>           <td>{{ empty($profil->address) ? '' : $profil->address }}</td></tr>
											<tr><td>Google</td>            <td>{{ empty($profil->google) ? '' : $profil->google }}</td></tr>
											<tr><td>Facebook</td>          <td>{{ empty($profil->facebook) ? '' : $profil->facebook}}</td></tr>
											<tr><td>Twitter</td>           <td>{{ empty($profil->twitter) ? '' : $profil->twitter }}</td></tr>
											<tr><td>Youtube</td>           <td>{{ empty($profil->youtube) ? '' : $profil->youtube  }}</td></tr>
											<tr><td>Path</td>              <td>{{ empty($profil->path) ? '' : $profil->path }}</td></tr>
											<tr><td>Description</td>       <td>{{ empty($profil->description) ? '' : $profil->description }}</td></tr>
										</table>
									</p>
								</div>
							</div><!-- /panel -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /tab1 -->
			</div><!-- /tab-content -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.padding-md -->
<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body" style="max-height: calc(100vh - 100px); overflow-y: auto;">
        <div id="modalcontent"></div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>
@endsection
