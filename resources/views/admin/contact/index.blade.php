@extends('layouts.admin.template')
@section('title', 'Contact')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li class="active">Inbox</li>
	</ul>
</div><!-- /breadcrumb-->
@endsection
@section('content')
<div class="panel panel-primary table-responsive">
	<div class="panel-heading">&nbsp;</div>
	<div class="padding-md clearfix">
		<table class="table table-condensed table-bordered" id="dataTable">
			<thead>
				<tr>
					<th width="20px">No</th>
					<th>Name</th>
					<th>Email</th>
					<th>Subject</th>
					<th>Date</th>
					<th width="90px">Action</th>
				</tr>
			</thead>
			<tbody>
				@php $no=0; @endphp
				@foreach($contacts as $r)
				@php $no++; @endphp
				<tr>
					<td>{!! $no !!}</td>
					<td>{!! $r->name !!}</td>
					<td><a class="btn btn-xs btn-success" href="mailto:{!! $r->email !!}" target="_top">{!! $r->email !!}</a></td>
					<td>{!! $r->subject !!}</td>
					<td>{!! $r->created_at !!}</td>
					<td>
						<button type="button" class="btn btn-info btn-xs btn-detail" data-toggle="modal" data-target=".bs-example-modal-md" data-source="{{ route('adm.contact.show', $r->id) }}"><i class="fa fa-eye"></i></button>
						{!! Form::open(['route' => ['adm.contact.destroy', $r->id], 'method' => 'DELETE', 'class' => 'delete_form', 'style' => 'display:inline']) !!}
						<button class="btn btn-danger btn-xs delete_button"><i class="fa fa-trash"></i></button>
						{!! Form::close() !!}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div><!-- /.padding-md -->
</div>

<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-body">
        <div id="modalcontent"></div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('style')
<!-- Datatable -->
<link href="{{ asset('admin/css/jquery.dataTables_themeroller.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('js')
<!-- Datatable -->
<script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>

<script>
		$('#dataTable').dataTable( {
			"bJQueryUI": true,
			"bLengthChange": false,
			"sPaginationType": "full_numbers"
		});
		$('.btn-detail').on('click', function(){
				var id = $(this).attr('data-source');
				$('#modalcontent').load(id);
		});
    $('button.delete_button').on('click', function(e){
    	e.preventDefault();
    	var self = $(this);
    	swal({
        title: "Alert",
        text: "Apakah anda yakin ingin menghapus data ini ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#cc3f44",
        confirmButtonText: "Hapus",
        cancelButtonText : "Batal",
        closeOnConfirm: false,
        closeOnCancel: false
    	},
    	function(isConfirm) {
	        if(isConfirm){
	        	self.parents(".delete_form").submit();
	        }else{
	        	swal('Gagal', 'Data anda gagal dihapus !', 'error');
	        }
	    });
    });
</script>
@endsection
