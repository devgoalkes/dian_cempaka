@extends('layouts.admin.template')
@section('title', 'Add Category')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li></i><a onclick="location.href='{{ route('adm.category.index') }}'">Category</a></li>
		 <li class="active">Add</li>
	</ul>
</div><!-- /breadcrumb-->
@endsection
@section('content')
<div class="panel panel-primary">
	<div class="panel-heading">&nbsp;</div>
	<div class="panel-body">
		{!! Form::open(['route' => 'adm.category.store', 'methode' => 'POST', 'class' => 'form-horizontal']) !!}

		<div class="form-group">
			<label for="category_name" class="col-sm-2 control-label">Category Name</label>
			<div class="col-sm-5">
				<input type="text" name="category_name" class="form-control" id="category_name" placeholder="Category Name" autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label for="status" class="col-sm-2 control-label">Status</label>
			<div class="col-lg-10">
				<label class="label-radio inline">
					<input type="radio" name="status" value="Y">
					<span class="custom-radio"></span>
					Aktive
				</label>
				<label class="label-radio inline">
					<input type="radio" name="status" value="N">
					<span class="custom-radio"></span>
					Non Active
				</label>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a onclick="location.href='{{ route('adm.category.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/ckeditor/script/sample.js') }}"></script>
<script src="{{ asset('admin/js/jquery.gritter.min.js') }}"></script>
<script>
	initSample();
</script>

@if(count($errors) > 0)
@foreach($errors->all() as $error)
<script>
	$.gritter.add({
			title: '<i class="fa fa-warning"></i> Error',
			text: '{{ $error }}',
			sticky: false,
			time: '5000',
			class_name: 'gritter-danger'
	});
</script>
@endforeach
@endif

@endsection
