@extends('layouts.admin.template')
@section('title', 'Category')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li class="active">Category</li>
	</ul>
</div>
@endsection
@section('content')
<div class="panel panel-primary table-responsive">
	<div class="panel-heading">&nbsp;</div>
	<div class="padding-md clearfix">
		@section('button')
		<a onclick="location.href='{{ route('adm.category.create') }}'" class="btn btn-primary btn-small"><i class="fa fa-plus"></i> Add Category</a>
		@endsection
		<table class="table table-condensed table-bordered" id="dataTable">
			<thead>
				<tr>
					<th width="20px">No</th>
					<th>Category Name</th>
					<th>Slug</th>
					<th>Active</th>
					<th width="80px">Action</th>
				</tr>
			</thead>
			<tbody>
				@if(!$categorys->isEmpty())
				@foreach($categorys as $r)
				<tr>
					<td>{!! $r->no !!}</td>
					<td>{!! $r->category_name !!}</td>
					<td>{!! $r->slug !!}</td>
					<td>{!! $r->status == 'Y' ? '<i class=\'fa fa-check\'></i>' : '<i class=\'fa fa-times\'></i>' !!}</td>
					<td>
						<a href="{{ route('adm.category.edit', $r->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
						{!! Form::open(['route' => ['adm.category.destroy', $r->id], 'method' => 'DELETE', 'class' => 'delete_form', 'style' => 'display:inline']) !!}
						<button class="btn btn-danger btn-xs delete_button"><i class="fa fa-trash"></i></button>
						{!! Form::close() !!}
					</td>
				</tr>
				@endforeach
				@endif
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/jquery.dataTables_themeroller.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>
<script>
	$(function(){
		$('#dataTable').dataTable( {
			"bJQueryUI": true,
			"bLengthChange": false,
			"sPaginationType": "full_numbers"
		});
		$('button.delete_button').on('click', function(e){
    	e.preventDefault();
    	var self = $(this);
    	swal({
        title: "Alert",
        text: "Apakah anda yakin ingin menghapus data ini ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#cc3f44",
        confirmButtonText: "Hapus",
        cancelButtonText : "Batal",
        closeOnConfirm: false,
        closeOnCancel: false
    	},
    	function(isConfirm) {
	        if(isConfirm){
	        	self.parents(".delete_form").submit();
	        }else{
	        	swal('Cancelled', 'Data anda gagal dihapus !', 'error');
	        }
	    });
    });
	});
</script>
@endsection
