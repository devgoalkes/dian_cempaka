@extends('layouts.admin.template')
@section('title', 'Edit Category')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li><a onclick="location.href='{{ route('adm.category.index') }}'">Category</a></li>
		 <li class="active">Edit</li>
	</ul>
</div><!-- /breadcrumb-->
@endsection
@section('content')
<section class="panel panel-primary">
	<header class="panel-heading">&nbsp;</header>
	<div class="panel-body">
	{!! Form::open(['route' => ['adm.category.update', $categorys->id], 'method' => 'PUT', 'class' => 'form-horizontal']) !!}

		<div class="form-group">
			<label for="category_name" class="col-sm-2 control-label">Category Name</label>
			<div class="col-sm-10">
				{!! Form::text('category_name', $categorys->category_name, array('placeholder' => 'Category Name', 'class' => 'form-control', 'id' => 'category_name')) !!}
			</div>
		</div>
		<div class="form-group">
			<label for="status" class="col-sm-2 control-label">Status</label>
			<div class="col-lg-10">
				@if ($categorys->status == 'Y')
					<label class="label-radio inline">
						<input type="radio" name="status" value="Y" checked>
						<span class="custom-radio"></span>
						Aktive
					</label>
					<label class="label-radio inline">
						<input type="radio" name="status" value="N">
						<span class="custom-radio"></span>
						Non Active
					</label>
		        @else
					<label class="label-radio inline">
						<input type="radio" name="status" value="Y">
						<span class="custom-radio"></span>
						Aktive
					</label>
					<label class="label-radio inline">
						<input type="radio" name="status" value="N" checked>
						<span class="custom-radio"></span>
						Non Active
					</label>
		        @endif
			</div><!-- /.col -->
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
				<a onclick="location.href='{{ route('adm.category.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a>
			</div>
		</div>
	{!! Form::close() !!}

	</div>
</section>
@endsection
