@extends('layouts.admin.template')
@section('title', 'Add Tags')

@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li></i><a onclick="location.href='{{ url('staticpages') }}'"> Static Pages</a></li>
		 <li class="active">Add</li>
	</ul>
</div>
@endsection

@section('content')
<div class="panel panel-primary">
	<div class="panel-heading">Add Tags</div>
	<div class="panel-body">
		{!! Form::open(array('route' => 'tag.store', 'methode' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) !!}
		<div class="form-group">
			<label for="tag_name" class="col-sm-2 control-label">Tag Name</label>
			<div class="col-sm-10">
				<input type="text" name="tag_name" class="form-control" id="tag_name" placeholder="Tag Name" autocomplete="off">
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a onclick="location.href='{{ route('staticpages.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/js/ckeditor/content.css') }}" rel="stylesheet"/>
<link href="{{ asset('admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('admin/js/ckeditor/script/sample.js') }}"></script>
<script src="{{ asset('admin/js/jquery.gritter.min.js') }}"></script>
<script>
	initSample();
	$('.picture').change(function()	{
		var filename = $(this).val().split('\\').pop();
		$(this).parent().find('span').attr('data-title',filename);
		$(this).parent().find('label').attr('data-title','Change file');
		$(this).parent().find('label').addClass('selected');
	});
</script>

@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <script>
			$.gritter.add({
				title: '<i class="fa fa-warning"></i> Error',
				text: '{{ $error }}',
				sticky: false,
				time: '5000',
				class_name: 'gritter-danger'
			});
        </script>
    @endforeach
@endif

@endsection
