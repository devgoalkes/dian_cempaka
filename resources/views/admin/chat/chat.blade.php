@extends('layouts.admin.template')
@section('title', 'Comment')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li class="active">Chat</li>
	</ul>
</div>
@endsection
@section('content')
<div class="row">
  <div class="col-md-12 col-lg-12">
    <div class="chat-wrapper">
				<div class="chat-sidebar border-right bg-white">
					<div class="border-bottom padding-sm" style="height: 40px;">
						Member
					</div>
					<ul class="friend-list">
						<li class="active bounceInDown">
							<a href="#" class="clearfix">
								<img src="img/user.jpg" alt="" class="img-circle">
								<div class="friend-name">
									<strong>Ravi Jaiswal</strong>
								</div>
								<div class="last-message text-muted">Hello, Are you there?</div>
								<small class="time text-muted">Just now</small>
								<small class="chat-alert badge badge-danger">1</small>
							</a>
						</li>
						<li>
							<a href="#" class="clearfix">
								<img src="img/user2.jpg" alt="" class="img-circle">
								<div class="friend-name">
									<strong>Jane Doe</strong>
								</div>
								<div class="last-message text-muted">Lorem ipsum dolor sit amet.</div>
								<small class="time text-muted">5 mins ago</small>
							<small class="chat-alert text-muted"><i class="fa fa-check"></i></small>
							</a>
						</li>
						<li class="active">
							<a href="#" class="clearfix">
								<img src="img/user3.jpg" alt="" class="img-circle">
								<div class="friend-name">
									<strong>Sarah</strong>
								</div>
								<div class="last-message text-muted">Lorem ipsum dolor sit amet.</div>
								<small class="time text-muted">12 mins ago</small>
								<small class="chat-alert badge badge-danger">2</small>
							</a>
						</li>
						<li class="active">
							<a href="#" class="clearfix">
								<img src="img/user4.jpg" alt="" class="img-circle">
								<div class="friend-name">
									<strong>Angelina</strong>
								</div>
								<div class="last-message text-muted">Lorem ipsum dolor sit amet.</div>
								<small class="time text-muted">1 hr ago</small>
								<small class="chat-alert badge badge-danger">1</small>
							</a>
						</li>
						<li>
							<a href="#" class="clearfix">
								<img src="img/user5.jpg" alt="" class="img-circle">
								<div class="friend-name">
									<strong>Kate</strong>
								</div>
								<div class="last-message text-muted">Lorem ipsum dolor sit amet.</div>
								<small class="time text-muted">Yesterday</small>
								<small class="chat-alert text-muted"><i class="fa fa-reply"></i></small>
							</a>
						</li>
					</ul>
				</div>

				<div class="chat-inner scrollable-div">
					<div class="chat-header bg-white">
						<button type="button" class="navbar-toggle" id="friendListToggle">
							<i class="fa fa-comment fa-lg"></i>
							<span class="notification-label bounceIn animation-delay4">7</span>
						</button>
						<div class="pull-right">
							<a class="btn btn-xs btn-default">Add Friend</a>
							<a class="btn btn-xs btn-default">Create Group</a>
						</div>
					</div>

					<div class="chat-message">
						<ul class="chat">
							<li class="left clearfix">
								<span class="chat-img pull-left">
									<img src="img/user.jpg" alt="User Avatar">
								</span>
								<div class="chat-body clearfix">
									<div class="header">
										<strong class="primary-font">Ravi Jaiswal</strong>
										<small class="pull-right text-muted"><i class="fa fa-clock-o"></i> 12 mins ago</small>
									</div>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									</p>
								</div>
							</li>
							<li class="right clearfix">
								<span class="chat-img pull-right">
									<img src="img/user3.jpg" alt="User Avatar">
								</span>
								<div class="chat-body clearfix">
									<div class="header">
										<strong class="primary-font">Sarah</strong>
										<small class="pull-right text-muted"><i class="fa fa-clock-o"></i> 13 mins ago</small>
									</div>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales at.
									</p>
								</div>
							</li>
							<li class="left clearfix">
								<span class="chat-img pull-left">
									<img src="img/user4.jpg" alt="User Avatar">
								</span>
								<div class="chat-body clearfix">
									<div class="header">
										<strong class="primary-font">Angelina</strong>
										<small class="pull-right text-muted"><i class="fa fa-clock-o"></i> 20 mins ago</small>
									</div>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									</p>
								</div>
							</li>
							<li class="right clearfix">
								<span class="chat-img pull-right">
									<img src="img/user2.jpg" alt="User Avatar">
								</span>
								<div class="chat-body clearfix">
									<div class="header">
										<strong class="primary-font">Jane Doe</strong>
										<small class="pull-right text-muted"><i class="fa fa-clock-o"></i> 25 mins ago</small>
									</div>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales at.
									</p>
								</div>
							</li>
							<li class="left clearfix">
								<span class="chat-img pull-left">
									<img src="img/user5.jpg" alt="User Avatar">
								</span>
								<div class="chat-body clearfix">
									<div class="header">
										<strong class="primary-font">Kate</strong>
										<small class="pull-right text-muted"><i class="fa fa-clock-o"></i> 12 mins ago</small>
									</div>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									</p>
								</div>
							</li>
							<li class="right clearfix">
								<span class="chat-img pull-right">
									<img src="img/user3.jpg" alt="User Avatar">
								</span>
								<div class="chat-body clearfix">
									<div class="header">
										<strong class="primary-font">Sarah</strong>
										<small class="pull-right text-muted"><i class="fa fa-clock-o"></i> 13 mins ago</small>
									</div>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales at.
									</p>
								</div>
							</li>
							<li class="left clearfix">
								<span class="chat-img pull-left">
									<img src="img/user.jpg" alt="User Avatar">
								</span>
								<div class="chat-body clearfix">
									<div class="header">
										<strong class="primary-font">Ravi Jaiswal</strong>
										<small class="pull-right text-muted"><i class="fa fa-clock-o"></i> 20 mins ago</small>
									</div>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									</p>
								</div>
							</li>
							<li class="right clearfix">
								<span class="chat-img pull-right">
									<img src="img/user2.jpg" alt="User Avatar">
								</span>
								<div class="chat-body clearfix">
									<div class="header">
										<strong class="primary-font">Jane Doe</strong>
										<small class="pull-right text-muted"><i class="fa fa-clock-o"></i> 25 mins ago</small>
									</div>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales at.
									</p>
								</div>
							</li>
						</ul>
					</div>
					<div class="chat-box bg-white">
						<div class="input-group">
							<input class="form-control border no-shadow no-rounded" placeholder="Type your message here">
							<span class="input-group-btn">
								<button class="btn btn-success no-rounded" type="button">Send</button>
							</span>
						</div><!-- /input-group -->
					</div>
				</div>
			</div><!-- /chat-wrapper -->
    </div>
</div>
@endsection

@section('js')
<script>
$('.info-btn').on('click', function () {
    $("#Messages").toggleClass('col-sm-12 col-sm-9');
});
</script>
@endsection
