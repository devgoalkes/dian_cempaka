@extends('layouts.admin.template')
@section('title', 'Galery')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li class="active">Galery</li>
	</ul>
</div>
@endsection
@section('content')
<div class="panel panel-primary table-responsive">
	<div class="panel-heading">
		&nbsp;
	</div>
	<div class="padding-md clearfix">
		@section('button')
		<a onclick="location.href='{{ route('adm.galery.create') }}'" class="btn btn-primary btn-small"><i class="fa fa-plus"></i> Add Galery</a>
		@endsection
		<table class="table table-condensed table-bordered" id="dataTable">
			<thead>
				<tr>
					<th width="20px">No</th>
					<th>Title</th>
					<th>Picture</th>
					<th>Album</th>
					<th>Created At</th>
					<th width="100px">Action</th>
				</tr>
			</thead>
			<tbody>
				@php $no=0; @endphp
				@foreach($galery as $r)
				@php $no++; @endphp
				<tr>
					<td>{{ $no }}</td>
					<td>{{ $r->galery_title }}</td>
					<td>{{ $r->picture }}</td>
					<td>{{ $r->album->title }}</td>
          <td>{{ tglIndo($r->created_at) }}</td>
					<td>
            <a class="gallery-zoom btn btn-xs btn-info" href="{{ url('storage/uploads/galery/'.$r->picture) }}"><i class="fa fa-eye"></i></a>

						{!! Form::open(['route' => ['adm.galery.destroy', $r->id], 'method' => 'DELETE', 'class' => 'delete_form', 'style' => 'display:inline']) !!}
						<button class="btn btn-danger btn-xs delete_button"><i class="fa fa-trash"></i></button>
						{!! Form::close() !!}

					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/jquery.dataTables_themeroller.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/sweetalert.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/colorbox/colorbox.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('admin/js/jquery.colorbox.min.js') }}"></script>

<script>
	$(function(){
      $('button.delete_button').on('click', function(e){
        e.preventDefault();
        var self = $(this);
        swal({
          title: "Alert",
          text: "Apakah anda yakin ingin menghapus data ini ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#cc3f44",
          confirmButtonText: "Hapus",
          cancelButtonText : "Batal",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm) {
            if(isConfirm){
              self.parents(".delete_form").submit();
            }else{
              swal('Cancelled', 'Data anda gagal dihapus !', 'error');
            }
        });
      });
			$('.gallery-zoom').colorbox({
				rel:'gallery',
				maxWidth:'80%'

			});
			$('#dataTable').dataTable( {
				"bJQueryUI": true,
				"bLengthChange": false,
				"sPaginationType": "full_numbers"
			});
	});
</script>
@endsection
