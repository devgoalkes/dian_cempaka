@extends('layouts.admin.template')
@section('title', 'Album')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li class="active">Album</li>
	</ul>
</div><!-- /breadcrumb-->
@endsection
@section('content')
<div class="panel panel-primary table-responsive">
	<div class="panel-heading">
		&nbsp;
	</div>
	<div class="padding-md clearfix">
		@section('button')
		<a onclick="location.href='{{ route('adm.album.create') }}'" class="btn btn-primary btn-small"><i class="fa fa-plus"></i> Add Album</a>
		@endsection
		<table class="table table-condensed table-bordered" id="dataTable">
			<thead>
				<tr>
					<th width="20px">No</th>
					<th>Title</th>
					<th>Picture</th>
					<th>Active</th>
					<th>Created At</th>
					<th width="100px">Action</th>
				</tr>
			</thead>
			<tbody>
				@php $no=0; @endphp
				@forelse($albums as $r)
				@php $no++; @endphp
				<tr>
					<td>{{ $no }}</td>
					<td>{{ $r->title }}</td>
					<td>{{ $r->picture }}</td>
					<td>{{ $r->active }}</td>
					<td>{{ $r->created_at }}</td>
					<td>
						@if(!empty($r->picture))
						<a class="gallery-zoom btn btn-xs btn-info" href="{{ url('storage/uploads/album/'.$r->picture) }}"><i class="fa fa-eye"></i></a>
						@endif
						<a href="{{ route('adm.album.edit', $r->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
						{!! Form::open(['route' => ['adm.album.destroy', $r->id], 'method' => 'DELETE', 'class' => 'delete_form', 'style' => 'display:inline']) !!}
						<button class="btn btn-danger btn-xs delete_button"><i class="fa fa-trash"></i></button>
						{!! Form::close() !!}
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="6">Belum ada data !</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div><!-- /.padding-md -->
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/jquery.dataTables_themeroller.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/sweetalert.css') }}" rel="stylesheet">
<link href="{{ asset('admin/css/colorbox/colorbox.css') }}" rel="stylesheet">
@endsection

@section('js')
<!-- Datatable -->
<script src="{{ asset('admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('admin/js/jquery.colorbox.min.js') }}"></script>

<script>
		$('#dataTable').dataTable( {
			"bJQueryUI": true,
			"bLengthChange": false,
			"sPaginationType": "full_numbers"
		});
		$('.gallery-zoom').colorbox({
			rel:'gallery',
			maxWidth:'90%',
			width:'800px'
		});
    $('button.delete_button').on('click', function(e){
    	e.preventDefault();
    	var self = $(this);
    	swal({
        title: "Alert",
        text: "Apakah anda yakin ingin menghapus data ini ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#cc3f44",
        confirmButtonText: "Hapus",
        cancelButtonText : "Batal",
        closeOnConfirm: false,
        closeOnCancel: false
    	},
    	function(isConfirm) {
	        if(isConfirm){
	        	self.parents(".delete_form").submit();
	        }else{
	        	swal('Cancelled', 'Data anda gagal dihapus !', 'error');
	        }
	    });
    });
</script>
@endsection
