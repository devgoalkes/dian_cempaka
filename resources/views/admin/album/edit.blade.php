@extends('layouts.admin.template')
@section('title', 'Edit Album')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a href="" onclick="location.href='{{ route('adm') }}'"> Home</a></li>
		 <li><a href="" onclick="location.href='{{ route('adm.album.index') }}'">Album</a></li>
		 <li class="active">Edit</li>
	</ul>
</div>
@endsection
@section('content')
<div class="panel panel-primary">
	<div class="panel-heading">Edit Album</div>
	<div class="panel-body">
		{!! Form::open(array('route' => ['adm.album.update', $albums->id], 'method' => 'PUT', 'class' => 'form-horizontal')) !!}

			<div class="form-group">
				<label for="title" class="col-sm-2 control-label">Album Title</label>
				<div class="col-sm-10">
					<input type="text" name="title" class="form-control" id="title" placeholder="Album Title" value="{{ $albums->title }}" autocomplete="off">
				</div>
			</div>
			<div class="form-group">
				<label for="picture" class="col-sm-2 control-label">Picture</label>
				<div class="col-sm-5">
					<div class="upload-file">
						<input type="file" name="picture" id="picture" class="picture">
						<label data-title="Select file" for="picture">
							<span data-title="No file selected..."></span>
						</label>
					</div>
          <br>
          <input type="hidden" name="picture_old" value="{{ $albums->picture }}">
          <span class="help-block alert alert-success">{{ $albums->picture }}</span>
				</div>
			</div>
			<div class="form-group">
				<label for="active" class="col-sm-2 control-label">Status</label>
				<div class="col-lg-10">
          @if($albums->active == 'Y')
          <label class="label-radio inline">
            <input type="radio" name="active" value="Y" checked>
            <span class="custom-radio"></span>
            Active
          </label>
          <label class="label-radio inline">
            <input type="radio" name="active" value="N">
            <span class="custom-radio"></span>
            Non Active
          </label>
          @else
          <label class="label-radio inline">
            <input type="radio" name="active" value="Y">
            <span class="custom-radio"></span>
            Aktive
          </label>
          <label class="label-radio inline">
            <input type="radio" name="active" value="N" checked>
            <span class="custom-radio"></span>
            Non Active
          </label>
          @endif
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
					<a onclick="location.href='{{ route('adm.album.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
				</div>
			</div>

		{!! Form::close() !!}

	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.gritter.min.js') }}"></script>
<script>
	$('.picture').change(function()	{
		var filename = $(this).val().split('\\').pop();
		$(this).parent().find('span').attr('data-title',filename);
		$(this).parent().find('label').attr('data-title','Change file');
		$(this).parent().find('label').addClass('selected');
	});
</script>

@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <script>
			$.gritter.add({
				title: '<i class="fa fa-warning"></i> Error',
				text: '{{ $error }}',
				sticky: false,
				time: '5000',
				class_name: 'gritter-danger'
			});
        </script>
    @endforeach
@endif

@endsection
