<?php

Route::name('tes')->get('tes', function () {
    return view('tes');
});
Route::group(['namespace' => 'Blog'], function() {
    Route::name('home')->get('/', 'HomeController@index');

    Route::name('artikel.list')->get('/artikel', 'NewsController@index');
    Route::name('artikel.detail')->get('/artikel/{slug}', 'NewsController@show');

    // Route::name('tentang_kami')->get('tentang-kami', function () {
    //     return view('front.about_us');
    // });
    Route::name('kontak_kami')->get('kontak-kami', 'ContactController@index');
    Route::name('kategori.list')->get('/kategori/{slug}', 'HomeController@category_detail');
    Route::name('agenda.index')->get('/agenda', 'ScheduleController@index');
    Route::name('agenda.list')->get('/agenda/{detail}', 'ScheduleController@detail');

    Route::name('pages.detail')->get('/pages/{slug}', 'HomeController@pages');
    Route::name('tag.list')->get('/tag/{slug}', 'HomeController@tag');
    Route::name('comment.post')->post('/comment', 'HomeController@comment');
    Route::name('searchajax')->get('/searchajax', 'HomeController@autoComplete');
    Route::name('portfolio')->get('/portfolio', 'HomeController@gallery');
    // Route::name('search')->post('/search', 'SearchController');
    Route::name('home.contact')->post('/home/contact', 'ContactController@sendMail');
});

Route::group(['prefix' => 'adm'], function () {
  Auth::routes();

  Route::group(['namespace' => 'Admin'], function() {

    Route::name('adm')->get('/', 'HomeController@index');

    Route::resource('/agenda', 'AgendaController');
    Route::get('/agenda/edit/{id}', 'AgendaController@edit');
    Route::put('/agenda/update/{id}', 'AgendaController@update');
    Route::get('/agenda/show/{id}', 'AgendaController@show');

    Route::resource('/album', 'AlbumController', ['names' => 'adm.album']);
    Route::resource('/banner', 'BannerController', ['names' => 'adm.banner']);
    Route::resource('/category', 'CategoryController', ['names' => 'adm.category']);
    Route::resource('/client', 'ClientController', ['names' => 'adm.client']);
    Route::resource('/comment', 'CommentController', ['names' => 'adm.comment']);
    // Route::name('adm.comment.index')->get('/comment', 'CommentController@index');
    // Route::name('adm.comment.show')->get('/comment/show/{comment}', 'CommentController@show');
    Route::name('comment.destroy')->delete('/comment/destroy/{comment}', 'CommentController@destroy');

    Route::name('contact.count')->get('/contact/count-message', 'ContactController@countMessage');
    Route::resource('/contact', 'ContactController', ['names' => 'adm.contact']);

    Route::resource('/galery', 'GaleryController', ['names' => 'adm.galery']);
    Route::resource('/identity', 'IdentityController', ['names' => 'adm.identity']);
    Route::resource('/identity_social', 'IdentitySocialController', ['names' => 'adm.identity_social']);
    Route::resource('/menu', 'MenuController', ['names' => 'adm.menu']);
    Route::resource('/news', 'NewsController', ['names' => 'adm.news']);
    Route::resource('/staticpages', 'StaticpagesController', ['names' => 'adm.staticpages']);
    Route::resource('/tag', 'TagController', ['names' => 'adm.tag']);
    Route::resource('/user', 'UserController', ['names' => 'adm.user']);

    Route::name('profil.change_password')->get('/profil/change-password/{id}', 'UserPasswordController@changePassword');
    Route::name('profil.change_password.update')->put('/profil/change-password/{id}', 'UserPasswordController@update');
    Route::name('profil.store')->post('/profil/store', 'UserProfilController@store');
    Route::name('profil.update')->put('/profil/update/{id}', 'UserProfilController@update');

    Route::get('/user/getposts', ['as'=>'adm.user.getposts','uses'=>'UserController@getPosts']);
    Route::name('adm.profil')->get('/profil', 'UserController@profil');
    Route::name('adm.profil.edit')->get('/edit_profil/{id}', 'UserController@edit_profil');

  });

});
