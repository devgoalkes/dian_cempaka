let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');

mix.scripts([
         'public/front/js/jquery.js',
         'public/front/js/bootstrap.min.js',
         'public/front/js/jquery.mCustomScrollbar.concat.min.js',
         'public/front/js/jquery.fancybox.js',
         'public/front/js/mixitup.js',
         'public/front/js/owl.js',
         'public/front/js/wow.js',
         'public/front/js/appear.js',
         'public/front/js/isotope.js',
         'public/front/js/jquery-ui.js',
         'public/front/js/script.js'

      ], 'public/front/js/script.min.js')
   .version();

mix.styles([
         'public/front/css/bootstrap.css',
         'public/front/css/font-awesome.css',
         'public/front/css/flaticon.css',
         'public/front/css/animate.css',
         'public/front/css/owl.css',
         'public/front/css/jquery-ui.css',
         'public/front/css/jquery.fancybox.min.css',
         'public/front/css/jquery.bootstrap-touchspin.css',
         'public/front/css/jquery.mCustomScrollbar.min.css',
         'public/front/css/style.css',
         'public/front/css/responsive.css'

      ], 'public/front/css/style.min.css')
   .version();
