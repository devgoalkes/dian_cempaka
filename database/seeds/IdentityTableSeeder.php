<?php

use Illuminate\Database\Seeder;
use App\IdentityModel;
use Carbon\Carbon;

class IdentityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      IdentityModel::create([
        "id" => 1,
        "website_name" => "My Company",
        "website_address" => "-",
        "meta_description" => "-",
        "meta_keyword" => "-",
        "favicon" => "-",
        "mail_address" => "info@mycompany",
        "address" => "Jl. Kaki nomor 212",
        "phone" => "081234567890",
        "created_at" => Carbon::now(),
        "updated_at" => null
      ]);
    }
}
