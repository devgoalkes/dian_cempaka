<?php

use Illuminate\Database\Seeder;
use App\MenuModel;
class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $menus = [
                  [
                    "id": 2,
                    "id_parent": "0",
                    "name": "Artikel",
                    "url": "#",
                    "icon": "fa-newspaper-o",
                    "admin": "Y",
                    "active": "Y"
                  ],
                  [
                    "id": 5,
                    "id_parent": "2",
                    "name": "Kategori",
                    "url": "adm/category",
                    "icon": "fa-newspaper-o",
                    "admin": "Y",
                    "active": "Y"
                  ],
                  [
                    "id": 6,
                    "id_parent": "2",
                    "name": "Tag",
                    "url": "adm/tag",
                    "icon": "fa-tags",
                    "admin": "Y",
                    "active": "Y"
                  ],
                  [
                    "id": 7,
                    "id_parent": "0",
                    "name": "Portfolio",
                    "url": "#",
                    "icon": "fa-image",
                    "admin": "Y",
                    "active": "Y"
                  ],
                  [
                    "id": 8,
                    "id_parent": "7",
                    "name": "Album",
                    "url": "adm/album",
                    "icon": "fa-image",
                    "admin": "Y",
                    "active": "Y"
                  ],
                  [
                    "id": 9,
                    "id_parent": "7",
                    "name": "Galeri",
                    "url": "adm/galery",
                    "icon": "fa-image",
                    "admin": "Y",
                    "active": "Y"
                  ],
                  [
                    "id": 10,
                    "id_parent": "2",
                    "name": "Komentar",
                    "url": "adm/comment",
                    "icon": "fa-commenting",
                    "admin": "Y",
                    "active": "Y"
                  ],
                  [
                    "id": 11,
                    "id_parent": "0",
                    "name": "Halaman Statis",
                    "url": "adm/staticpages",
                    "icon": "fa-copy",
                    "admin": "Y",
                    "active": "Y"
                  ],
                  [
                    "id": 12,
                    "id_parent": "2",
                    "name": "Artikel",
                    "url": "adm/news",
                    "icon": "fa-newspaper-o",
                    "admin": "Y",
                    "active": "Y"
                  ],
                  [
                    "id": 13,
                    "id_parent": "0",
                    "name": "Banner",
                    "url": "adm/banner",
                    "icon": "fa-file-image-o",
                    "admin": "Y",
                    "active": "Y"
                  ],
                  [
                    "id": 14,
                    "id_parent": "0",
                    "name": "Setting",
                    "url": "#",
                    "icon": "fa-gears",
                    "admin": "Y",
                    "active": "Y"
                  ],
                  [
                    "id": 15,
                    "id_parent": "14",
                    "name": "Identitas",
                    "url": "adm/identity",
                    "icon": "fa-gears",
                    "admin": "Y",
                    "active": "Y"
                  ],
                  [
                    "id": 16,
                    "id_parent": "0",
                    "name": "Home",
                    "url": "/",
                    "icon": "fa-home",
                    "admin": "N",
                    "active": "Y"
                  ],
                  [
                    "id": 17,
                    "id_parent": "0",
                    "name": "Tentang Kami",
                    "url": "/tentang-kami",
                    "icon": "fa-user-circle-o",
                    "admin": "N",
                    "active": "Y"
                  ],
                  [
                    "id": 21,
                    "id_parent": "0",
                    "name": "Portfolio",
                    "url": "/portfolio",
                    "icon": "fa-image",
                    "admin": "N",
                    "active": "Y"
                  ],
                  [
                    "id": 23,
                    "id_parent": "0",
                    "name": "Produk",
                    "url": "/pages/produk",
                    "icon": "fa-th-list",
                    "admin": "N",
                    "active": "Y"
                  ],
                  [
                    "id": 24,
                    "id_parent": "23",
                    "name": "Kempress Copper",
                    "url": "/pages/kempres-copper",
                    "icon": "fa-500px",
                    "admin": "N",
                    "active": "Y"
                  ],
                  [
                    "id": 25,
                    "id_parent": "23",
                    "name": "Kempress Stainless",
                    "url": "/pages/kempress-stainless",
                    "icon": "fa-500px",
                    "admin": "N",
                    "active": "Y"
                  ],
                  [
                    "id": 26,
                    "id_parent": "0",
                    "name": "Client",
                    "url": "/adm/client",
                    "icon": "fa-500px",
                    "admin": "Y",
                    "active": "Y"
                  ],
                  [
                    "id": 27,
                    "id_parent": "0",
                    "name": "Sertifikat",
                    "url": "pages/sertifikat",
                    "icon": "fa-file-text-o",
                    "admin": "N",
                    "active": "Y"
                  ],
                  [
                    "id": 28,
                    "id_parent": "0",
                    "name": "Artikel",
                    "url": "/artikel",
                    "icon": "fa-newspaper-o",
                    "admin": "N",
                    "active": "Y"
                  ],
                  [
                    "id": 29,
                    "id_parent": "0",
                    "name": "Kontak Kami",
                    "url": "/kontak-kami",
                    "icon": "fa-vcard-o",
                    "admin": "N",
                    "active": "Y"
                  ],
                  [
                    "id": 30,
                    "id_parent": "14",
                    "name": "Social Media",
                    "url": "adm/identity_social",
                    "icon": "fa-500px",
                    "admin": "Y",
                    "active": "Y"
                  ]
                ];

        foreach ($menus as $menu) {
            \DB::table('menu')->insert([
                'id' => $menu['id'],
                'id_parent' => $menu['id_parent'],
                'name' => $menu['name'],
                'url' => $menu['url'],
                'icon' => $menu['icon'],
                'admin' => $menu['admin'],
                'active' => $menu['active']
            ]);
        }
    }
}
