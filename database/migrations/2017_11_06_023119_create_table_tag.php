<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTag extends Migration
{

    public function up()
    {
        Schema::create('tag', function (Blueprint $table) {
            $table->string('id', 36)->primary();
            $table->string('tag_name');
            $table->string('tag_slug');
            $table->integer('count')->nullable(true);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tag');
    }
}
