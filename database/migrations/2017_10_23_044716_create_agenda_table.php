<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda', function (Blueprint $table) {
            $table->string('id', 36)->primary();
            $table->string('title');
            $table->string('slug');
            $table->text('content');
            $table->string('place');
            $table->string('sender');
            $table->date('date_start');
            $table->date('date_end');
            $table->string('time');
            $table->string('user_id');
            $table->string('active', 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda');
    }
}
