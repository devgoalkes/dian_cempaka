<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumModel extends Model
{
  protected $table = 'album';
  public $incrementing = false;

  public function galeries()
  {
      return $this->hasMany(GaleryModel::class, 'album_id', 'id');
  }
}
