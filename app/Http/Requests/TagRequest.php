<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         return [
           'tag_name' => 'required|min:3|unique:tag,tag_name',
         ];
     }

     public function messages()
     {
         return [
           'tag_name.required' => 'Nama tag wajib diisi !',
           'tag_name.min' => 'Minimal nama tag terdiri dari 3 karakter !',
           'tag_name.unique' => 'Nama tag sudah ada !',
         ];
     }
}
