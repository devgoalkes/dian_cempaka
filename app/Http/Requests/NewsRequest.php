<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'user_id' => 'required',
            'category_name' => 'required',
            'content' => 'required',
            'picture.*' => 'image|file|mimes:jpeg,jpg',
            'active' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Title wajib diisi !',
            'user_id.required' => 'User wajib diisi !',
            'category_name.required' => 'Nama Kategori wajib diisi !',
            'category_name.min' => 'Minimal nama kategory terdiri dari 5 karakter !',
            'content.required' => 'Content news harus diisi !',
            'picture.*.image' => 'Pastikan Anda Memasukkan Sebuah Gambar!',
            'picture.*.mimes' => 'Gambar harus berextensi: jpeg, jpg !',
            'active.required' => 'Active wajib diisi !',
        ];
    }
}
