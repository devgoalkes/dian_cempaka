<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgendaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'content' => 'required',
            'place' => 'required',
            'sender' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
            'time' => 'required',
            'user_id' => 'required',
            'active' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Judul agenda wajib diisi !',
            'content.required' => 'Content wajib diisi !',
            'place.required' => 'Place wajib diisi !',
            'sender.required' => 'Sender wajib diisi !',
            'date_start.required' => 'Date Start wajib diisi !',
            'date_end.required' => 'Date End wajib diisi !',
            'time.required' => 'Time wajib diisi !',
            'user_id.required' => 'User wajib diisi !',
            'active.required' => 'Active wajib diisi !',
        ];
    }
}
