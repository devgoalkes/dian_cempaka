<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GaleryRequest extends FormRequest
{

      public function authorize()
      {
          return true;
      }

     public function rules()
     {
         return [
             'galery_title' => 'required',
             'album_id' => 'required',
             'picture' => 'required',
             'picture.*' => 'image|file|mimes:jpeg,jpg'
         ];
     }

     public function messages()
     {
         return [
             'galery_title.required' => 'Title wajib diisi !',
             'album_id.required' => 'Album wajib diisi !',
             'picture.required' => 'Picture wajib diisi !',
             'picture.*.image' => 'Pastikan Anda Memasukkan Sebuah Gambar!',
             'picture.*.mimes' => 'Gambar harus berextensi: jpeg, jpg !'
         ];
     }
}
