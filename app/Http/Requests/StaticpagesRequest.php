<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StaticpagesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'content' => 'required',
            'active' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Title wajib diisi !',
            'content.required' => 'Content news harus diisi !',
            'active.required' => 'Active wajib diisi !',
        ];
    }

}
