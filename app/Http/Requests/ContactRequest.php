<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         return [
             'contact_name' => 'required',
             'contact_email' => 'required',
             'contact_subject' => 'required',
             'contact_message' => 'required'
         ];
     }

     public function messages()
     {
         return [
           'contact_name' => 'Nama wajib diisi !',
           'contact_email' => 'Email wajib diisi !',
           'contact_subject' => 'Subject wajib diisi !',
           'contact_message' => 'Pesan wajib diisi !'
         ];
     }
}
