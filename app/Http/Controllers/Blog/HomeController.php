<?php
namespace App\Http\Controllers\Blog;

use App\Http\Requests\CommentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Helpers\ItjadHelper;

use Ramsey\Uuid\Uuid;
use App\{ ClientModel, NewsModel, TagModel, GaleryModel, CategoryModel, CommentModel, StaticpagesModel, AlbumModel, BannerModel };

class HomeController extends Controller
{
    public function index(){
        $data['lasts'] = NewsModel::select('news.id','users.name as post_name', 'category_name', 'category.slug as category_slug', 'title', 'news.slug as news_slug', 'content', 'picture', 'news.active', 'news.created_at as news_created_at')
          			        ->leftjoin('users','users.id', '=', 'news.user_id')
          			        ->leftjoin('category', 'category.id', '=', 'news.category_id')
          			        ->orderBy('news.created_at', 'desc')
          			        ->limit(3)
          			        ->get();
        $data['galleries'] = GaleryModel::limit(4)->get();
        $data['slides'] = BannerModel::all();
        $data['clients'] = ClientModel::all();
        return view('front.home', $data);
    }

    public function comment(CommentRequest $request)
    {
        $data['id']         = Uuid::uuid4()->toString();
        $data['news_id']    = $request->news_id;
        $data['comment_name']= $request->comment_name;
        $data['url']        = $request->url;
        $data['content']    = $request->content;
        $data['active']     = 'Y';
        $data['created_at'] = date('Y-m-d H:i:s');
        $query = NewsModel::select('news.id as news_id','users.name as post_name', 'category_name', 'title', 'news.slug as news_slug', 'content', 'picture', 'news.active', 'news.created_at as news_created_at')
              	        ->leftjoin('users','users.id', '=', 'news.user_id')
              	        ->leftjoin('category', 'category.id', '=', 'news.category_id')
              	        ->orderBy('news.created_at', 'desc')
              	        ->where('news.id', '=', $request->news_id)
              	        ->limit(1)
              					->first();

        if (CommentModel::insert($data)) {
            return redirect('artikel/'.$query->news_slug)->withSuccess('Komentar berhasil dikirim !');
        }else{
            return redirect('artikel/'.$query->news_slug)->withError('Gagal mengirim komentar !');
        }
    }

    public function pages($slug)
    {
    	 $static = StaticpagesModel::where('slug', '=', $slug)
	        ->first();
			return view('front.staticpages', compact('static', 'helper'));
    }


		public function category_detail($slug)
		{
			$category = CategoryModel::where('slug', $slug)->first();
			$news = $category->news()->orderBy('created_at', 'desc')
            					->paginate(10);
			return view('front.category_detail', compact(['category', 'news']));
		}

    public function tag($slug)
		{
        $cek = TagModel::where('tag_slug', $slug)->get();

        $data['tag_name'] = TagModel::where('tag_slug', $slug)->first();
        $data['tags'] = NewsModel::orderBy('news.created_at', 'desc')
                        ->where('news.tag', 'like', '%'.$slug.'%')
                        ->paginate(10);
        return view('front.tag_detail', $data);
		}


    public function gallery()
		{
  			$data['albums'] = AlbumModel::get();
        $data['galeries'] = GaleryModel::get();
  			return view('front.gallery', $data);
		}


    public function autoComplete(Request $request) {
        $query = $request->get('term','');
        $news  = NewsModel::where('title','LIKE','%'.$query.'%')->get();
        $data  = array();
        foreach ($news as $r) {
            $data[]=array('title'=>$r->title,'slug'=>$r->id);
        }
        if(count($data))
            return $data;
        else
            return ['title'=>'No Result Found','slug'=>''];
    }
}
