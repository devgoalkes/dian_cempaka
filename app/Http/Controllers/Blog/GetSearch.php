<?php

namespace App\Http\Controllers\Blog;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GetSearch extends Controller
{
    public function __invoke($key)
    {
        $data['read'] = NewsModel::select('news.id','users.name as post_name', 'category_name', 'title', 'news.slug as news_slug', 'news.content', 'picture', 'news.news_read', 'news.active', 'news.created_at as news_created_at')
                  ->leftjoin('users','users.id', '=', 'news.user_id')
                  ->leftjoin('category', 'category.id', '=', 'news.category_id')
                  ->where('news.title','LIKE','%'.$key.'%')
                  ->orWhere('news.content', 'LIKE','%'.$key.'%')
                  ->orderBy('news.created_at', 'desc')
                  ->get();

        $data['helper'] = new \App\Helpers\ItjadHelper();

        if (count($data['read']) > 0) {
          return view('front.search', $data);
        }else{
          return redirect('error404');
        }
    }
}
