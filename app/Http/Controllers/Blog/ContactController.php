<?php

namespace App\Http\Controllers\Blog;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use Ramsey\Uuid\Uuid;

use App\{ IdentityModel, ContactModel };

use Mail;

class ContactController extends Controller
{
    private $name;
    private $email;
    private $subj;
    private $message;

    public function index(){
      $data['identities'] = IdentityModel::first();
      return view('front.contact_us', $data);
    }

    public function sendMail(ContactRequest $request)
    {

        $isi = array("name" =>"PT. Dian Cempaka",
                    "sender_subject" => $request->contact_subject,
                    "sender_message" => $request->contact_message,
                    "sender_email" => $request->contact_email,
                    "sender_name" => $request->contact_name
                  );

        $cs = IdentityModel::first();

        Mail::send('front.mail_format', $isi, function($message) use($cs) {
        $message->to($cs->mail_address)
                ->subject('Email dari Customer');
        $message->from('info@diancempaka.com','Website PT. Dian Cempaka');
        });

        $data['id']     = Uuid::uuid4()->toString();
        $data['name'] 	= $request->contact_name;
        $data['email'] 	= $request->contact_email;
        $data['subject']= $request->contact_subject;
        $data['message']= $request->contact_message;
        $data['read']   = '0';
        $data['created_at'] = date('Y-m-d H:i:s');

        if (ContactModel::insert($data)) {
            return response()->json('success');
        }else{
            return response()->json('error');
        }
    }
}
