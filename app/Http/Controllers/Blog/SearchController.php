<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\NewsModel;

class SearchController extends Controller
{
    public function __invoke(Request $request)
    {
      $news = NewsModel::where('title', 'LIKE', "%$request->q%")->paginate();

      return view('front.search', compact('news'));
    }
}
