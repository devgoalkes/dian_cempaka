<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\{ NewsModel, CommentModel };

class NewsController extends Controller
{
    public function index()
    {
        $data['news'] = NewsModel::orderBy('news.created_at', 'desc')->paginate(5);
          return view('front.news_all', $data);
    }

    public function show($slug)
    {
          $cek = NewsModel::where('slug', $slug)->get();

    			NewsModel::where('slug', '=', $slug)->update(
            ['news_read' => DB::raw('news_read + 1')]
          );
          
        	$data['article'] = NewsModel::where('news.slug', '=', $slug)->first();
          $q = $data['article'];
          $data['comments'] = CommentModel::where('news_id', '=', $q->id)
                            ->orderBy('created_at', 'desc')
                            ->paginate(3);
  				return view('front.news_detail', $data);

    }
}
