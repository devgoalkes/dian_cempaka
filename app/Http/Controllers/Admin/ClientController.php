<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests\ClientRequest;
use Illuminate\Support\Str;
use Storage;


use App\ClientModel;
use Ramsey\Uuid\Uuid;
use Session;
use Alert;
use Intervention\Image\Facades\Image;

class ClientController extends Controller
{
    public function index()
    {
        $clients = ClientModel::select('id', 'url', 'name', 'picture', 'created_at')
                  ->orderBy('created_at', 'desc')
                  ->get();
        return view('admin.client.index', compact('clients'));
    }


    public function create()
    {
        return view('admin.client.create');
    }

    public function store(ClientRequest $request)
    {
        request()->validate([
            'picture' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $photoName = time().'.'.$request->picture->getClientOriginalExtension();
        $request->picture->move(public_path('storage/uploads/client'), $photoName);
        $data['picture']= $photoName;

        $data['url']        = $request->url;
        $data['name']       = $request->name;
        $data['created_at'] = date('Y-m-d H:i:s');

        if (ClientModel::insert($data)) {
            Alert::success('Data berhasil ditambah !', 'Sukses');
            return redirect('adm/client');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/client');
         }
    }

    public function edit($id)
    {
        if ($id!='') {
            $data['client'] = ClientModel::where('id', $id)->first();
            return view('admin.client.edit', $data);
        }
    }

    public function update(ClientRequest $request, $id)
    {
        if ($request->picture != '') {
            request()->validate([
                'picture' => 'required|mimes:jpeg,png,jpg|max:2048',
            ]);
            \File::delete('storage/uploads/client/'.$request->picture_old);
            $photoName = time().'.'.$request->picture->getClientOriginalExtension();
            $request->picture->move(public_path('storage/uploads/client'), $photoName);
            $data['picture'] = $photoName;
        }else{
            $data['picture']    = $request->picture_old;
        }

        $data['url']        = $request->url;
        $data['name']       = $request->name;
        $data['updated_at'] = date('Y-m-d H:i:s');

        if (ClientModel::where('id', $id)->update($data)) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect('adm/client');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/client/edit/'.$id);
         }
    }

    public function destroy($id)
    {
        if ($id != '') {
            $client = ClientModel::where('id', $id)->first();
              \File::delete('storage/uploads/client/'.$client->picture);
            if (ClientModel::where('id', $id)->delete()){
                Alert::success('Data berhasil dihapus', 'Sukses');
                return redirect('adm/client');
            }else{
                Alert::error('Silahkan di coba lagi !', 'Error');
                return redirect('adm/client');
            }
        }
    }
}
