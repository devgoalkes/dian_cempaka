<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\GaleryRequest;
use Illuminate\Support\Str;

use App\{ GaleryModel, AlbumModel };
use Ramsey\Uuid\Uuid;
use Session;
use Alert;
use Storage;

class GaleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $galery = GaleryModel::orderBy('created_at', 'desc')->get();
        return view('admin.galery.index', compact('galery'));
    }

    public function create()
    {
        $album = AlbumModel::orderBy('created_at', 'desc')->get();
        return view('admin.galery.create', compact('album'));
    }

    public function store(GaleryRequest $request)
    {
        $photoName = time().'.'.$request->picture->getClientOriginalExtension();
        $request->picture->move(public_path('storage/uploads/galery'), $photoName);

        $data['id']           = Uuid::uuid4()->toString();
        $data['album_id']     = $request->album_id;
        $data['galery_title'] = $request->galery_title;
        $data['galery_slug']  = Str::slug($request->galery_title);
        $data['picture']      = $photoName;
        $data['description']  = $request->description;
        $data['created_at']   = date('Y-m-d H:i:s');

        if (GaleryModel::insert($data)) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect('adm/galery');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/galery');
         }
    }

    public function update(GaleryRequest $request, $id)
    {
        if ($request->picture != '') {
          $photoName = time().'.'.$request->picture->getClientOriginalExtension();
          $request->picture->move(public_path('storage/uploads/galery'), $photoName);
          $data['picture']      = $photoName;
        }
        $data['album_id']     = $request->album_id;
        $data['galery_title'] = $request->galery_title;
        $data['galery_slug']  = Str::slug($request->galery_title);
        $data['description']  = $request->description;
        $data['updated_at']   = date('Y-m-d H:i:s');

          if (GaleryModel::where('id', $id)->update($data)) {
              Alert::success('Data berhasil disimpan', 'Sukses');
              return redirect('adm/galery');
           }else{
              Alert::error('Silahkan di coba lagi !', 'Error');
              return redirect('adm/galery/edit/'.$id);
           }
    }

    public function destroy($id)
    {
        if ($id != '') {
            $data = GaleryModel::select('picture')->where(array('id' => $id))->get()->first();

            if (GaleryModel::where('id', $id)->delete()){
                Storage::delete('storage/uploads/galery/'.$data->picture);
                Alert::success('Data berhasil dihapus', 'Sukses');
                return redirect('adm/galery');
            }else{
                Alert::error('Silahkan di coba lagi !', 'Error');
                return redirect('adm/galery');
            }
        }
    }
}
