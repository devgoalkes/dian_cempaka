<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\BannerRequest;
use Illuminate\Support\Str;

use App\BannerModel;
use Ramsey\Uuid\Uuid;
use Session;
use Alert;
use File;
use Storage;

class BannerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $banners = BannerModel::orderBy('created_at', 'desc')->get();
        return view('admin.banner.index', compact('banners'));
    }

    public function create()
    {
        return view('admin.banner.create');
    }

    public function store(BannerRequest $request)
    {
        $photoName = time().'.'.$request->picture->getClientOriginalExtension();
        $request->picture->move(public_path('storage/uploads/banner'), $photoName);

        $data['id']         = Uuid::uuid4()->toString();
        $data['title']      = $request->title;
        $data['url']        = $request->url;
        $data['picture']    = $photoName;
        $data['active']     = $request->active;
        $data['created_at'] = date('Y-m-d H:i:s');

        if (BannerModel::insert($data)) {
            Alert::success('Data berhasil ditambah !', 'Sukses');
            return redirect('adm/banner');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/banner');
         }
    }

    public function edit($id)
    {
        if ($id!='') {
            $banner  = BannerModel::where(array('id' => $id))->get()->first();
            return view('admin.banner.edit', compact('banner'));
        }
    }

    public function update(BannerRequest $request, $id)
    {
        if ($request->picture != '') {
          request()->validate([
              'gambar' => 'required|gambar|mimes:jpeg,png,jpg,gif,svg|max:2048',
          ]);
          $photoName = time().'.'.$request->picture->getClientOriginalExtension();
          $request->picture->move(public_path('storage/uploads/banner'), $photoName);
          $data['picture'] = $photoName;
        }

        $data['title'] = $request->title;
        $data['url'] = $request->url;
        $data['active'] = $request->active;
        $data['updated_at'] = date('Y-m-d H:i:s');

        if (BannerModel::where('id', $id)->update($data)) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect('adm/banner');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/banner/edit/'.$id);
         }
    }

    public function destroy($id)
    {
        if ($id != '') {
            $data = BannerModel::select('picture')->where(array('id' => $id))->get()->first();
            if (BannerModel::where('id', $id)->delete()){
                Storage::delete('storage/uploads/banner/'.$data->picture);
                Alert::success('Data berhasil dihapus', 'Sukses');
                return redirect('adm/banner');
            }else{
                Alert::error('Silahkan di coba lagi !', 'Error');
                return redirect('adm/banner');
            }
        }
    }
}
