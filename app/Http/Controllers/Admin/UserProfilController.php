<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserProfilRequest;

use Ramsey\Uuid\Uuid;
use Session;
use Alert;
use LogActivity;

class UserProfilController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function store(UserProfilRequest $request)
  {
      $data['id']           = Uuid::uuid4()->toString();
      $data['user_id']      = $request->id;
      $data['full_name']    = $request->full_name;
      $data['sex']          = $request->sex;
      $data['address']      = $request->address;
      $data['phone_number'] = $request->phone_number;
      $data['facebook']     = $request->facebook;
      $data['twitter']      = $request->twitter;
      $data['google']       = $request->google;
      $data['youtube']      = $request->youtube;
      $data['path']         = $request->path;
      $data['description']   = $request->description;
      $data['created_at']   = date('Y-m-d H:i:s');

      if (DB::table('user_profils')->insert($data)) {
          Alert::success('Data berhasil disimpan', 'Sukses');
          return redirect('adm/profil');
       }else{
          Alert::error('Silahkan coba lagi !', 'Error');
          return redirect('adm/edit_profil');
       }
  }

  public function update(UserProfilRequest $request, $id)
  {
      $data['user_id']      = $request->user_id;
      $data['full_name']    = $request->full_name;
      $data['sex']          = $request->sex;
      $data['address']      = $request->address;
      $data['phone_number'] = $request->phone_number;
      $data['facebook']     = $request->facebook;
      $data['twitter']      = $request->twitter;
      $data['google']       = $request->google;
      $data['youtube']      = $request->youtube;
      $data['path']         = $request->path;
      $data['description']   = $request->description;
      $data['updated_at']   = date('Y-m-d H:i:s');

      if (DB::table('user_profils')->where('id', $id)->update($data)) {
          Alert::success('Data berhasil disimpan', 'Sukses');
          return redirect('adm/profil');
       }else{
          Alert::error('Silahkan di coba lagi !', 'Error');
          return redirect('adm/profil/edit/'.$id);
       }
  }
}
