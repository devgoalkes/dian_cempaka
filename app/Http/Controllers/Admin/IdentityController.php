<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\IdentityModel;
use Alert;

class IdentityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['r'] = IdentityModel::orderBy('created_at', 'desc')
                  ->first();
        if(empty($data['r'])){
          return redirect()->route('adm.identity.create');
        }else{
          return view('admin.identity.index', $data);
        }

    }

    public function create()
    {
        return view('admin.identity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['website_name'] 	  = $request->website_name;
        $data['website_address']  = $request->website_address;
        $data['meta_description'] = $request->meta_description;
        $data['meta_keyword'] 		= $request->meta_keyword;
        $data['favicon'] 			    = $request->favicon;
        $data['mail_address'] 		= $request->mail_address;
        $data['address'] 			    = $request->address;
        $data['phone'] 			      = $request->phone;

        if (DB::table('identity')->insert($data)) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect('adm/identity');
        }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/identity/create');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['r'] = DB::table('identity')
                  ->where('id', $id)
                  ->orderBy('created_at', 'desc')
                  ->first();
        return view('admin.identity.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
         $data['website_name'] 	  = $request->website_name;
         $data['website_address'] = $request->website_address;
         $data['meta_description']= $request->meta_description;
         $data['meta_keyword'] 		= $request->meta_keyword;
         $data['favicon'] 			  = $request->favicon;
         $data['mail_address'] 		= $request->mail_address;
         $data['address'] 			  = $request->address;
         $data['phone'] 			    = $request->phone;

         if (DB::table('identity')->where('id', $id)->update($data)) {
             Alert::success('Data berhasil disimpan', 'Sukses');
             return redirect('adm/identity');
         }else{
             Alert::error('Silahkan di coba lagi !', 'Error');
             return redirect('adm/identity/edit/'.$id);
         }
     }
}
