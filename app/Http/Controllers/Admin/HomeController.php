<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\UserModel;
use App\NewsModel;
use App\ContactModel;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $jum_user = UserModel::count();
        $jum_news = NewsModel::count();
        $jum_contacts = ContactModel::count();
        return view('admin.home', compact('jum_user', 'jum_news', 'jum_contacts'));
    }
}
