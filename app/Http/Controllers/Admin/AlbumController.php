<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\AlbumRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;


use App\AlbumModel;
use Ramsey\Uuid\Uuid;
use Session;
use Alert;
use Intervention\Image\Facades\Image;

class AlbumController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $albums = AlbumModel::orderBy('created_at', 'desc')->get();
        return view('admin.album.index', compact('albums'));
    }

    public function create()
    {
        return view('admin.album.create');
    }

    public function store(AlbumRequest $request)
    {
        if ($request->picture != '') {
            request()->validate([
                'picture' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $photoName = time().'.'.$request->picture->getClientOriginalExtension();
            $request->picture->move(public_path('storage/uploads/album'), $photoName);
            $data['picture'] = $photoName;
        }else{
            $data['picture']    = $request->picture_old;
        }
        $data['id']         = Uuid::uuid4()->toString();
        $data['title']      = $request->title;
        $data['slug']       = Str::slug($request->title);
        $data['active']     = $request->active;
        $data['created_at'] = date('Y-m-d H:i:s');

        if (AlbumModel::insert($data)) {
            Alert::success('Data berhasil ditambah !', 'Sukses');
            return redirect('adm/album');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/album');
         }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        if ($id!='') {
            $albums = AlbumModel::where(array('id' => $id))->get()->first();
            return view('admin.album.edit', compact('albums'));
        }
    }

    public function update(AlbumRequest $request, $id)
    {
        if ($request->picture != '') {
            request()->validate([
                'picture' => 'required|mimes:jpeg,png,jpg|max:2048',
            ]);
            \File::delete('storage/uploads/album/'.$request->picture_old); //Menghapus foto yang lama
            $photoName = time().'.'.$request->picture->getClientOriginalExtension();
            $request->picture->move(public_path('storage/uploads/album'), $photoName);
            $data['picture'] = $photoName;
        }else{
            $data['picture']    = $request->picture_old;
        }

        $data['title']      = $request->title;
        $data['slug']       = Str::slug($request->title);
        $data['active']     = $request->active;
        $data['updated_at'] = date('Y-m-d H:i:s');

        if (AlbumModel::where('id', $id)->update($data)) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect('adm/album');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/album/edit/'.$id);
         }
    }

    public function destroy($id)
    {
        if ($id != '') {
            $album = AlbumModel::where('id', $id)->first();
              \File::delete('storage/uploads/album/'.$album->picture);
            if (AlbumModel::where('id', $id)->delete()){
                Alert::success('Data berhasil dihapus', 'Sukses');
                return redirect('adm/album');
            }else{
                Alert::error('Silahkan di coba lagi !', 'Error');
                return redirect('adm/album');
            }
        }
    }
}
