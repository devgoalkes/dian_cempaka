<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Str;

use App\CategoryModel;
use Ramsey\Uuid\Uuid;
use Session;
use Alert;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        DB::statement(DB::raw('set @nomor=0 '));
        $categorys = CategoryModel::select(DB::raw('@nomor := @nomor + 1 as no'), 'id', 'category_name', 'slug', 'status', 'created_at')->orderBy('created_at', 'desc')->get();
        return view('admin.category.index', compact('categorys'));
    }

    public function create()
    {
        return view('admin.category.create');
    }

    public function store(CategoryRequest $request)
    {
        $data['id'] = Uuid::uuid4()->toString();
        $data['category_name'] = $request->category_name;
        $data['slug'] = Str::slug($request->category_name);
        $data['status'] = $request->status;
        $data['created_at'] = date('Y-m-d H:i:s');

        if (CategoryModel::insert($data)) {
            Alert::success('Data berhasil ditambah !', 'Sukses');
            return redirect('adm/category');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/category');
         }
    }

    public function edit($id)
    {
        if ($id!='') {
            $categorys = CategoryModel::where(array('id' => $id))->get()->first();
            return view('admin.category.edit', compact('categorys'));
        }
    }

    public function update(CategoryRequest $request, $id)
    {
        $data['category_name'] = $request->category_name;
        $data['slug'] = Str::slug($request->category_name);
        $data['status'] = $request->status;
        $data['updated_at'] = date('Y-m-d H:i:s');

        if (CategoryModel::where('id', $id)->update($data)) {
            Alert::success('Data berhasil diubah !', 'Sukses');
            return redirect('adm/category');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/category/edit/'.$id);
         }
    }

    public function destroy($id)
    {
        if ($id != '') {
            if (CategoryModel::where('id', $id)->delete()){
                Alert::success('Data berhasil dihapus', 'Sukses');
                return redirect('adm/category');
            }else{
                Alert::error('Data tidak berhasil dihapus', 'Error');
                return redirect('adm/category');
            }
        }
    }
}
