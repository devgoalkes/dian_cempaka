<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\AgendaRequest;
use Illuminate\Support\Str;

use App\AgendaModel;
use Ramsey\Uuid\Uuid;
use Session;
use Alert;

class AgendaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        DB::statement(DB::raw('set @nomor=0 '));
        $agendas = AgendaModel::select(DB::raw('@nomor := @nomor + 1 as no'), 'agenda.id', 'title', 'content', 'place', 'sender', 'date_start', 'date_end', 'time', 'users.name', 'agenda.created_at')
                  ->leftjoin('users', 'users.id', '=', 'agenda.user_id')
                  ->orderBy('created_at', 'desc')->get();
        return view('admin.agenda.index', compact('agendas'));
    }

    public function create()
    {
        return view('admin.agenda.create');
    }

    public function show($id)
    {
      if ($id!='') {
          $data['agendas'] = AgendaModel::where(array('id' => $id))->get()->first();
          return view('admin.agenda.show', $data);
      }
    }

    public function store(AgendaRequest $request)
    {
        $id = $request->user_id;
        $users = DB::table('users')->where('name', $id )->first();

        $data['id'] = Uuid::uuid4()->toString();
        $data['title'] = $request->title;
        $data['slug'] = Str::slug($request->title);
        $data['content'] = $request->content;
        $data['place'] = $request->place;
        $data['sender'] = $request->sender;
        $data['date_start'] = $request->date_start;
        $data['date_end'] = $request->date_end;
        $data['time'] = $request->time;
        $data['user_id'] = $users->id;
        $data['active'] = $request->active;
        $data['created_at'] = date('Y-m-d H:i:s');

        if (DB::table('agenda')->insert($data)) {
            Alert::success('Data berhasil ditambah !', 'Sukses');
            return redirect('adm/agenda');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/agenda');
         }
    }

    public function edit($id)
    {
        if ($id!='') {
            $agendas = DB::table('agenda')->where(array('id' => $id))->get()->first();
            return view('admin.agenda.edit', compact('agendas'));
        }
    }

    public function update(AgendaRequest $request, $id)
    {
        $data['title'] = $request->title;
        $data['slug'] = Str::slug($request->place);
        $data['content'] = $request->content;
        $data['place'] = $request->place;
        $data['sender'] = $request->sender;
        $data['date_start'] = $request->date_start;
        $data['date_end'] = $request->date_end;
        $data['time'] = $request->time;
        $data['user_id'] = $request->id;
        $data['active'] = $request->active;
        $data['updated_at'] = date('Y-m-d H:i:s');

        if (DB::table('agenda')->where('id', $id)->update($data)) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect('adm/agenda');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/agenda/edit/'.$id);
         }
    }

    public function destroy($id)
    {
        if ($id != '') {
            if (DB::table('agenda')->where('id', $id)->delete()){
                Alert::success('Data berhasil dihapus', 'Sukses');
                return redirect('adm/agenda');
            }else{
                Alert::error('Silahkan di coba lagi !', 'Error');
                return redirect('adm/agenda');
            }
        }
    }
}
