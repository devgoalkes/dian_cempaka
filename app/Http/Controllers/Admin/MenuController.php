<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\MenuRequest;
use Illuminate\Support\Str;

use App\MenuModel;
use Ramsey\Uuid\Uuid;
use Session;
use Alert;

class MenuController extends Controller
{
		public function __construct()
		{
				$this->middleware('auth');
		}

		public function index()
		{
				$menus = MenuModel::get();
				return view('admin.menu.index', compact('menus'));
		}

    public function create()
    {
				$parent_menus = MenuModel::select('id', 'name', 'active')
											->where('active', '=', 'Y')
											->get();
				return view('admin.menu.create', compact('parent_menus'));
    }

    public function store(MenuRequest $request)
    {
			$data['id_parent'] 	= $request->id_parent;
			$data['name'] 			= $request->name;
			$data['url'] 				= $request->url;
			$data['icon'] 			= $request->icon;
			$data['admin'] 			= $request->admin;
			$data['active'] 		= $request->active;

			if (MenuModel::insert($data)) {
					Alert::success('Data berhasil disimpan', 'Sukses');
					return redirect('adm/menu');
			 }else{
					Alert::error('Silahkan di coba lagi !', 'Error');
					return redirect('adm/menu');
			 }
    }

    public function edit($id)
    {
			if ($id!='') {
					$data['menus'] = MenuModel::where(array('id' => $id))
											->get()
											->first();
					$data['parent_menus'] = MenuModel::select('id', 'name', 'active', 'icon')
											->where('active', '=', 'Y')
											->get();
					return view('admin.menu.edit', $data);
			}
    }

    public function update(MenuRequest $request, $id)
    {
				$data['id_parent'] 	= $request->id_parent;
				$data['name'] 			= $request->name;
				$data['url'] 				= $request->url;
				$data['icon'] 			= $request->icon;
				$data['admin'] 			= $request->admin;
				$data['active'] 		= $request->active;

				if (MenuModel::where('id', $id)->update($data)) {
						Alert::success('Data berhasil disimpan', 'Sukses');
						return redirect('adm/menu');
				}else{
						Alert::error('Silahkan di coba lagi !', 'Error');
						return redirect('adm/menu/edit/'.$id);
				}
    }

    public function destroy($id)
    {
				if ($id != '') {
						if (MenuModel::where('id', $id)->delete()){
								Alert::success('Data berhasil dihapus', 'Sukses');
								return redirect('adm/menu');
						}else{
								Alert::error('Silahkan di coba lagi !', 'Error');
								return redirect('adm/menu');
						}
				}else{
						return redirect('adm/menu');
				}
    }
}
