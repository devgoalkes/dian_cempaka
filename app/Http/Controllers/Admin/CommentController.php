<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\GaleryRequest;
use Illuminate\Support\Str;

use App\CommentModel;
use Ramsey\Uuid\Uuid;
use Session;
use Alert;
use Storage;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['comment'] = CommentModel::orderBy('created_at', 'desc')->get();
        return view('admin.comment.index', $data);
    }


    public function show($id)
    {
        if ($id!='') {
          $data['comment'] = CommentModel::where('comment.id', $id)->first();
            return view('admin.comment.show', $data);
        }
    }

    public function destroy($id)
    {
        if ($id != '') {
            if (CommentModel::where('id', $id)->delete()){
                Alert::success('Data berhasil dihapus', 'Sukses');
                return redirect('adm/comment');
            }else{
                Alert::error('Silahkan di coba lagi !', 'Error');
                return redirect('adm/comment');
            }
        }
    }
}
