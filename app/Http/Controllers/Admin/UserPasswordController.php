<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Support\Facades\Auth;

use App\UserModel;
use Alert;
use Hash;
class UserPasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function changePassword($id)
    {
          $data['user'] = UserModel::where('id',$id)->get()->first();
          return view('admin.user_profil.change_password', $data);
    }

    public function update(ChangePasswordRequest $request, $id)
    {
        if(Hash::check($request->old_password, Auth::user()->password)) {
            $user = UserModel::where('id',$id)->get()->first();
            $user->password = bcrypt($request->password);
            $user->update();
            Alert::success('Password berhasil diubah', 'Sukses');
            return redirect('adm/profil');
        }else{
          Alert::error('Gagal update password.', 'Error');
          return redirect('adm/profil');
        }


    }

}
