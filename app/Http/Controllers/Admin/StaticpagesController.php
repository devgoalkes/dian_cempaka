<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StaticpagesRequest;
use Illuminate\Support\Str;

use App\StaticpagesModel;
use Ramsey\Uuid\Uuid;
use Session;
use Alert;

class StaticpagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $static = StaticpagesModel::select('id','title', 'slug', 'active', 'created_at')
            ->orderBy('staticpages.created_at', 'desc')->get();
        return view('admin.staticpages.index', compact('static'));
    }

    public function create()
    {
        return view('admin.staticpages.create');
    }

    public function store(StaticpagesRequest $request)
    {
      if ($request->hasFile('picture')) {

        $photoName = time().'.'.$request->picture->getClientOriginalExtension();
        $request->picture->move(public_path('storage/uploads/static_pages'), $photoName);
        $data['id']         = Uuid::uuid4()->toString();
        $data['title']      = $request->title;
        $data['slug']       = Str::slug($request->title);
        $data['content']    = $request->content;
        $data['picture']    = $photoName;
        $data['active']     = $request->active;
        $data['created_at'] = date('Y-m-d H:i:s');

      }else{

        $data['id']         = Uuid::uuid4()->toString();
        $data['title']      = $request->title;
        $data['slug']       = Str::slug($request->title);
        $data['content']    = $request->content;
        $data['active']     = $request->active;
        $data['created_at'] = date('Y-m-d H:i:s');

      }

        if (StaticpagesModel::insert($data)) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect('adm/staticpages');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/staticpages');
         }
    }

    public function show($id)
    {
      if ($id!='') {
        $data['static'] = StaticpagesModel::select('title', 'slug', 'content', 'picture', 'active', 'created_at')
                        ->where('id', $id)
                        ->first();
          return view('admin.staticpages.show', $data);
      }
    }

    public function edit($id)
    {
        if ($id!='') {
            $data['static'] = StaticpagesModel::where(array('id' => $id))->get()->first();
            return view('admin.staticpages.edit', $data);
        }
    }

    public function update(StaticpagesRequest $request, $id)
    {

        if (isset($request->picture)) {

          $photoName = time().'.'.$request->picture->getClientOriginalExtension();
          $request->picture->move(public_path('storage/uploads/static_pages'), $photoName);

          $data['title']        = $request->title;
          $data['slug']         = Str::slug($request->title);
          $data['content']      = $request->content;
          $data['active']       = $request->active;
          $data['picture']      = $photoName;
          $data['updated_at']   = date('Y-m-d H:i:s');

        }else{
          $data['title']        = $request->title;
          $data['slug']         = Str::slug($request->title);
          $data['content']      = $request->content;
          $data['active']       = $request->active;
          $data['updated_at']   = date('Y-m-d H:i:s');
        }

        if (StaticpagesModel::where('id', $id)->update($data)) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect('adm/staticpages');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/staticpages/edit/'.$id);
         }
    }

    public function destroy($id)
    {
        if ($id != '') {
            if (StaticpagesModel::where('id', $id)->delete()){
                   Alert::success('Data berhasil dihapus', 'Sukses');
                    return redirect('adm/staticpages');
            }else{
                Alert::error('Silahkan di coba lagi !', 'Error');
                return redirect('adm/staticpages');
            }
        }
    }
}
