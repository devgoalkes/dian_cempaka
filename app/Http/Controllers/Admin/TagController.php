<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\TagRequest;
use Illuminate\Support\Str;

use App\TagModel;
use Ramsey\Uuid\Uuid;
use Session;
use Alert;

class TagController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
      $tag = TagModel::orderBy('created_at', 'desc')->get();
      return view('admin.tag.index', compact('tag'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request)
    {
      $data['id'] = Uuid::uuid4()->toString();
      $data['tag_name'] = $request->tag_name;
      $data['tag_slug'] = Str::slug($request->tag_name);
      $data['created_at'] = date('Y-m-d H:i:s');

      if (TagModel::insert($data)) {
          Alert::success('Data berhasil ditambah !', 'Sukses');
          return redirect('adm/tag');
       }else{
          Alert::error('Silahkan di coba lagi !', 'Error');
          return redirect('adm/tag');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($id!='') {
            $data['tags'] = TagModel::where(array('id' => $id))->get()->first();
            return view('admin.tag.edit', $data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($id != '') {
            if (TagModel::where('id', $id)->delete()){
                Alert::success('Data berhasil dihapus', 'Sukses');
                return redirect('adm/tag');
            }else{
                Alert::error('Data tidak berhasil dihapus', 'Error');
                return redirect('adm/tag');
            }
        }
    }
}
