<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\ContactModel;
use Ramsey\Uuid\Uuid;
use Session;
use Alert;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['contacts'] = ContactModel::orderBy('created_at', 'desc')->get();
        return view('admin.contact.index', $data);
    }

    public function show($id)
    {
        if ($id!='') {
            $contact = ContactModel::where('id', $id)->get()->first();
            $data['read'] 		= '1';
            ContactModel::where('id', $id)->update($data);
            return view('admin.contact.show', compact('contact'));
        }else{
          $contact = "Maaf, data anda tidak dapat diakses.";
          return view('admin.contact.show', compact('contact'));
        }
    }

    public function countMessage()
    {
        $value = ContactModel::where('read', 0)->count();
        return $value;
    }

    public function destroy($id)
    {
        if ($id != '') {
            if (ContactModel::where('id', $id)->delete()){
                Alert::success('Data berhasil dihapus', 'Sukses');
                return redirect('adm/contact');
            }else{
                Alert::error('Silahkan di coba lagi !', 'Error');
                return redirect('adm/contact');
            }
        }
    }
}
