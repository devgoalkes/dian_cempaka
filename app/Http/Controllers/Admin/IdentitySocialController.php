<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\IdentitySocialsModel;
use App\IdentityModel;
use Alert;

class IdentitySocialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['socials'] = IdentitySocialsModel::all();
        return view('admin.identity_social.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.identity_social.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $identity = IdentityModel::first();
        $data['driver']     = $request->driver;
        $data['identity_id']= $identity->id;
        $data['username']   = $request->username;
        $data['created_at'] = date('Y-m-d H:i:s');

        if (IdentitySocialsModel::insert($data)) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect('adm/identity_social');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/identity_social');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($id!='') {
            $data['socials'] = IdentitySocialsModel::where('id', $id)->first();
            return view('admin.identity_social.edit', $data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($id != '') {
            if (IdentitySocialsModel::where('id', $id)->delete()){
                Alert::success('Data berhasil dihapus', 'Sukses');
                return redirect('adm/identity_social');
            }else{
                Alert::error('Silahkan di coba lagi !', 'Error');
                return redirect('adm/identity_social');
            }
        }else{
            return redirect('adm/identity_social');
        }
    }
}
