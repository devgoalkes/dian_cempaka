<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\NewsRequest;
use Illuminate\Support\Str;
use Yajra\DataTables\Html\Builder;
use App\{ NewsModel, CategoryModel, TagModel, CommentModel };
use Ramsey\Uuid\Uuid;
use DataTables, Session, Alert, Storage;
use Auth;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['news'] = NewsModel::select('news.id', 'news.title', 'news.slug', 'users.name as user_name', 'category.category_name', 'news.active', 'news.created_at')
            ->leftjoin('category', 'category.id', '=', 'news.category_id')
            ->leftjoin('users', 'users.id', '=', 'news.user_id')
            ->orderBy('news.created_at', 'desc')->get();
        return view('admin.news.index', $data);
    }

    public function create()
    {
        $data['categorys'] = CategoryModel::select('id', 'category_name', 'created_at')
										->orderBy('created_at', 'desc')
										->get();
        $data['tags'] = TagModel::select('id', 'tag_name', 'tag_slug', 'created_at')
										->orderBy('created_at', 'desc')
										->get();
        return view('admin.news.create', $data);
    }

    public function store(NewsRequest $request)
    {
        if ($request->picture != '') {
            $photoName = time().'.'.$request->picture->getClientOriginalExtension();
            $request->picture->move(public_path('storage/uploads/news'), $photoName);
            $data['picture'] = $photoName;
        }else{
              $photoName = '';
        }

        $tag = $request->input('tag');
        if ($tag != '') {
          $tags = implode(',', $tag);
        }else{
          $tags = '';
        }
        $data['id']         = Uuid::uuid4()->toString();
        $data['user_id']    = Auth::user()->id;
        $data['category_id']= $request->category_name;
        $data['title']      = $request->title;
        $data['slug']       = Str::slug($request->title);
        $data['content']    = $request->content;
        $data['picture']    = $photoName;
        $data['active']     = $request->active;
        $data['tag']        = $tags;
        $data['news_read']  = '0';
        $data['created_at'] = date('Y-m-d H:i:s');

        if (NewsModel::insert($data)) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect('adm/news');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/news');
         }
    }

    public function show($id)
    {
        if ($id!='') {
            $data['news'] = NewsModel::where('id', $id)->first();
            $data['comment'] = CommentModel::where('news_id', $id)
                        ->orderBy('created_at', 'desc')
                        ->get();
            return view('admin.news.show', $data);
        }
    }

    public function edit($id)
    {
        if ($id!='') {
            $data['tags'] = TagModel::select('id', 'tag_name', 'tag_slug', 'created_at')
    										->orderBy('created_at', 'desc')
    										->get();
            $data['news'] = NewsModel::where(array('id' => $id))->get()->first();
            $data['categorys'] = CategoryModel::select('id', 'category_name', 'created_at')->orderBy('created_at', 'desc')->get();
            return view('admin.news.edit', $data);
        }
    }

    public function update(NewsRequest $request, $id)
    {
        if ($request->picture != '') {
            $photoName = time().'.'.$request->picture->getClientOriginalExtension();
            $request->picture->move(public_path('storage/uploads/news'), $photoName);
            $data['picture'] = $photoName;
        }else {
            $data['picture']  = $request->picture_old;
        }
        $data['user_id']      = $request->user_id;
        $data['category_id']  = $request->category_name;
        $data['title']        = $request->title;
        $data['slug']         = Str::slug($request->title);
        $data['content']      = $request->content;
        $data['active']       = $request->active;
        $data['updated_at']   = date('Y-m-d H:i:s');

        if (NewsModel::where('id', $id)->update($data)) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect('adm/news');
        }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect('adm/news/edit/'.$id);
        }
    }

    public function destroy($id)
    {
        if ($id != '') {
            $data = NewsModel::select('picture')->where(array('id' => $id))->get()->first();
            if (NewsModel::where('id', $id)->delete()){
                Storage::delete('upload/news/'.$data->picture);
                Alert::success('Data berhasil dihapus', 'Sukses');
                return redirect('adm/news');
            }else{
                Alert::error('Silahkan di coba lagi !', 'Error');
                return redirect('adm/news');
            }
        }
    }
}
