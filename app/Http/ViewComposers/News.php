<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\NewsModel;

class News
{

  	public $latest;


  	function __construct(NewsModel $latest)
  	{
  		$this->latest = NewsModel::limit(5)->get();
  	}

  	public function compose(View $view)
  	{
  		$view->with('latest', $this->latest);
  	}
}
