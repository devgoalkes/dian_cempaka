<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\ContactModel;

class Messages
{

  	public $message;


  	function __construct(ContactModel $message)
  	{
  		$this->message = ContactModel::where('read', 0)->get();
  	}

  	public function compose(View $view)
  	{
  		$view->with('messages', $this->message);
  	}
}
