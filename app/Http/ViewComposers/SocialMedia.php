<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\IdentitySocialsModel;

class SocialMedia
{

  	public $social;


  	function __construct(IdentitySocialsModel $social)
  	{
  		$this->social = IdentitySocialsModel::all();
  	}

  	public function compose(View $view)
  	{
  		$view->with('social', $this->social);
  	}
}
