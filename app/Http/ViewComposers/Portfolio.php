<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\AlbumModel;

class Portfolio
{

  	public $portfolio;


  	function __construct(AlbumModel $portfolio)
  	{
  		$this->portfolio = AlbumModel::all();
  	}

  	public function compose(View $view)
  	{
  		$view->with('portfolio', $this->portfolio);
  	}
}
