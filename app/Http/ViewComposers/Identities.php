<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\IdentityModel;

class Identities
{

  	public $identity;


  	function __construct(IdentityModel $identity)
  	{
  		$this->identity = IdentityModel::first();
  	}

  	public function compose(View $view)
  	{
  		$view->with('identities', $this->identity);
  	}
}
