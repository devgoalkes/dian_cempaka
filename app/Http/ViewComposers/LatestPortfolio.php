<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\GaleryModel;

class LatestPortfolio
{

  	public $portfolio;


  	function __construct(GaleryModel $portfolio)
  	{
  		$this->portfolio = GaleryModel::latest()->limit(3)->get();
  	}

  	public function compose(View $view)
  	{
  		$view->with('latest_portfolio', $this->portfolio);
  	}
}
