<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdentityModel extends Model
{
    protected $table = 'identity';
    public $incrementing = false;
}
