<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfilModel extends Model
{
  protected $table = 'user_profils';
  public $incrementing = false;

  public function user()
  {
    return $this->belongsTo(User::class);
  }
}
