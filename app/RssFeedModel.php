<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RssFeedModel extends Model
{
    public function getRssFeed(){
      $rssfeed = '<?xml version="1.0" encoding="ISO-8859-1"?>';
      $rssfeed.= '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">'. "\n";
      $rssfeed .= '<channel>'. "\n";
      $rssfeed .= '<atom:link href="'.Request::root().'/rss" rel="self" type="application/rss+xml" />'."\n";
      $rssfeed .= '<title>GoAlkes RSS Feed</title>'. "\n";
      $rssfeed .= '<link>localhost:8000</link>'. "\n";
      $rssfeed .= '<description>Rss Saya </description>'. "\n";
      $rssfeed .= '<language>en-us</language>'. "\n";
      $rssfeed .= '<copyright>Copyright (C) 2013 www.blog-goalkes.com</copyright>'. "\n";

      $article = RssFeedModel::getArticles();

      foreach($article as $r){

        $link = Request::root().'/read/'.$r->news_slug;

        /*cek di feedvalidator.org, ada error,
        maka lakukan ini (sesuaikan saja haha)*/
        $desc= str_replace(array('&nbsp;','&nb','&nbsp','...','&'),
        array('','','','',''),
        trim(Str::limit(strip_tags($r->content),200)));

        $rssfeed .= '<item>' . "\n";
        $rssfeed .= '<title> '.$r->title.' </title>'. "\n";
        $rssfeed .= '<description> <![CDATA['.$desc.' ]]></description>'. "\n";
        $rssfeed .= '<link> '.$link.' </link>'. "\n";
        $rssfeed .= '<pubDate> ' . date("D, d M Y H:i:s O", strtotime($r->created_at)) . ' </pubDate>'. "\n";
        $rssfeed .= '<guid> '.$link.' </guid>'. "\n";
        $rssfeed .= '</item>'. "\n";

      }//endforeach

      $rssfeed .= '</channel>'. "\n";
      $rssfeed .= '</rss>'. "\n";
      return $rssfeed;
    }


    private function getArticles(){
        $result = DB::table('news')->select('news.id','users.name as post_name', 'category_name', 'category.slug as category_slug', 'title', 'news.slug as news_slug', 'content', 'picture', 'news.active', 'news.created_at as news_created_at')
    			        ->leftjoin('users','users.id', '=', 'news.user_id')
    			        ->leftjoin('category', 'category.id', '=', 'news.category_id')
    			        ->orderBy('news.created_at', 'desc')
    			        ->limit(10)
    			        ->get();
    }
}
