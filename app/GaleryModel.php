<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GaleryModel extends Model
{
  protected $table = 'galery';
  public $incrementing = false;

  public function album()
  {
      return $this->hasOne(AlbumModel::class, 'id', 'album_id');
  }
}
