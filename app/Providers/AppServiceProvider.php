<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.admin.partial._script', 'App\Http\ViewComposers\Messages');

        View::composer('layouts.front.partials._head', 'App\Http\ViewComposers\Identities');
        View::composer('layouts.front.partials._header', 'App\Http\ViewComposers\Identities');
        View::composer('layouts.front.partials._footer', 'App\Http\ViewComposers\Identities');
        View::composer('layouts.front.partials._sidebar', 'App\Http\ViewComposers\News');
        View::composer('layouts.front.partials._sidebar', 'App\Http\ViewComposers\Portfolio');

        View::composer('layouts.front.partials._content-footer', 'App\Http\ViewComposers\SocialMedia');
        View::composer('layouts.front.partials._header', 'App\Http\ViewComposers\SocialMedia');
        View::composer('layouts.front.partials._content-footer', 'App\Http\ViewComposers\LatestPortfolio');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path('Helpers/ItjadHelper.php');
    }
}
