<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdentitySocialsModel extends Model
{
    protected $table = 'identity_socials';
    public $incrementing = false;

    public function identity()
    {
        return $this->hasOne(IdentityModel::class, 'id', 'identity_id');
    }
}
