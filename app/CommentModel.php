<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentModel extends Model
{
  protected $table = 'comment';
  public $incrementing = false;

  public function news()
  {
      return $this->belongsTo(NewsModel::class, 'id', 'news_id');
  }
}
