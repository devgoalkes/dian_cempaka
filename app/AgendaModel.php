<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgendaModel extends Model
{
    protected $table = 'agenda';
   	public $incrementing = false;
}
