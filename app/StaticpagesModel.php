<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaticpagesModel extends Model
{
    protected $table = 'staticpages';
   	public $incrementing = false;
}
